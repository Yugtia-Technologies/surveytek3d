angular.module('starter.surveyorControllers', ['ionic'])
    .controller('surveyorCtrl',['Api','$scope','$state','$stateParams','$http','$ionicPopup','$ionicLoading','$rootScope','$stateParams', function(Api,$scope,$state,$stateParams,$http,$ionicPopup,$ionicLoading,$rootScope,$stateParams){
		$scope.surveyList = [];
		setPopupAddSurveryor($scope,$ionicPopup);
		setPopupAddClaim($scope,$state,$ionicPopup);
		setPopupEditSurveryor($scope,$ionicPopup,$rootScope);
		var req = {
			headers: {
			  'X-public': localStorage.getItem("X-public"),
			  'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
			  'device-id':localStorage.getItem("device-id"),
			  'Content-Type': 'application/x-www-form-urlencoded'
			}
      	};
		$scope.$on("$ionicView.beforeEnter", function(event, data){//on dasbord load
			$scope.surveyorList(); 
		});

		$scope.surveyorList = function(keyword) {
			$ionicLoading.show({
		          animation: 'fade-in',
		          showBackdrop: true,
		          maxWidth: 200,
		          showDelay: 0
		        });
			var pdata   = jQuery.param({'user_id':localStorage.getItem("UserId"),'user_token':localStorage.getItem("UserToken"),'keyword':keyword,'user_type':localStorage.getItem("UserType")});     
			var urlBase = Api.urlBase+"surveyors"; 

		    	$http.post(urlBase,pdata,req).then(
			        function (response) {
			        	$ionicLoading.hide();
			        	console.log(response);
			        	if(response.data.status == 1){
			        		$scope.surveyList = response.data.response;
			        		console.log($state.surveyList);
			        	} else {
			        		var alertPopup = $ionicPopup.alert({
								title: 'Surveyor',
								cssClass:'notification',
								template: response.data.message
				            });
			        	} 
			        },
			        function (error) {
			            $ionicLoading.hide();
			            var alertPopup = $ionicPopup.alert({
			              title: 'Surveyor',
			              cssClass:'notification',
			              template: 'There is some server issue, please try again after some time'
			            });
			        }
		        );
		}
		$scope.changeStatus = function(id,status) {
			var req = {
					headers: {
					  'X-public': localStorage.getItem("X-public"),
					  'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
					  'device-id':localStorage.getItem("device-id"),
					  'Content-Type': 'application/x-www-form-urlencoded'
					}
		      	};	
			var pdata   = jQuery.param({'user_id':localStorage.getItem("UserId"),'user_token':localStorage.getItem("UserToken"),'id':id,'status':status});     
			console.log(pdata);
			var urlBase = Api.urlBase+"surveyors/enabledisable"; 
			$http.post(urlBase,pdata,req).then(
		        function (response) {
		        	$ionicLoading.hide();
		        	$scope.surveyorList();		
		        },
		        function (error) {
		            $ionicLoading.hide();
		            var alertPopup = $ionicPopup.alert({
		              title: 'Surveyor',
		              cssClass:'notification',
		              template: 'There is some server issue, please try again after some time'
		            });
		        }
	        );
		}
		//search box
		$scope.show_search_box = function(){
			$(".search-box").show(200);
		}
		$scope.hide_search_box = function(){
			$("#searchtxt").val('');
			$scope.surveyorList();
			$(".search-box").hide(200);
		}
		$scope.search = function() { 
			$scope.surveyorList($("#searchtxt").val());
		}
		$scope.equalHeight = function() {
			equalHeight($(".caption"));
		}

}]).controller('editsurveyorCtrl',['Api','$scope','$state','$stateParams','$http','$ionicPopup','$ionicLoading','$rootScope','$stateParams','$cordovaFileTransfer', function(Api,$scope,$state,$stateParams,$http,$ionicPopup,$ionicLoading,$rootScope,$stateParams,$cordovaFileTransfer) {
		$scope.surveyList = [];
		
		var req = {
			headers: {
			  'X-public': localStorage.getItem("X-public"),
			  'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
			  'device-id':localStorage.getItem("device-id"),
			  'Content-Type': 'application/x-www-form-urlencoded'
			}
      	};
		
		$scope.goToSurveyorEdit = function(sid) {
			$state.go('app.editsurveyor',{id:sid});
		}
		$scope.addsurveryor = {user_id:localStorage.getItem("UserId"),user_token:localStorage.getItem("UserToken")};
		$scope.setSurveyordata = function() {
			console.log($scope.$parent.$parent.data.sid);
			$ionicLoading.show({
		          animation: 'fade-in',
		          showBackdrop: true,
		          maxWidth: 200,
		          showDelay: 0
		        });
			
			var pdata   = jQuery.param({'user_id':localStorage.getItem("UserId"),'user_token':localStorage.getItem("UserToken"),'id':$scope.$parent.$parent.data.sid});     
			var urlBase = Api.urlBase+"users/viewprofile"; 

		    	$http.post(urlBase,pdata,req).then(
			        function (response) {
			        	$ionicLoading.hide();
			        	//console.log(response);
			        	if(response.data.status =="1") {
			        		$scope.addsurveryor = response.data.response;
			        		console.log($scope.addsurveryor);	
			        	}
			        },
			        function (error) {
			            $ionicLoading.hide();
			            var alertPopup = $ionicPopup.alert({
			              title: 'Surveyor',
			              cssClass:'notification',
			              template: 'There is some server issue, please try again after some time'
			            });
			        }
		        );
		}
		$scope.setSurveyordata();

		
		//search box
		$scope.show_search_box = function(){
			$(".search-box").show(200);
		}
		$scope.hide_search_box = function(){
			$("#searchtxt").val('');
			$scope.surveyorList();
			$(".search-box").hide(200);
		}
		$scope.search = function() { 
			$scope.surveyorList($("#searchtxt").val());
		}
		$scope.equalHeight = function() {
			equalHeight($(".caption"));
		}
	 $scope.user_id = localStorage.getItem("UserId");
	 $scope.addsurveryor = {user_id:localStorage.getItem("UserId"),user_token:localStorage.getItem("UserToken")};
	$scope.editSurveryor = function(EditSurveryor) {
      console.log(EditSurveryor);
      if(EditSurveryor.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata                 = [];
        $scope.ServererrMsg   = false;
       
        var urlBase = Api.urlBase+"surveyors/edit";

        req = {
         headers: {
            'X-public': localStorage.getItem("X-public"),
            'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
            'device-id':localStorage.getItem("device-id"),
            //'uuid':device.uuid,
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        };
        
        var targetPath  = $scope.pathForImage($rootScope.profile_image);
        $scope.addsurveryor.user_id = localStorage.getItem("UserId");
       	$scope.addsurveryor.user_token = localStorage.getItem("UserToken");
       	
       	console.log($scope);

       	if( $rootScope.new_pro_image !="" ) {
       		var pdata   = jQuery.param({'user_id':localStorage.getItem("UserId"),'user_token':localStorage.getItem("UserToken"),'id':$scope.addsurveryor.id,'address1': $scope.addsurveryor.address1,'address2': $scope.addsurveryor.address2,'city': $scope.addsurveryor.city,'country': $scope.addsurveryor.country,'email': $scope.addsurveryor.email,'firstname': $scope.addsurveryor.firstname,'id_number': $scope.addsurveryor.id_number,'lastname': $scope.addsurveryor.lastname,'mobile': $scope.addsurveryor.mobile,'profile_pic': $scope.addsurveryor.profile_pic,'state': $scope.addsurveryor.state,'vehical_reg': $scope.addsurveryor.vehical_reg,'zip': $scope.addsurveryor.zip});     
			$http.post(urlBase,pdata,req).then(
		        function (response) {
		        	if(response.data.status =="1") {
		        		  $ionicLoading.hide();
        				  var alertPopup = $ionicPopup.alert({
			                title: 'Edit Surveyor',
			                cssClass:'notification',
			                template: response.data.message
			              });
			              alertPopup.then(function(res) {
			                $rootScope.EditSurveryor.close();
			                $scope.surveyorList(); 
			              });
			        } else {
			        	 $ionicLoading.hide();
			        	var alertPopup = $ionicPopup.alert({
			                title: 'Edit Surveyor',
			                cssClass:'notification',
			                template: response.data.message
		              	});
		              	alertPopup.then(function(res) { });
			        }
		        },
		        function (error) {
		            $ionicLoading.hide();
		            var alertPopup = $ionicPopup.alert({
		              title: 'Surveyor',
		              cssClass:'notification',
		              template: 'There is some server issue, please try again after some time'
		            });
		             $rootScope.EditSurveryor.close();
		        }
	        );	
	   	} else {
       		var filename = $scope.profile_image;
	        var options = {
	            fileKey: "profile_pic",
	            fileName: filename,
	            chunkedMode: false,
	            mimeType: "multipart/form-data",
	            params : $scope.addsurveryor,
	        };
	        
	        $cordovaFileTransfer.upload(urlBase, targetPath, options).then(function(result) {
	        console.log(result.response);
	       	$scope.data_responce = JSON.parse(result.response);
	          if($scope.data_responce.status == 1){
	             var alertPopup = $ionicPopup.alert({
	               title: 'Add Surveyor',
	               cssClass:'notification',
	               template: $scope.data_responce.message
	              });
	              alertPopup.then(function(res) {
	                $rootScope.EditSurveryor.close();
	                $scope.viewprofile();
	                delete $scope.data_responce;
	              });
	              $ionicLoading.hide();
	            } else {
	              var alertPopup = $ionicPopup.alert({
	                title: 'Edit Surveyor',
	                cssClass:'notification',
	                template: $scope.data_responce.message
	              });
	              alertPopup.then(function(res) {
	                $rootScope.EditSurveryor.close();
	                delete $scope.data_responce;
	              });
	              $ionicLoading.hide();
	           }
	        });
       	}
      }   
    
    } 
}])
.directive('repeatDone', function() {
	return function(scope, element, attrs) {
		if (scope.$last) { // all are rendered
			scope.$eval(attrs.repeatDone);
		}
	}
});