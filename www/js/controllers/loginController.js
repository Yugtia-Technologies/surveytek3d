angular.module('starter.logincontrollers', ['ionic', 'googlechart'])
  .controller('loginCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', 'masterTable', 'DbService', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, masterTable, DbService) {
    // Perform the login action when the user submits the login form

    $rootScope.app = {
      internet_connection_message: 'No internet connection !',
      is_admin: false
    };

    setTimeout(function() {
      if (check_connection()) {
        masterTable.masterTableImport();
        DbService.CreateDatabaseClaimTable();
      }
    }, 2000);
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
      masterTable.masterTableImport();
    });

    console.log($rootScope);
    $scope.loginForm = {
      username: '',
      password: ''
    };

    $scope.forgot_password = function() {
      $('#loginForm')[0].reset();
      $state.go("forgot_password");
    };
    $scope.$on("$ionicView.beforeEnter", function(event, data) { //on dasbord load
      if (localStorage.getItem("password") != '' && localStorage.getItem("password") != null &&
        localStorage.getItem("username") != '' && localStorage.getItem("username") != null) {
        $state.go("app.dashboard");
      }
    });
    $scope.doLogin = function(loginForm) {

      if (loginForm.$valid) {
        if (check_connection()) {
          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          $scope.ServererrMsg = false;

          var pdata = $('#loginForm').serialize();
          var urlBase = Api.urlBase + "users/login";

          req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              // console.log(response);
              if (response.data.status == 0) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Login',
                  cssClass: 'notification',
                  template: response.data.message
                });

                alertPopup.then(function(res) {
                  $('#loginForm').find("input[name=username]").focus();
                });
              } else {
                $ionicLoading.hide();
                if (typeof loginForm.airplaneMode != 'undefine' && loginForm.airplaneMode == true) {
                  //if select kipp me login
                  localStorage.setItem("username", loginForm.username.$viewValue);
                  localStorage.setItem("password", loginForm.password.$viewValue);
                }
                localStorage.setItem("UserId", response.data.response.id);
                localStorage.setItem("UserToken", response.data.response.user_token);
                localStorage.setItem("ProfilePic", response.data.response.profile_pic);
                $rootScope.UserDisplayname = response.data.response.firstname + ' ' + response.data.response.lastname;
                $rootScope.profile_pic = response.data.response.profile_pic;

                localStorage.setItem("UserType", response.data.response.usertype);
                $rootScope.userData = response.data.response;
                $rootScope.UserType = response.data.response.usertype;
                $rootScope.app = {};

                localStorage.setItem("UserData", JSON.stringify(response.data.response));
                $('#loginForm')[0].reset();

                if (localStorage.getItem("UserType") == 1) {
                  $rootScope.app.is_admin = true;
                } else {
                  $rootScope.app.is_admin = false;
                }

                $state.go('app.dashboard');
              }
              console.log(response);
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Login',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          console.log($rootScope.app);
          var alertPopup = $ionicPopup.alert({
            title: 'Login',
            cssClass: 'notification',
            template: $rootScope.app.internet_connection_message
          });
        }
      }
    };
  }]);
