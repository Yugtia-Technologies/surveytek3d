angular.module('starter.forgotpassworcontrollers', ['starter.services','ionic', 'googlechart'])

.controller('ForgotPasswordCtrl',['Api','$scope','$state','$http','$ionicPopup','$ionicLoading','$rootScope', function(Api,$scope,$state,$http,$ionicPopup,$ionicLoading,$rootScope){
    $scope.loginForgotPassword = {
      email: '',
    };
   
    $scope.sendReqForgotPassword = function(loginForgotPassword) {
      if(loginForgotPassword.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
      
        $scope.ServererrMsg = false;

        var pdata   = $('#loginForgotPassword').serialize();     
        var urlBase = Api.urlBase+"users/forgot_password";

        req = {
         headers: {
            'X-public': localStorage.getItem("X-public"),
            'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
            'device-id':localStorage.getItem("device-id"),
            //'uuid':device.uuid,
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        };
        $http.post(urlBase,pdata,req).then(
          function (response) {
            if(response.data.status==0)
            {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                 title: 'Forgot Password',
                  cssClass:'notification',
                 template: response.data.message
               });               
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                 title: 'Forgot Password',
                 cssClass:'notification',
                 template: response.data.message
              });
              $('#loginForgotPassword')[0].reset();
              alertPopup.then(function(res) {
                $state.go('login');
              });
            } 
          }, function (error) {
             $ionicLoading.hide();
             var alertPopup = $ionicPopup.alert({
                title: 'Login',
                cssClass:'notification',
                template: 'There is some server issue, please try again after some time'
              });
        });
      }
    }
}])
