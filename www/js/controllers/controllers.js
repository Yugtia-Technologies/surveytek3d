angular.module('starter.controllers', ['ngMap', 'ngCordova', 'ionic', 'googlechart'])
  .controller('AppCtrl', function(masterTable, Api, $ionicHistory, $http, $scope, $rootScope, $ionicPopup, $ionicLoading, $state, $ionicModal, $timeout, $cordovaFileTransfer, $cordovaActionSheet, $cordovaCamera, $cordovaSQLite, $cordovaDevice, $cordovaFile, $cordovaNetwork, DbService, $stateParams) {
    var database = null;
    $rootScope.app = {
      internet_connection_message: 'No internet connection !',
      is_admin: false
    };
    $scope.$on('$ionicView.loaded', function() {
      if (localStorage.getItem("UserType") != "" && localStorage.getItem("UserType") == 1) {
        $rootScope.app.is_admin = true;
      } else {
        $rootScope.app.is_admin = false;
      }
      if (localStorage.getItem("UserType") != "") {
        $rootScope.UserType = localStorage.getItem("UserType");
      }
    });

    /*$rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
      $scope.app = {
        is_online: true
      }
      //alert("onLine");
    });
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
      $scope.app = {
        is_online: false
      }
      //alert("offLine");
    });*/


    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:

    // Form data for the login modal
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
      DbService.syncClaimsData();
      setTimeout(function() {
        masterTable.masterTableImport();
      }, 2000);
      $rootScope.$apply(function() {
        $rootScope.app.is_online = true;
      });
    });
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
      //$rootScope.$apply(function() {
      $rootScope.app.is_online = false;
      //});
    });
    $scope.loginData = {};
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    var d = new Date();
    d.setMonth(d.getMonth() - 1);
    //February 16, 2017 - March 17, 2017

    $scope.menu_date = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear() + ' - ' + monthNames[new Date().getMonth()] + ' ' + new Date().getDate() + ', ' + new Date().getFullYear();
    //alert($scope.menu_date);
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login-home.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    var user_detail = JSON.parse(localStorage.getItem("UserData"));
    if (user_detail != null) {
      $rootScope.UserDisplayname = user_detail['firstname'] + ' ' + user_detail['lastname'];
      $rootScope.profile_pic = localStorage.getItem("ProfilePic");
    } else {
      $state.go("login");
    }
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };
    $scope.claimDetails = function(id, from_dashboard) {
      if (typeof from_dashboard == 'undefined' || from_dashboard == '') {
        from_dashboard = 0;
      }
      if (check_connection() && localStorage.getItem("UserType") == 1) {
        $state.go("app.detailview", {
          'id': id,
          'from_dashboard': from_dashboard
        });
      } else if (localStorage.getItem("UserType") != 1) {
        $state.go("app.claimview", {
          'id': id,
          'from_dashboard': from_dashboard
        });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Login',
          cssClass: 'notification',
          template: $rootScope.app.internet_connection_message
        });
      }
    };
    $scope.claimDetailView = function(id, from_dashboard) {
      if (typeof from_dashboard == 'undefined' || from_dashboard == '') {
        from_dashboard = 0;
      }
      $state.go("app.claimview", {
        'id': id,
        'from_dashboard': from_dashboard
      });
    };
    $scope.claimReports = function(claim_id) {
      $state.go("app.claim-report", {
        'claim_id': claim_id
      });
    };
    $scope.dryingForm = function(claim_id) {
      $state.go("app.drying_form", {
        'claim_id': claim_id
      });
    };
    $scope.healthsafeForm = function(claim_id) {
      $state.go("app.healthsafe_form", {
        'claim_id': claim_id
      });
    };
    $scope.pfiForm = function(claim_id) {
      $state.go("app.pfi_form", {
        'claim_id': claim_id
      });
    };
    $scope.recoveryForm = function(claim_id) {
      $state.go("app.recovery_form", {
        'claim_id': claim_id
      });
    };

    $scope.claimList = function() {
      counter = counter + 1;
      $state.go("app.jobpreview", {
        'client_id': '',
        'daterange': $scope.menu_date,
        'type': '1',
        'count': counter
      });
    };
    $scope.sows = function() {
      $state.go("app.sow");
    };
    $scope.profile = function() {
      $state.go("app.profile");
    };
    var counter = 0;
    $scope.go_claim_list = function(date) {
      //alert($("#reportrange .current-date").text());
      $ionicHistory.clearCache();
      counter = counter + 1;
      $state.go("app.jobpreview", {
        'client_id': '',
        'daterange': date,
        'type': '1',
        'count': counter
      });
    };
    $scope.dashboard = function() {
      $state.go("app.dashboard");
    };
    // Open the login modal
    $scope.doLogout = function() {
      var device_id = localStorage.getItem("device-id");
      var Xpub_id = localStorage.getItem("X-public");
      var app_var = $rootScope.app;
      $rootScope = null;
      $scope = null;
      $rootScope = {
        'app': app_var
      };
      localStorage.clear();
      localStorage.setItem("device-id", device_id);
      localStorage.setItem("X-public", Xpub_id);
      $state.go('login');
      //$state.go("app.login-home");
    };
    $rootScope.file_option = function(upload_file, rootScope) {
      $rootScope = rootScope;
      var options = {
        title: 'Select Image Source',
        buttonLabels: ['Load from Library', 'Use Camera'],
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton: true,
      };
      $rootScope.is_upload_file = false;
      if (upload_file == 'upload') {
        $rootScope.is_upload_file = true;
      }
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          console.log(Camera.PictureSourceType);
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        } else if (btnIndex === 2) {
          type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
          $rootScope.selectPicture(type);
        }
      });
    }
    $rootScope.SelectDrp = function(Options, vlaue_, title_, default_) {
      var selectList = [];
      if (default_ != '') {
        selectList.push({
          'id': '',
          'name': default_
        });
      }
      $.each(Options, function(k, v) {
        selectList.push({
          'id': v[vlaue_],
          'name': v[title_]
        });
      });
      console.log(selectList);
      return selectList;
    };
    $rootScope.selectPicture = function(sourceType) {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        targetWidth: 320,
        targetHeight: 320
      };
      console.log($cordovaCamera);
      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');
          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
              window.resolveLocalFileSystemURL(entry, success, fail);

              function fail(e) {
                console.error('Error: ', e);
              }

              function success(fileEntry) {
                var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                // Only copy because of access rights
                $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success) {
                  $rootScope.profile_image = newFileName;

                  if (!$rootScope.$$phase) {
                    $rootScope.$apply(function() {
                      $rootScope.new_pro_image = $scope.pathForImage(newFileName);
                    });
                  } else {
                    $rootScope.new_pro_image = $scope.pathForImage(newFileName);
                  }
                  if ($rootScope.is_upload_file) {
                    $rootScope.upload_file();
                  }
                }, function(error) {
                  //$scope.showAlert('Error', error.exception);
                });
              };
            });
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success) {
              $rootScope.profile_image = newFileName;
              if (!$rootScope.$$phase) {
                $rootScope.$apply(function() {
                  $rootScope.new_pro_image = $scope.pathForImage(newFileName);
                });
              } else {
                $rootScope.new_pro_image = $scope.pathForImage(newFileName);
              }
              //$rootScope.new_pro_image = $rootScope.pathForImage(newFileName);
              console.log($state.current);
              if ($rootScope.is_upload_file) {
                $rootScope.upload_file();
              }
              //location.href.reload(true);
              //$state.go($state.current, {}, {reload: true});
              //$window.location.reload(true);
            }, function(error) {
              //$scope.showAlert('Error', error.exception);
            });
          }
        },
        function(err) {
          // Not always an error, maybe cancel was pressed...
        })
    };
    $rootScope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };
    $scope.claim_detail_manage = function(claim_details) {
      if (claim_details.length > 0) {
        $.each(claim_details, function(k_claim, v_claim) {
          DbService.addUser(1, 'first_name', 'last_name', 'email', '456132789', '123123', 'address2', 'address1', 'city', 'state', '123456');
        });
      }
    };
    $rootScope.downloadFile = function(fiels, drive_name) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
        fs.root.getDirectory(
          "surveytek/" + drive_name, {
            create: true
          },
          function(dirEntry) {
            console.log(dirEntry);
            $.each(fiels, function(key_files, v_filename) {
              file_path = v_filename.split('/');
              file_name = file_path[file_path.length - 1];
              console.log('file_name' + file_name);
              console.log('file_name' + file_path);
              dirEntry.getFile(
                file_name, {
                  create: true,
                  exclusive: false
                },
                function gotFileEntry(fe) {
                  var p = fe.toURL();
                  fe.remove();
                  ft = new FileTransfer();
                  ft.download(
                    encodeURI(v_filename),
                    p,
                    function(entry) {
                      $ionicLoading.hide();
                      $scope.imgFile = entry.toURL();
                    },
                    function(error) {
                      $ionicLoading.hide();
                      alert("Download Error Source -> " + error.source);
                    },
                    false,
                    null
                  );
                },
                function() {
                  $ionicLoading.hide();
                  console.log("Get file failed");
                }
              );
            });
          }
        );
      });
    }
    $scope.file_upload = function(url, targetPath) {
      // Destination URL
      var filename = targetPath.split("/").pop();

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpg",
        params: {
          'directory': 'upload',
          'fileName': filename
        } // directory represents remote directory,  fileName represents final remote file name
      };

      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
        console.log("SUCCESS: " + JSON.stringify(result.response));
      }, function(err) {
        console.log("ERROR: " + JSON.stringify(err));
      }, function(progress) {
        // PROGRESS HANDLING GOES HERE
      });
    };
    $rootScope.openSow = function(room_id, claim_id, open_tab, local_db_id, to_link) {
      if (typeof to_link == 'undefined') {
        to_link = '';
      }
      $state.go("app.sow", {
        'room_id': room_id,
        'claim_id': claim_id,
        'open_tab': open_tab,
        'local_db_id': local_db_id,
        'to_link': to_link
      });
    };
  }).controller('detailviewCtrl', function($scope, $ionicPopup) {
    // When button is clicked, the popup will be shown...
    setPopupAddStructur($scope, $ionicPopup);
    setPopupDeleteClaim($scope, $ionicPopup);
    assign_surveryor($scope, $ionicPopup);
  })

//popup form
function assign_surveryor($scope, $ionicPopup) {
  $scope.showPopupAssignSurveryor = function() {
    $scope.data = {}
    // Custom popup
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/assign-surveryor.html',
      title: 'Select Surveryor',
      cssClass: 'modal-popup',
      scope: $scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupDeleteClaim($scope, $ionicPopup) {
  $scope.showPopupDeleteClaim = function() {
    $scope.data = {}
    // Custom popup
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/delete-claim.html',
      title: 'Delete Claim',
      cssClass: 'modal-popup',
      scope: $scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupAddStructur($scope, $ionicPopup, $rootScope) {
  $scope.showPopup = function() {

    //$rootScope.popup_title = "SELECT FROM THE LIST BELOW OR ADD A NEW STRUCTURE";
    //$scope.data = {}
    var scope = $scope;
    // Custom popup
    $rootScope.AddStructur = $ionicPopup.show({
      templateUrl: 'templates/add-structur.html',
      title: $rootScope.popup_title,
      cssClass: 'modal-popup',
      scope: scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.AddStructur.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupAddClaim($scope, $state, $ionicPopup, $rootScope) {
  $scope.showAddClaimPopup = function(claim_id) {
    $state.go('app.add-claim', {
      'claim_id': claim_id
    });
  };
}

function setPopupAddSurveryor($scope, $ionicPopup, $rootScope) {
  $scope.showAddSurveryorPopup = function() {
    $scope.data = {}
    // Custom popup
    $rootScope.AddSurveryor = $ionicPopup.show({
      templateUrl: 'templates/add-surveryor.html',
      title: 'Add Surveyor',
      cssClass: 'modal-popup',
      scope: $scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.AddSurveryor.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupEditSurveryor($scope, $ionicPopup, $rootScope) {
  $scope.showEditSurveryorPopup = function(id) {
    $scope.data = {
      sid: id
    }
    $rootScope.EditSurveryor = $ionicPopup.show({
      templateUrl: 'templates/edit-surveryor.html',
      title: 'Edit Surveyor',
      cssClass: 'modal-popup',
      scope: $scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.EditSurveryor.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupAddSow($scope, $ionicPopup, $rootScope) {

  $rootScope.showAddSowPopup = function() {
    //$rootScope.gettasktrades(0);
    scope = $scope;
    //$scope.data = {};
    //$scope.data = {};
    // Custom popup
    $rootScope.AddSowPopup = $ionicPopup.show({
      templateUrl: 'templates/add-sow.html',
      title: 'Add SOW',
      cssClass: 'modal-popup',
      scope: scope,
      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.AddSowPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupAddNote($scope, $ionicPopup, $rootScope) {
  $scope.showAddNote = function() {
    $scope.data = {}
    // Custom popup
    $rootScope.AddClaimNotePopup = $ionicPopup.show({
      templateUrl: 'templates/add-note.html',
      title: 'Add Note',
      cssClass: 'modal-popup',
      scope: $scope,
      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.AddClaimNotePopup.then(function(res) {
      console.log('Tapped!', res);
    });
  };
}

function setPopupshowMapInPopUp($scope, $ionicPopup, $rootScope) {
  $scope.showMapInPopUp = function(mapAddress, popup_title) {
    // Custom popup
    if (check_connection()) {
      $rootScope.mapAddress = mapAddress;
      $rootScope.AddClaimNotePopup = $ionicPopup.show({
        templateUrl: 'templates/custommap.html',
        title: popup_title,
        cssClass: 'modal-popup-map',
        scope: $scope,
        buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
          text: '<i class="icon ion-close-circled"></i>',
          type: 'popclose',
          onTap: function(e) {}
        }]
      });
    } else {
      var alertPopup = $ionicPopup.alert({
        title: popup_title,
        cssClass: 'notification',
        template: 'No internet connection !'
      });
    }
  };
}

function setPopupEditProfile($scope, $ionicPopup, $state, $rootScope) {
  $scope.showPopEditProfile = function() {
    $scope.data = {}
    // Custom popup
    $rootScope.AddEditProfile = $ionicPopup.show({
      templateUrl: 'templates/edit-profile.html',
      title: 'Edit Profile',
      cssClass: 'modal-popup',
      scope: $scope,

      buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
        text: '<i class="icon ion-close-circled"></i>',
        type: 'popclose',
        onTap: function(e) {}
      }]
    });

    $rootScope.AddEditProfile.then(function(res) {
      //$state.go('app.profile');
      //location.reload();
    });
  };
}

function setClaimLocationImageInDashboard($scope, $ionicPopup, $rootScope) {
  $scope.showClaimLocationImageInDashboard = function(customerFullAddress) {
    //console.log(customerFullAddress);
    return "https://maps.googleapis.com/maps/api/staticmap?center=" + customerFullAddress + "&zoom=19&scale=1&size=358x190&maptype=satellite&key=&format=jpg&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C" + customerFullAddress;
  };
}

function check_connection() {
  //console.log(localStorage);
  if (navigator.onLine) {
    //localStorage.setItem("is_online", true);
    return true;
  } else {
    console.log("device offline");
    return false;
  }
}
