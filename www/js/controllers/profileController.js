angular.module('starter.profilecontrollers', ['ionic', 'googlechart'])
  .controller('profileCtrl', ['Api', 'DbService', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', function(Api, DbService, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope) {
    setPopupEditProfile($scope, $ionicPopup, $state, $rootScope);
    $scope.$on("$ionicView.beforeEnter", function(event, data) { //on dasbord load
      $scope.viewprofile();
    });
    $rootScope.new_pro_image = '';
    $rootScope.new_pro_image = '';
    $scope.viewprofile = function() {
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_token': localStorage.getItem("UserToken"),
        'user_id': localStorage.getItem("UserId"),
        'id': localStorage.getItem("UserId")
      });
      var urlBase = Api.urlBase + "users/viewprofile";

      req = {
        headers: {
          'X-public': localStorage.getItem("X-public"),
          'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
          'device-id': localStorage.getItem("device-id"),
          //'uuid':device.uuid,
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };
      //$cordovaFileTransfer()
      $http.post(urlBase, pdata, req).then(
        function(response) {

          if (response.data['status']) {
            user_dtl = response.data.response;
            //DbService.addUser(user_dtl.id, user_dtl.first_name, user_dtl.last_name,user_dtl.email,user_dtl.mobile,user_dtl.vehical_reg,user_dtl.address2,user_dtl.address1,user_dtl.city,user_dtl.state,user_dtl.zip);
            $scope.prfileDtl = response.data.response;
            $ionicLoading.hide();
            localStorage.setItem("ProfilePic", $scope.prfileDtl.profile_pic);
            $rootScope.profile_pic = localStorage.getItem("ProfilePic");
            $rootScope.UserDisplayname = $scope.prfileDtl.firstname + ' ' + $scope.prfileDtl.lastname;
          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Profile',
              cssClass: 'notification',
              template: response['message']
            });
          }

        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Profile',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }

  }])
  .controller('editProfileCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaFileTransfer', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaFileTransfer) {

    $scope.UserToken = localStorage.getItem("UserToken");
    $scope.UserId = localStorage.getItem("UserId");
    setPopupEditProfile($scope, $ionicPopup);
    $rootScope.profile_image = '';
    $rootScope.new_pro_image = '';
    var pdata = jQuery.param({
      'user_token': localStorage.getItem("UserToken"),
      'user_id': localStorage.getItem("UserId"),
      'id': localStorage.getItem("UserId")
    });
    var urlBase = Api.urlBase + "users/viewprofile";
    var file_name;
    /*var fileModel = {
        lastModified: '',
        lastModifiedDate:'',
        name: '',
        size: '',
        type: '',
        webkitRelativePath:''
    }*/
    $scope.ctrl_file_option = function() {
      $rootScope.file_option('', $rootScope);
    }
    $scope.uploadfile_scop = function(file_) {
      file_name = file_;
      console.log(file_);
    }
    req = {
      headers: {
        'X-public': localStorage.getItem("X-public"),
        'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
        'device-id': localStorage.getItem("device-id"),
        //'uuid':device.uuid,
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };

    $http.post(urlBase, pdata, req).then(
      function(response) {
        if (response.data['status']) {
          console.log(response.data.response);
          $scope.prfileDtl = response.data.response;
          $scope.prfileDtl.user_id = localStorage.getItem("UserId");
          $scope.prfileDtl.user_token = localStorage.getItem("UserToken");
          $scope.prfileDtl.password = '';
          $scope.prfileDtl.confirmpassword = '';
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Profile',
            cssClass: 'notification',
            template: response['message']
          });
        }
      },
      function(error) {
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Profile',
          cssClass: 'notification',
          template: 'There is some server issue, please try again after some time'
        });
      });

    $scope.updateProfile = function(EditProfile) {
      //console.log(EditProfile.profile_pic.$modelValue);
      //$scope.file_upload(Api.urlBase+"users/editprofile",EditProfile.profile_pic.$modelValue);
      //return;
      if (EditProfile.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;
        var pdata = $('#EditProfile').serialize();
        var urlBase = Api.urlBase + "users/editprofile";

        req = {
          headers: {
            'X-public': localStorage.getItem("X-public"),
            'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
            'device-id': localStorage.getItem("device-id"),
            //'uuid':device.uuid,
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        };
        if (typeof $rootScope.new_pro_image == 'undefined' || $rootScope.new_pro_image == '') {
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Edit Profile',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  $scope.viewprofile();
                  $scope.AddEditProfile.close();
                });
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Edit Profile',
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {

                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Edit Profile',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });

        } else {

          // File for Upload
          var targetPath = $rootScope.new_pro_image;

          // File name only
          var files_name = $rootScope.new_pro_image.split('/');
          var filename = files_name[files_name.length - 1];


          delete $scope.prfileDtl.profile_pic;
          var options = {
            fileKey: "profile_pic",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: $scope.prfileDtl
          };
          console.log(targetPath);
          console.log(options);
          console.log('---');


          $cordovaFileTransfer.upload(urlBase, targetPath, options).then(function(result) {

            $scope.data_responce = JSON.parse(result.response);
            if ($scope.data_responce.status == 1) {
              var alertPopup = $ionicPopup.alert({
                title: 'Edit Profile',
                cssClass: 'notification',
                template: $scope.data_responce.message
              });
              alertPopup.then(function(res) {
                $scope.AddEditProfile.close();
                $scope.viewprofile();
                delete $scope.data_responce;
              });
              $ionicLoading.hide();
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Edit Profile',
                cssClass: 'notification',
                template: $scope.data_responce.message
              });
              alertPopup.then(function(res) {
                //$scope.AddEditProfile.close();
                delete $scope.data_responce;
              });
              $ionicLoading.hide();
            }

            //$scope.showAlert('Success', 'Image upload finished.');
          }, function(err) {
            $ionicLoading.hide();
            console.log(err.body);
          });
        }
      }
    }
  }]);
