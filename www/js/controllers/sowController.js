angular.module('starter.sowcontrollers', ['ionic', 'ngCordova', 'slickCarousel'])
  .controller('sowCtrl', ['Api', '$ionicPlatform', 'DbService', '$timeout', '$filter', '$scope', '$state', '$stateParams', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', 'ionicDatePicker', 'ionicTimePicker', '$cordovaMedia', '$cordovaFile', function(Api, $ionicPlatform, DbService, $timeout, $filter, $scope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, ionicDatePicker, ionicTimePicker, $cordovaMedia, $cordovaFile) {


    $ionicPlatform.ready(function() {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      setPopupAddSow($scope, $ionicPopup, $rootScope);
      //setPopupMedia($scope, $ionicPopup, $rootScope);
    });

    $scope.showPopupMedia = function(file_utl, file_type) {
      if (file_type == 'V') {
        popup_title = "Play Video";
      } else {
        popup_title = "View Picture";
      }

      /*setTimeout(function() {
        if (file_type == 'V') {
          files = file_utl.split('.');
          extenson = files[files.length - 1];
          if (extenson == 'mp4') {
            var video_tag = '<video style="height:100%;" ng-show="data.file_type==\'V\'" autobuffer autoloop class="video" autoplay loop controls>' +
              '<source src="' + file_utl + '" type="video/mp4">' +
              '</video>';
          } else {
            var video_tag = '<video src="' + file_utl + '" style="height:100%;" ng-show="data.file_type==\'V\'" autobuffer autoloop class="video" autoplay loop controls>' +
              '</video>';
          }
          $("div.video").html(video_tag);
          $("div.video").removeClass("hide");
        } else if (file_type == 'I') {
          $("div.video").html("");
          $("img.img").attr("src", file_utl);
          $("img.img").removeClass("hide");
        }
      }, 500);*/
      $scope.data = {
        'file_utl': file_utl,
        'file_type': file_type
      };

      //scope = $scope;
      // Custom popup
      $scope.MediaPopup = $ionicPopup.show({
        templateUrl: 'templates/media-popup.html',
        title: popup_title,
        cssClass: 'modal-popup',
        scope: $scope,

        buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
          text: '<i class="icon ion-close-circled"></i>',
          type: 'popclose',
          onTap: function(e) {}
        }]
      });

    };
    $scope.internal_notes = {
      id: '',
      internal_notes: ''
    }
    $scope.new_pro_image = '';
    $scope.sowList = {
      structurenotes: '',
      sowtask: '',
      job_number: '',
      name: '',
      structuremedia: {
        'Audio': {},
        'Picture': {},
        'Video': {}
      },
    };
    $scope.dataPictureLoaded = false;
    $scope.dataVideoLoaded = false;
    $scope.dataAudioLoaded = false;
    $scope.is_first_time = 0;
    $scope.view_all = 0;

    $scope.images = [];
    $scope.tab_click = function(pass_value) {
      if (pass_value == 'prev_all') {
        $scope.audio_stop();
        $scope.view_all = 1;
        $scope.dataAudioLoaded = true;
        $scope.dataVideoLoaded = true;
        $scope.dataPictureLoaded = true;
      } else if (pass_value == 'videos_div') {
        $scope.audio_stop();
        $scope.view_all = 0;
        $scope.dataPictureLoaded = false;
        $scope.dataAudioLoaded = false;
        $scope.dataVideoLoaded = true;
      } else if (pass_value == 'notes' || pass_value == 'sow_tab') {
        $scope.audio_stop();
        $scope.view_all = 0;
        $scope.dataPictureLoaded = false;
        $scope.dataAudioLoaded = false;
        $scope.dataVideoLoaded = false;
      } else if (pass_value == 'audio_div') {
        $scope.view_all = 0;
        $scope.dataPictureLoaded = false;
        $scope.dataAudioLoaded = true;
        $scope.dataVideoLoaded = false;
      } else if (pass_value == 'pictures_div') {
        $scope.audio_stop();
        $scope.view_all = 0;
        $scope.dataPictureLoaded = true;
        $scope.dataAudioLoaded = false;
        $scope.dataVideoLoaded = false;
      } else {
        $scope.audio_stop();
        $scope.view_all = 1;
        $scope.dataAudioLoaded = true;
        $scope.dataVideoLoaded = true;
        $scope.dataPictureLoaded = true;
      }

    }

    $scope.getSOWDetail = function() { // also edit time data fill

      if (check_connection() && localStorage.getItem("UserType") == 1) {
        //alert($stateParams.room_id);
        room_id = $stateParams.room_id;
        $scope.room_id = $stateParams.room_id;
        $scope.claim_id = $stateParams.claim_id;
        $scope.open_tab = $stateParams.open_tab;

        $scope.UserToken = localStorage.getItem("UserToken");
        $scope.UserId = localStorage.getItem("UserId");
        $scope.UserType = localStorage.getItem("UserType");

        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'id': room_id
        });

        var urlBase = Api.urlBase + "claimstructures/details";
        var req = {
          headers: Api.HEADERS
        };
        $scope.images = [];
        $http.post(urlBase, pdata, req).then(
          function(response) {
            $ionicLoading.hide();
            if (response.data.status == 1) {
              $scope.sowList = response.data.response;
              $rootScope.audio_object_array = {};
              $scope.audiolist = $scope.sowList.structuremedia.Audio;
              $.each($scope.sowList.structuremedia.Audio, function(k, v) {
                $scope.audiolist[k]['is_stope'] = true;
                $scope.audiolist[k]['is_play'] = false;
              });

              var is_pick_true = $scope.dataPictureLoaded;
              var is_video_true = $scope.dataVideoLoaded;
              var is_audio_true = $scope.dataAudioLoaded;

              if ($scope.is_first_time == 0 && $stateParams.open_tab == 'pictures') {
                is_pick_true = true;
              }
              if ($scope.is_first_time == 0 && $stateParams.open_tab == 'audios') {
                is_audio_true = true;
              }
              if ($scope.is_first_time == 0 && $stateParams.open_tab == 'videos') {
                is_video_true = true;
              }
              $scope.dataPictureLoaded = false;
              $scope.dataVideoLoaded = false;
              $scope.dataAudioLoaded = false;

              $timeout(function() {
                if (is_audio_true == true) {
                  $scope.dataAudioLoaded = true;
                  $scope.is_first_time = 1;
                }
                if (is_video_true == true) {
                  console.log($scope.dataVideoLoaded);
                  $scope.dataVideoLoaded = true;
                  $scope.is_first_time = 1;
                }
                if (is_pick_true == true) {
                  $scope.dataPictureLoaded = true;
                  $scope.is_first_time = 1;
                }
              }, 1000);
              if ($scope.sowList.structuremedia['3DImage'].length > 0 && $scope.sowList.structuremedia['3DImage'][0]['media_file'] != '') {
                $scope.new_pro_image = $scope.sowList.structuremedia['3DImage'][0]['media_file'];
              }
              //console.log($scope.sowList);
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Structure Detail',
                cssClass: 'notification',
                template: response.data.message
              });
            }

          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Structure Detail',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
            return;
          }
        );
      } else if (localStorage.getItem("UserType") != 1) {
        $scope.room_id = $stateParams.room_id;
        $scope.claim_id = $stateParams.claim_id;
        $scope.open_tab = $stateParams.open_tab;
        $scope.new_pro_image = '';
        $scope.sowList = {
          'sowtask': {},
          'structuremedia': {
            'Video': [],
            'Picture': [],
            'Audio': []
          },
          'structurenotes': {}
        };
        DbService.getStructurClaimNo($stateParams.room_id).then(function(claimDetail) {
          //if (!$scope.$$phase) {
          //$scope.$apply(function() {
          $scope.sowList.name = claimDetail['name'];
          $scope.sowList.job_number = claimDetail['job_number'];
          /*});
          } else {
            $scope.sowList.name = claimDetail['name'];
            $scope.sowList.job_number = claimDetail['job_number'];
          }*/
        });

        DbService.getSOWList($scope.room_id, $scope.claim_id).then(function(res_sowtask) {
          /*if (!$scope.$$phase) {
            $scope.$apply(function() {
              $scope.sowList.sowtask = res_sowtask[$scope.claim_id][$scope.room_id];
              console.log($scope.sowList.sowtask);
            });
          } else {*/
          $scope.sowList.sowtask = res_sowtask[$scope.claim_id][$scope.room_id];
          //console.log($scope.sowList.sowtask);
          //}
        });
        DbService.getStructurenotes($scope.room_id, $scope.claim_id).then(function(res_structurenotes) {
          /*if (!$scope.$$phase) {
            $scope.$apply(function() {
              $scope.sowList.structurenotes = res_structurenotes[$scope.claim_id][$scope.room_id];
              console.log($scope.sowList.structurenotes);
            });
          } else {*/
          $scope.sowList.structurenotes = res_structurenotes[$scope.claim_id][$scope.room_id];
          /*console.log($scope.sowList.structurenotes);
          }*/
        });
        DbService.getClaimMedia($scope.room_id, $scope.claim_id).then(function(res_claimmedia) {
          console.log(res_claimmedia);
          //$scope.audiolist = [];
          for (i = 0; i < res_claimmedia[$scope.claim_id][$scope.room_id].length; i++) {
            if (res_claimmedia[$scope.claim_id][$scope.room_id][i]['filetype'] == '3D') {
              if (!$scope.$$phase) {
                //$scope.$apply(function() {
                $scope.new_pro_image = cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $stateParams.local_db_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file'];
                //});
              } else {
                $scope.new_pro_image = cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $stateParams.local_db_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file'];
              }
            } else if (res_claimmedia[$scope.claim_id][$scope.room_id][i]['filetype'] == 'A') {
              //if (!$scope.$$phase) {
              //$scope.$apply(function() {
              $scope.sowList.structuremedia.Audio.push({
                'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $stateParams.local_db_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
              });

              //break;
              //});
              /*} else {
                $scope.sowList.structuremedia.Audio.push({
                  'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                  'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $scope.room_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
                });
                $scope.audiolist = $scope.sowList.structuremedia.Audio;
              }*/
            } else if (res_claimmedia[$scope.claim_id][$scope.room_id][i]['filetype'] == 'V') {
              /*if (!$scope.$$phase) {
                $scope.$apply(function() {
                  $scope.sowList.structuremedia.Video.push({
                    'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                    'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $scope.room_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
                  });
                });
              } else {
                $scope.sowList.structuremedia.Video.push({
                  'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                  'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $scope.room_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
                });
              }*/

              /*$scope.sowList.structuremedia.Video.push({
                'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $scope.room_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
              });*/
              //$scope.$digest(function() {
              $scope.sowList.structuremedia.Video.push({
                'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                'media_file': cordova.file.dataDirectory + "claims/" + $scope.claim_id + "/" + $stateParams.local_db_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
              });
              //});
              //$scope.$watch('sowList', function() {});
            } else if (res_claimmedia[$scope.claim_id][$scope.room_id][i]['filetype'] == 'P') {
              /*if (!$scope.$$phase) {
                $scope.$apply(function() {
                  $scope.sowList.structuremedia.Picture.push({
                    'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                    'media_file': cordova.file.dataDirectory + "/claims/" + $scope.claim_id + "/" + $scope.room_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
                  });
                });
              } else {*/
              $scope.sowList.structuremedia.Picture.push({
                'id': res_claimmedia[$scope.claim_id][$scope.room_id][i]['id'],
                'media_file': cordova.file.dataDirectory + "/claims/" + $scope.claim_id + "/" + $stateParams.local_db_id + "/" + res_claimmedia[$scope.claim_id][$scope.room_id][i]['media_file']
              });
              //}
            }
          }
          $scope.audiolist = $scope.sowList.structuremedia.Audio;
          // `SET` claim media file tab
          $.each($scope.sowList.structuremedia.Audio, function(k, v) {
            $scope.audiolist[k]['is_stope'] = true;
            $scope.audiolist[k]['is_play'] = false;
          });

          var is_pick_true = $scope.dataPictureLoaded;
          var is_video_true = $scope.dataVideoLoaded;
          var is_audio_true = $scope.dataAudioLoaded;

          if ($stateParams.open_tab == 'pictures') {
            is_pick_true = true;
          }
          if ($stateParams.open_tab == 'audios') {
            is_audio_true = true;
          }
          if ($stateParams.open_tab == 'videos') {
            is_video_true = true;
          }


          $scope.dataPictureLoaded = false;
          $scope.dataVideoLoaded = false;
          $scope.dataAudioLoaded = false;
          $scope.safeApply($scope);
          $timeout(function() {
            if (is_audio_true == true) {
              $scope.dataAudioLoaded = true;
              $scope.is_first_time = 1;
            }
            if (is_video_true == true) {
              $scope.dataVideoLoaded = true;
              $scope.is_first_time = 1;
            }
            if (is_pick_true == true) {
              $scope.dataPictureLoaded = true;
              $scope.is_first_time = 1;
            }
            $scope.safeApply($scope);
            //$scope.is_first_time = 1;


          }, 2000);
          if (typeof $scope.sowList.structuremedia['3DImage'] != 'undefined' && $scope.sowList.structuremedia['3DImage'][0]['media_file'] != '') {
            $scope.new_pro_image = $scope.sowList.structuremedia['3DImage'][0]['media_file'];
          }
        });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Claim Detail',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
        return;
      }
      /*if ($stateParams.to_link != '') {
        setTimeout(function() {
          $rootScope.openSow($stateParams.room_id, $stateParams.claim_id, $stateParams.to_link, $stateParams.local_db_id);
        }, 500);
        return;
      } else {
        $ionicLoading.hide();
      }*/
      $ionicLoading.hide();
    }
    $scope.audio_track = null;
    $scope.audio_stop = function() {
      if ($scope.audio_track != null) {
        $scope.audio_track.stop();
      }
      $.each($scope.audiolist, function(k, v) {
        $scope.audiolist[k]['is_stope'] = true;
        $scope.audiolist[k]['is_play'] = false;
      });
    }
    $scope.audio_play = function(audio_id) {
      var audio_id = audio_id;
      var id = 0
      //$scope.audioPay = [];

      function onDeviceReady() {
        $.each($scope.audiolist, function(k, v) {
          $scope.audiolist[k]['is_stope'] = true;
          $scope.audiolist[k]['is_play'] = false;
          if ($scope.audiolist[k].id == audio_id) {
            $scope.audioPay = $scope.audiolist[k]['media_file'];
            $scope.audiolist[k]['is_play'] = true;
            $scope.audiolist[k]['is_stope'] = false;
          }
        });
        $scope.audio_track = new Media($scope.audioPay.replace('file://', ''), onSuccess, onError, onStatus);
        $scope.audio_track.play();
      }
      // onSuccess Callback
      function onSuccess() {
        //console.log("Success")
        console.log('Success' + audio_id);
      }
      // onError Callback
      function onError(error) {
        console.log(error);
      }
      // onStatus Callback
      function onStatus(status) {
        if (status == Media.MEDIA_STOPPED) {

          $scope.$apply(function() {
            $.each($scope.audiolist, function(k, v) {
              $scope.audiolist[k]['is_stope'] = true;
              $scope.audiolist[k]['is_play'] = false;
            });
          });
        }
      }
      onDeviceReady();
    };

    $scope.deleteNotes = function(note_id) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete note',
        template: 'Are you sure you want to delete note ?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'id': structure_id
          });
          var urlBase = Api.urlBase + "claimstructures/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              if (response.data.status == 1) {
                $rootScope.get_detail($stateParams.id);
              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Structure',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
              $ionicLoading.hide();
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Structure',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
    //$scope.getSOWDetail();
    $scope.select_fileoption = function(file_upload, file_media_type) {
      $scope.media_type = file_media_type;
      $rootScope.file_option(file_upload, $rootScope);
    }

    $rootScope.upload_file = function() {

      pdata = [];
      $scope.ServererrMsg = false;
      // File for Upload
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var filename = $scope.sowList.name + '_' + $scope.media_type + '_' + $filter('date')(new Date(), 'yyyyMMddHHmmss') + '.' + $rootScope.profile_image.split('.')[1];

      if (check_connection()) {
        var urlBase = Api.urlBase + "claimstructures/uploadmedia";
        //var filename = $rootScope.profile_image;
        var options = {
          fileKey: "mediafiles[]",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: {
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'filetype': $scope.media_type,
            'claimstructure_id': $stateParams.room_id
          }
        };
        var targetPath = $scope.pathForImage($rootScope.profile_image);

        $cordovaFileTransfer.upload(urlBase, targetPath, options).then(function(result) {
          $scope.data_responce = JSON.parse(result.response);
          if ($scope.data_responce.status == 1) {
            if (localStorage.getItem('UserType') != 1) {
              // File name only
              folder = 'claims/' + $scope.claim_id + "/" + $stateParams.local_db_id + "/";
              folders = folder.split('/');
              fullPath = '/';
              var i = 0;
              while (i < folders.length) {
                fullPath = fullPath + '/' + folders[i];
                $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false);
                i = i + 1;
              }
              var NewPath = cordova.file.dataDirectory + folder;
              $cordovaFile.copyFile(cordova.file.dataDirectory, $rootScope.profile_image, NewPath, filename).then(function(success) {
                console.log(success);

                media_id = $scope.data_responce['response'][0]['id'];
                DbService.setMediaLocal(media_id, $stateParams.room_id, localStorage.getItem("UserId"), filename, $scope.media_type, '', '', '', '', '', '1').then(function(res) {
                  /*if ($scope.media_type == "3D") {
                    $scope.new_pro_image = NewPath + filename;
                    if ($scope.view_all == 0) {
                      $scope.navigateTab('add_scan', 'add_scan');
                    } else {
                      $scope.navigateTab('add_scan', 'preview_all');
                    }
                    $ionicLoading.hide();
                    return;
                  }
                  if ($scope.media_type == "P") {
                    $scope.sowList.structuremedia.Picture.push({
                      'id': res.insertId,
                      'media_file': NewPath + filename
                    });
                    if ($scope.view_all == 0) {
                      $scope.navigateTab('add_scan', 'pictures');
                    } else {
                      $scope.navigateTab('add_scan', 'preview_all');
                    }
                    $ionicLoading.hide();
                    return;
                  }*/

                  $scope.getSOWDetail();
                  delete $scope.data_responce;
                  $ionicLoading.hide();
                });
              }, function(error) {
                console.log(error);
                $ionicLoading.hide();
              });
            } else {
              /*if ($scope.media_type == "P") {
                if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'pictures');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }
                $ionicLoading.hide();
                return;
              } else {
                if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'add_scan');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }
                $ionicLoading.hide();
                return;
              }*/

              $scope.getSOWDetail();
              $ionicLoading.hide();
            }
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Upload media',
              cssClass: 'notification',
              template: $scope.data_responce.message
            });
            alertPopup.then(function(res) {
              delete $scope.data_responce;
            });
            $ionicLoading.hide();
          }
          //$scope.showAlert('Success', 'Image upload finished.');
        }, function(error) {
          var message = '';
          if ($scope.media_type == '3D') {
            message = "3D image";
          } else {
            message = "Picture";
          }
          var alertPopup = $ionicPopup.alert({
            title: 'Upload media',
            cssClass: 'notification',
            template: 'Error while ' + message + ' capture. Please try again.'
          });
          $ionicLoading.hide();
          console.log(error);
          console.log(error.body);
        });
      } else if (localStorage.getItem('UserType') != 1) {

        var targetPath = $scope.pathForImage($rootScope.profile_image);
        // File name only
        //var filename = $rootScope.profile_image;

        folder = 'claims/' + $scope.claim_id + "/" + $stateParams.local_db_id + "/";
        folders = folder.split('/');
        fullPath = '/';
        var i = 0;
        while (i < folders.length) {
          fullPath = fullPath + '/' + folders[i];
          $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false);
          i = i + 1;
        }
        var NewPath = cordova.file.dataDirectory + folder;
        var is_pick_true = false;
        $cordovaFile.copyFile(cordova.file.dataDirectory, $rootScope.profile_image, NewPath, filename).then(function(success) {
          console.log(success);
          /*if ($scope.media_type == "3D") {
            $scope.new_pro_image = cordova.file.dataDirectory + folder + filename;
            $scope.tab_click('add_scan');
          }*/

          //is_pick_true

          DbService.syncStatusClaimsData($stateParams.claim_id, '0');

          DbService.setMediaLocal('00' + ($filter('date')(new Date(), 'yyyyMMddHHmmss')), $stateParams.room_id, localStorage.getItem("UserId"), filename, $scope.media_type, '', '', '', '', '', '1').then(function(res) {
            //$scope.getSOWDetail();
            if (res.insertId != 0) {
              /*if ($scope.media_type == "P") {
                $scope.sowList.structuremedia.Picture.push({
                  'id': res.insertId,
                  'media_file': cordova.file.dataDirectory + folder + filename
                });

                $scope.tab_click('pictures_div');

                //setTimeout(function() {

                //$scope.$watch('sowList.structuremedia.Picture', function() {
                //$scope.sowList.structuremedia.Picture = $scope.sowList.structuremedia.Picture;
                $scope.dataPictureLoaded = false;
                //});
                //}, 1000);
                setTimeout(function() {
                  $scope.$apply(function() {
                    $scope.dataPictureLoaded = true;
                  });
                }, 2000);*/
              $scope.getSOWDetail();
              $ionicLoading.hide();
              /*if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'pictures');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }
                $ionicLoading.hide();
                return;
              }*/
            }
            $ionicLoading.hide();
          });

        }, function(error) {
          console.log(error);
          $ionicLoading.hide();
        });
      } else {
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Add Media',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
      }
    }

    $scope.navigateTab = function(from_link, to_link) {
      $rootScope.openSow($stateParams.room_id, $stateParams.claim_id, from_link, $stateParams.local_db_id, to_link);
      //$rootScope.openSow($stateParams.room_id, $stateParams.claim_id, to_link, $stateParams.local_db_id);
    }

    $scope.getnote = function(notes, notes_id) {
      $scope.internal_notes = {
        id: notes_id,
        internal_notes: notes
      }
    }
    $scope.savenote = function() {
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      pdata = [];
      $scope.ServererrMsg = false;
      var structurenotesModel = {
        id: $scope.internal_notes.id,
        claimstructure_id: $stateParams.room_id,
        user_id: localStorage.getItem("UserId"),
        internal_notes: $scope.internal_notes.internal_notes,
        username: $rootScope.UserDisplayname,
        created: Api.formatDate(''),
        deleted: null,
        modified_by: null,
        deleted_by: null,
        status: 1
      }
      if (check_connection()) {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claimstructure_id': $stateParams.room_id,
          'id': $scope.internal_notes.id,
          'internal_notes': $scope.internal_notes.internal_notes
        });
        var urlBase = Api.urlBase + "claimstructures/addeditnotes";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            if (response.data.status == 1) {
              if (localStorage.getItem('UserType') != 1) {
                structurenotesModel = response['data']['response'];
                DbService.addStructurnotes(structurenotesModel).then(function() {
                  $scope.internal_notes = {
                    id: '',
                    internal_notes: ''
                  }
                  $scope.getSOWDetail();
                  /*if ($scope.view_all == 0) {
                    $scope.navigateTab('add_scan', 'notes');
                  } else {
                    $scope.navigateTab('add_scan', 'preview_all');
                  }*/
                  $ionicLoading.hide();
                });
              } else {
                $scope.internal_notes = {
                  id: '',
                  internal_notes: ''
                }
                $scope.getSOWDetail();
                /*if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'notes');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }*/
                $ionicLoading.hide();
                return;
              }
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Note',
                cssClass: 'notification',
                template: response.data.message
              });
              alertPopup.then(function(res) {
                //location.reload();
              });

            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Note',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      } else if (localStorage.getItem('UserType') != 1) {
        if (structurenotesModel.id == '' || structurenotesModel.id == 0) {
          structurenotesModel.id = '00' + ($filter('date')(new Date(), 'yyyyMMddHHmmss'));
        }
        DbService.addStructurnotes(structurenotesModel)
          .then(function() {
            $scope.internal_notes = {
              id: '',
              internal_notes: ''
            };
            DbService.syncStatusClaimsData($stateParams.claim_id, '0');
            /*if ($scope.view_all == 0) {
              $scope.navigateTab('add_scan', 'notes');
            } else {
              $scope.navigateTab('add_scan', 'preview_all');
            }*/
            $scope.getSOWDetail();
            $ionicLoading.hide();
            return;
          });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Add Note',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
      }
    }
    $scope.deleteNote = function(note_id, local_db_id) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete note',
        template: 'Are you sure you want to delete internal note ?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          if (check_connection()) {
            $scope.UserToken = localStorage.getItem("UserToken");
            $scope.UserId = localStorage.getItem("UserId");

            $ionicLoading.show({
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
            });

            var pdata = jQuery.param({
              'user_id': localStorage.getItem("UserId"),
              'user_token': localStorage.getItem("UserToken"),
              'id': note_id
            });
            var urlBase = Api.urlBase + "claimstructures/deletenotes";
            var req = {
              headers: Api.HEADERS
            };
            if (note_id.toString().substr(0, 2) == 0) {
              DbService.DeleteStructurnotes('', note_id, localStorage.getItem('UserId')).then(function() {
                $ionicLoading.hide();
                $scope.getSOWDetail();
              });
            } else {
              $http.post(urlBase, pdata, req).then(
                function(response) {
                  if (response.data.status == 1) {
                    //$scope.getSOWDetail();
                    if (localStorage.getItem('UserType') != 1) {
                      DbService.DeleteStructurnotes(note_id, local_db_id, localStorage.getItem('UserId')).then(function() {
                        //$ionicLoading.hide();
                        $scope.getSOWDetail();
                        /*if ($scope.view_all == 0) {
                          $scope.navigateTab('add_scan', 'notes');
                        } else {
                          $scope.navigateTab('add_scan', 'preview_all');
                        }*/
                        $ionicLoading.hide();
                        return;
                      });
                    }
                  } else {
                    var alertPopup = $ionicPopup.alert({
                      title: 'Delete internal note',
                      cssClass: 'notification',
                      template: response.data.message
                    });
                  }
                  $ionicLoading.hide();
                },
                function(error) {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                    title: 'Delete internal note',
                    cssClass: 'notification',
                    template: 'There is some server issue, please try again after some time'
                  });
                }
              );
            }
          } else if (localStorage.getItem('UserType') != 1) {
            if (note_id.substr(0, 2) == 0) {
              DbService.DeleteStructurnotes('', local_db_id, localStorage.getItem('UserId')).then(function() {
                $ionicLoading.hide();
                //DbService.syncStatusClaimsData($stateParams.claim_id, '0');
                $scope.getSOWDetail();
                /*if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'notes');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }*/
                $ionicLoading.hide();
                return;
              });
            } else {
              DbService.DeleteStructurnotes(note_id, local_db_id, localStorage.getItem('UserId')).then(function() {
                $ionicLoading.hide();
                DbService.syncStatusClaimsData($stateParams.claim_id, '0');
                $scope.getSOWDetail();
                /*if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'notes');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }*/
                $ionicLoading.hide();
                return;
              });
            }
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Delete internal note',
              cssClass: 'notification',
              template: 'No internet connection'
            });
          }
        }
      });
    };
    $rootScope.gettasktrades = function(parent_id) {
      DbService.getSOWMaster(parent_id).then(function(SOWlist) {
        console.log(SOWlist);
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.sowtrades_list = SOWlist['sowtrades'];
            $scope.back_button_val = SOWlist['back_button_val'];
          });
        } else {
          $scope.sowtrades_list = SOWlist['sowtrades'];
          $scope.back_button_val = SOWlist['back_button_val'];
        }
      });
      /*$scope.UserToken  = localStorage.getItem("UserToken");
      $scope.UserId     = localStorage.getItem("UserId");
      $scope.UserType   = localStorage.getItem("UserType");
      claim_id          = $stateParams.claim_id;
      $rootScope.current_structure_page = parent_id;
      $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
      var pdata   = jQuery.param({'user_id':localStorage.getItem("UserId"),'user_token':localStorage.getItem("UserToken"),'claim_id':claim_id,'parent_id':parent_id});
      var urlBase = Api.urlBase+"claimstructures/gettasktrades";
      var req     = {
        headers: Api.HEADERS
      };

      $http.post(urlBase,pdata,req).then(
        function (response) {
          console.log(response);
          if(response.data.status == 1) {
            $scope.SowList = response.data.response;
            if($scope.SowList.length > 0 && $rootScope.current_structure_page != 0){
              $scope.parent_back_id = $scope.SowList[0].previous_parent_id;
            }else{
              $scope.parent_back_id = -1;
            }
          } else {
            var alertPopup = $ionicPopup.alert({
                  title: 'Claim Detail',
                  cssClass:'notification',
                  template: response.data.message
                });
          }
          $ionicLoading.hide();
        },function (error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass:'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );*/

    }
    $scope.savetasktrades = function(task_id, trade_id, measure, uom) {
      claim_id = $stateParams.claim_id;

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var SowModel = {
        id: '00' + ($filter('date')(new Date(), 'yyyyMMddHHmmss')),
        user_id: localStorage.getItem('UserId'),
        claimstructure_id: $stateParams.room_id,
        trade_id: trade_id,
        task_id: task_id,
        surveyor_id: 0,
        measure: measure,
        uom: uom,
        start_date: null,
        end_date: null,
        measure: '',
        status: 1,
        created: Api.formatDateDatabase(),
        modified: null,
        deleted: null,
        modified_by: null,
        deleted_by: null
      };
      if (check_connection()) {
        pdata = [];
        $scope.ServererrMsg = false;

        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claimstructure_id': $stateParams.room_id,
          'task_id': task_id
        });
        var urlBase = Api.urlBase + "claimstructures/addsowtask";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            if (response.data.status == 1) {

              SowModel.id = response['data']['response'][0]['id'];
              if (localStorage.getItem('UserType') != 1) {
                $rootScope.AddSowPopup.close();
                DbService.addSowtask(SowModel).then(function() {
                  $scope.getSOWDetail();
                  $ionicLoading.hide();
                });
              } else {
                $scope.getSOWDetail();
                $rootScope.AddSowPopup.close();
                $ionicLoading.hide();
              }

              /*if ($scope.view_all == 0) {
                $scope.navigateTab('add_scan', 'sow');
              } else {
                $scope.navigateTab('add_scan', 'preview_all');
              }*/
              //$ionicLoading.hide();
              return;
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Add SOW',
                cssClass: 'notification',
                template: response.data.message
              });
              alertPopup.then(function(res) {
                //location.reload();
              });
            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add SOW',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      } else if (localStorage.getItem('UserType') != 1) {
        DbService.addSowtask(SowModel).then(function() {

          $rootScope.AddSowPopup.close();
          DbService.syncStatusClaimsData(claim_id, '0');
          /*if ($scope.view_all == 0) {
            $scope.navigateTab('add_scan', 'sow');
          } else {
            $scope.navigateTab('add_scan', 'preview_all');
          }*/
          $scope.getSOWDetail();
          $ionicLoading.hide();
          return;
        });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Add SOW',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
      }
    }
    $scope.deleteSowTask = function(task_id) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete SOW',
        template: 'Are you sure you want to delete this?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          if (check_connection()) {
            $scope.UserToken = localStorage.getItem("UserToken");
            $scope.UserId = localStorage.getItem("UserId");

            $ionicLoading.show({
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
            });

            var pdata = jQuery.param({
              'user_id': localStorage.getItem("UserId"),
              'user_token': localStorage.getItem("UserToken"),
              'id': task_id
            });
            var urlBase = Api.urlBase + "claimstructures/deletesowtask";
            var req = {
              headers: Api.HEADERS
            };

            $http.post(urlBase, pdata, req).then(
              function(response) {
                if (response.data.status == 1) {
                  if (localStorage.getItem('UserType') != 1) {
                    DbService.deleteSowTask(task_id);
                  }

                  /*if ($scope.view_all == 0) {
                    $scope.navigateTab('add_scan', 'sow');
                  } else {
                    $scope.navigateTab('add_scan', 'preview_all');
                  }*/
                  $scope.getSOWDetail();
                  $ionicLoading.hide();
                  return;
                } else {
                  var alertPopup = $ionicPopup.alert({
                    title: 'Delete SOW',
                    cssClass: 'notification',
                    template: response.data.message
                  });
                }
                $ionicLoading.hide();
              },
              function(error) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete SOW',
                  cssClass: 'notification',
                  template: 'There is some server issue, please try again after some time'
                });
              }
            );
          } else if (localStorage.getItem('UserType') != 1) {
            if (localStorage.getItem('UserType') != 1) {
              DbService.deleteStarusSowTask(task_id, Date(), localStorage.getItem('UserId')).then(function() {
                DbService.syncStatusClaimsData($stateParams.claim_id, '0');
                /*if ($scope.view_all == 0) {
                  $scope.navigateTab('add_scan', 'sow');
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }*/
                $scope.getSOWDetail();
                $ionicLoading.hide();
                return;
              });
            }
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Delete SOW',
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      });
    };
    $scope.deletemedia = function(media_id, media_type) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete media',
        template: 'Are you sure you want to delete this?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          if (check_connection()) {
            $scope.UserToken = localStorage.getItem("UserToken");
            $scope.UserId = localStorage.getItem("UserId");

            $ionicLoading.show({
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
            });

            var pdata = jQuery.param({
              'user_id': localStorage.getItem("UserId"),
              'user_token': localStorage.getItem("UserToken"),
              'id': media_id
            });
            var urlBase = Api.urlBase + "claimstructures/deletemedia";
            var req = {
              headers: Api.HEADERS
            };
            if (media_id.toString().substring(0, 2) != 0) {
              $http.post(urlBase, pdata, req).then(
                function(response) {
                  if (response.data.status == 1) {
                    if (localStorage.getItem('UserType') != 1) {
                      DbService.DeleteMedia(media_id).then(function() {
                        //$scope.getSOWDetail();
                        /*if ($scope.view_all == 0) {

                          $scope.navigateTab('add_scan', media_type);
                        } else {
                          $scope.navigateTab('add_scan', 'preview_all');
                        }*/
                        $scope.getSOWDetail();
                        $ionicLoading.hide();
                        return;
                      });
                    } else {
                      /*if ($scope.view_all == 0) {

                        $scope.navigateTab('add_scan', media_type);
                      } else {
                        $scope.navigateTab('add_scan', 'preview_all');
                      }*/
                      $scope.getSOWDetail();
                      $ionicLoading.hide();
                      return;
                    }

                  } else {
                    var alertPopup = $ionicPopup.alert({
                      title: 'Delete media',
                      cssClass: 'notification',
                      template: response.data.message
                    });
                  }
                  $ionicLoading.hide();
                },
                function(error) {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                    title: 'Delete media',
                    cssClass: 'notification',
                    template: 'There is some server issue, please try again after some time'
                  });
                }
              );
            } else {
              DbService.DeleteMedia(media_id).then(function() {
                /*if ($scope.view_all == 0) {

                  $scope.navigateTab('add_scan', media_type);
                } else {
                  $scope.navigateTab('add_scan', 'preview_all');
                }*/
                $scope.getSOWDetail();
                $ionicLoading.hide();
                return;
              });
              $ionicLoading.hide();
            }
          } else if (localStorage.getItem('UserType') != 1) {
            DbService.DeleteMediaStatus(media_id).then(function() {
              DbService.syncStatusClaimsData($stateParams.claim_id, '0');
              $scope.getSOWDetail();
              //$ionicLoading.hide();
              /*if ($scope.view_all == 0) {
                $scope.navigateTab('add_scan', media_type);
              } else {
                $scope.navigateTab('add_scan', 'preview_all');
              }*/
              $ionicLoading.hide();
              return;
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Delete media',
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      });
    };
    $scope.cap_video = function() {

      function captureError(error) {
        var msg = 'An error occurred during capture: ' + error.code;
        //navigator.notification.alert(msg, null, 'Uh oh!');
      }

      function capture_Video() {
        // Launch device video recording application,
        // allowing user to capture up to 2 video clips
        navigator.device.capture.captureVideo(captureSuccess, captureError);
        return true;
      }

      function captureSuccess(mediaFiles) {
        console.log(mediaFiles);
        if (typeof mediaFiles != 'undefined' && mediaFiles.length > 0) {
          $scope.uploadMedia(mediaFiles[0], 'V');
        } else {
          var msg = 'An error occurred during capture: ';
          //navigator.notification.alert(msg, null, 'Uh oh!');
        }
      }
      capture_Video();
    }
    $scope.cap_audio = function() {
      function captureAudioError(error) {
        var msg = 'An error occurred during capture: ' + error.code;
        //navigator.notification.alert(msg, null, 'Uh oh!');
      }

      function captureAudioSuccess(mediaFiles) {
        $scope.uploadMedia(mediaFiles[0], 'A');
      }

      navigator.device.capture.captureAudio(captureAudioSuccess, captureAudioError, {
        limit: 1,
        duration: 20
      });

    }
    $scope.uploadMedia = function(mediaFile, media_type) {
      $scope.media_type = media_type;
      console.log("You are in uploadMedia");
      var filename = $scope.sowList.name + '_' + $scope.media_type + '_' + $filter('date')(new Date(), 'yyyyMMddHHmmss') + '.' + mediaFile.name.split('.')[1];
      if (check_connection()) {
        console.log("You are in check_connection");
        var urlBase = Api.urlBase + "claimstructures/uploadmedia";
        var options = {
          fileKey: "mediafiles[]",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: {
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'filetype': media_type,
            chunkedMode: false,
            'claimstructure_id': $stateParams.room_id
          }
        };
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        $cordovaFileTransfer.upload(urlBase, mediaFile.fullPath, options).then(function(result) {

            $scope.data_responce = JSON.parse(result.response);
            if ($scope.data_responce.status == 1) {
              if (localStorage.getItem("UserType") != 1) { //note admin
                folder = 'claims/' + $scope.claim_id + "/" + $stateParams.local_db_id + "/";
                folders = folder.split('/');
                fullPath = '/';
                i = 0;
                while (i < folders.length) {
                  if (i == 0) {
                    fullPath = folders[i];
                  } else {
                    fullPath = fullPath + '/' + folders[i];
                  }
                  $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false).then(function(success) {
                    // success
                    console.log(success);
                  }, function(error) {
                    console.log(error);
                    // error
                  });;
                  i = i + 1;
                }

                var NewPath = cordova.file.dataDirectory + folder;
                var saurcePath = mediaFile.fullPath.replace(mediaFile.name, '');
                //var file_name = $filter('date')(new Date(), 'yyyyMMddHHmmss') + "_" + mediaFile.name;
                var file_name = options['fileName'];
                $cordovaFile.copyFile('file://' + saurcePath, mediaFile.name, NewPath, file_name).then(function(success) {

                  $ionicLoading.hide();
                  media_id = $scope.data_responce['response'][0]['id'];
                  DbService.setMediaLocal(media_id, $stateParams.room_id, localStorage.getItem("UserId"), file_name, $scope.media_type, '', '', '', '', '', '1').then(function(res) {
                    var is_video_true = false;
                    var is_audio_true = false;
                    /*if ($scope.media_type == 'V') {
                    $scope.sowList.structuremedia.Video.push({
                      'id': res.insertId,
                      'media_file': NewPath + file_name
                    });
                    $scope.tab_click('videos_div');
                    is_video_true = true;
                    if ($scope.view_all == 0) {
                      $scope.navigateTab('add_scan', 'videos');
                    } else {
                      $scope.navigateTab('add_scan', 'preview_all');
                    }
                    $ionicLoading.hide();
                    return;
                  }
                  if ($scope.media_type == 'A') {
                    $scope.sowList.structuremedia.Audio.push({
                      'id': res.insertId,
                      'media_file': NewPath + file_name,
                      'is_play': false,
                      'is_stope': true
                    });
                    //$scope.audioPay[res.insertId] = NewPath + file_name;
                    $scope.tab_click('audio_div');
                    is_audio_true = true;
                    if ($scope.view_all == 0) {
                      $scope.navigateTab('add_scan', 'audios');
                    } else {
                      $scope.navigateTab('add_scan', 'preview_all');
                    }
                    $ionicLoading.hide();
                    return;
                  }
                  $scope.dataAudioLoaded = false;
                  $scope.dataVideoLoaded = false;
                  $timeout(function() {
                    if (is_audio_true == true) {
                      $scope.dataAudioLoaded = true;
                    }
                    if (is_video_true == true) {
                      console.log($scope.dataVideoLoaded);
                      $scope.dataVideoLoaded = true;
                    }
                  }, 1000);
                });*/
                    $scope.getSOWDetail();
                  }, function(error) {
                    console.log(error);
                    $ionicLoading.hide();
                  });
                  $ionicLoading.hide();
                });
              } else {
                /*if ($scope.media_type == 'A') {
                  if ($scope.view_all == 0) {
                    $scope.navigateTab('add_scan', 'audios');
                  } else {
                    $scope.navigateTab('add_scan', 'preview_all');
                  }
                  $ionicLoading.hide();
                  return;
                } else if ($scope.media_type == 'V') {
                  if ($scope.view_all == 0) {
                    $scope.navigateTab('add_scan', 'videos');
                  } else {
                    $scope.navigateTab('add_scan', 'preview_all');
                  }
                  $ionicLoading.hide();
                  return;
                }*/
                $scope.getSOWDetail();
                //delete $scope.data_responce;
              }
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Video Upload',
                cssClass: 'notification',
                template: $scope.data_responce.message
              });
              alertPopup.then(function(res) {
                //$scope.AddEditProfile.close();
                delete $scope.data_responce;
              });
              $ionicLoading.hide();
            }
          },
          function(err) {
            var message = '';
            if ($scope.media_type == 'V') {
              message = "Video";
            } else if ($scope.media_type == 'A') {
              message = "Audio";
            }

            var alertPopup = $ionicPopup.alert({
              title: 'Upload media',
              cssClass: 'notification',
              template: 'Error while ' + message + ' capture. Please try again.'
            });
            console.log(err);
            $ionicLoading.hide();
          },
          function(progress) {
            console.log(progress);
          });
      } else if (localStorage.getItem('UserType') != 1) {

        console.log("You are in check_connection else part");
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        folder = 'claims/' + $scope.claim_id + "/" + $stateParams.local_db_id + "/";
        folders = folder.split('/');
        fullPath = '/';
        i = 0;
        while (i < folders.length) {
          if (i == 0) {
            fullPath = folders[i];
          } else {
            fullPath = fullPath + '/' + folders[i];
          }
          $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false).then(function(success) {
            // success
            console.log(success);
          }, function(error) {
            console.log(error);
            // error
          });;
          i = i + 1;
        }

        var NewPath = cordova.file.dataDirectory + folder;
        var saurcePath = mediaFile.fullPath.replace(mediaFile.name, '');
        //var filename = $scope.sowList.name + '_' + $scope.media_type + '_' + $filter('date')(new Date(), 'yyyyMMddHHmmss') + mediaFile.name.split('.')[1];

        //var new_file = $filter('date')(new Date(), 'yyyyMMddHHmmss') + "_" + mediaFile.name;
        $cordovaFile.copyFile('file://' + saurcePath, mediaFile.name, NewPath, filename).then(function(success) {

          DbService.setMediaLocal('00' + ($filter('date')(new Date(), 'yyyyMMddHHmmss')), $stateParams.room_id, localStorage.getItem("UserId"), filename, $scope.media_type, '', '', '', '', '', '1').then(
            function(res) {
              /*var is_video_true = false;
            var is_audio_true = false;
            if ($scope.media_type == 'V') {
              $scope.sowList.structuremedia.Video.push({
                'id': res.insertId,
                'media_file': NewPath + new_file
              });
              $scope.tab_click('videos_div');
              is_video_true = true;
              if ($scope.view_all == 0) {
                $scope.navigateTab('add_scan', 'videos');
              } else {
                $scope.navigateTab('add_scan', 'preview_all');
              }
              DbService.syncStatusClaimsData($stateParams.claim_id, '0');
              $ionicLoading.hide();
              return;
            }
            if ($scope.media_type == 'A') {
              $scope.sowList.structuremedia.Audio.push({
                'id': res.insertId,
                'media_file': NewPath + new_file,
                'is_play': false,
                'is_stope': true
              });
              //$scope.audioPay[res.insertId] = NewPath + file_name;
              $scope.tab_click('audio_div');
              is_audio_true = true;
              if ($scope.view_all == 0) {
                $scope.navigateTab('add_scan', 'audios');
              } else {
                $scope.navigateTab('add_scan', 'preview_all');
              }
              DbService.syncStatusClaimsData($stateParams.claim_id, '0');
              $ionicLoading.hide();
              return;
            }


            $scope.dataVideoLoaded = false;
            $scope.dataAudioLoaded = false;

            $timeout(function() {
              if (is_audio_true == true) {
                $scope.dataAudioLoaded = true;
              }
              if (is_video_true == true) {
                console.log($scope.dataVideoLoaded);
                $scope.dataVideoLoaded = true;
              }
            }, 1000);
          });*/
              $ionicLoading.hide();
              $scope.getSOWDetail();
            },
            function(error) {
              console.log(error);
              $ionicLoading.hide();
            });
        });
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Add media',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
        $ionicLoading.hide();
      }
    };

    $scope.get_camaraoption = function() {
      function captureSuccess(mediaFiles) {
        console.log(mediaFiles);
        //uploadFile(mediaFiles[0]);
      }
      // Called if something bad happens.
      //
      function captureError(error) {
        var msg = 'An error occurred during capture: ' + error.code;
        //navigator.notification.alert(msg, null, 'Uh oh!');
      }

      navigator.device.capture.captureVideo(captureSuccess, captureError, {
        limit: 1
      });
    };
    $scope.media_popup_open = function(file_utl, file_type) {
      $scope.data = {};
      $scope.data.file_utl = file_utl;
      $scope.data.file_type = file_type;
      $scope.showPopupMedia(file_utl, file_type);
    };
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
  }])
  .controller('sowmanageCtrl', ['Api', '$filter', '$scope', '$state', '$stateParams', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', 'ionicDatePicker', 'ionicTimePicker', function(Api, $filter, $scope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, ionicDatePicker, ionicTimePicker) {
    $rootScope.gettasktrades(0);
  }]);
