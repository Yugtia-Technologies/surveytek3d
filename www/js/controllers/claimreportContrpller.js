//angular.module('starter.jobcontrollers', ['starter.services','ionic', 'googlechart'])
angular.module('starter.claimreportcontrollers', ['ionic'])
  .controller('claimreportCtrl', ['Api', 'DbService', 'masterTable', '$filter', '$scope', '$state', '$stateParams', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', 'ionicDatePicker', 'ionicTimePicker', function(Api, DbService, masterTable, $filter, $scope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, ionicDatePicker, ionicTimePicker) {
    $scope.back_btn = $stateParams.claim_id;
    $scope.healthsafety = {
      claim_id: $stateParams.claim_id,
      user_token: localStorage.getItem("UserToken"),
      user_id: localStorage.getItem("UserId"),
      access_and_egress: '',
      assign_cmd: '',
      customer_considerations: '',
      environments: '',
      health_safety_comments: '',
      plant_uses: '',
      substances: '',
      live_id: 0,
      id: 0
    }
    // claim report model.
    $scope.claim_report = {
      live_id: 0,
      id: 0,
      claim_id: $stateParams.claim_id,
      user_token: localStorage.getItem("UserToken"),
      user_id: localStorage.getItem("UserId"),
      property_id: {
        'id': ''
      },
      rooftype_id: {
        'id': ''
      },
      propertyage_id: {
        'id': ''
      },
      propertycondition_id: {
        "id": ''
      },
      specify_rooftype: '',
      propertycondition_id: {
        "id": ''
      },
      storey_id: {
        'id': '-1'
      },
      specify_roofmaterial: '',
      property_unoccupied: '',
      specify_storey: '',
      walltype_id: {
        'id': ''
      },
      listed_building: {
        'id': '-1'
      },
      multiple_flats: {
        'id': '-1'
      },
      roofmaterial_id: {
        'id': ''
      },
      actualperil_id: {
        'id': ''
      },
      crime_reference: '',
      roomsaffected_id: {
        'id': ''
      },
      eurotempest: {
        'id': '-1'
      },
      specify_roomafftected: '',
      inspection_performed: '',
      nirs_completed: '',
      eow_reason: '',
      recovery_opportunity: false,
      inspection_performed: '',
      nirs_completed: '',
      ta_started: '',
      ta_completed: '',
      strip_outworks: '',
      strip_outworks_completed: '',
      drying_equipment_installed: '',
      drying_equipment_removed: '',
      drying_end: '',
      aa_type: '',
      aa_start_date: '',
      aa_end_date_estimated: '',
      aa_end_date_actual: '',
      business_trading: '',
      reinstatement_type: '',
      works_started: '',
      estimated_works_completed: '',
      works_completed: '',
      customer_completion: '',
      reserves_content: '',
      reserves_building: '',
      reserves_fees: '',
      reports: '',
      requirements: '',
    };

    $scope.drying_form = {
      claim_id: $stateParams.claim_id,
      user_token: localStorage.getItem("UserToken"),
      user_id: localStorage.getItem("UserId"),
      property_address: '',
      customer_name: '',
      firstcall_reference: '',
      incident_date: '',
      drying_date: '',
      firstcall_name: '',
      firstcall_position: '',
      firstcall_signature: '',
      live_id: 0,
      id: 0
    };
    //recovery model
    $scope.recoverys = {
      claim_id: $stateParams.claim_id,
      user_token: localStorage.getItem("UserToken"),
      user_id: localStorage.getItem("UserId"),
      allegation_negligence: '',
      third_party: '',
      response_referral: '',
      followup1: '',
      followup2: '',
      aditional_followup: '',
      final_action: '',
      live_id: 0,
      id: 0
    };
    //pfi model
    $scope.pfi = {
      live_id: 0,
      claim_id: $stateParams.claim_id,
      user_token: localStorage.getItem("UserToken"),
      user_id: localStorage.getItem("UserId"),
      fraud_type: '',
      referral_reason: '',
      concerns_reason: '',
      customer_challenged: '',
      response_referral: '',
      followup1: '',
      followup2: '',
      aditional_followup: '',
      final_action: '',
      created: ''
    }
    $scope.claimReportMaster = function() { // also edit time data fill

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });

      var blank_values = [];
      promiseProperties = masterTable.getMasterTable('id,name', 'properties', '', blank_values);
      promiseProperties.then(function(result) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            console.log('Select Type of Property');
            console.log($rootScope.SelectDrp(result, 'id', 'name', 'Select Type of Property'));
            $scope.propertyList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          console.log('Select Type of Property');
          console.log($rootScope.SelectDrp(result, 'id', 'name', 'Select Type of Property'));
          $scope.propertyList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promis_requirenment = masterTable.getParameters(1);
      promis_requirenment.then(function(result) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.masterData = {
              'requirenments': result
            };
            //console.log($scope.masterData);
          });
        } else {
          $scope.masterData = {
            'requirenments': result
          };
        }
      });
      promisePropertyage = masterTable.getMasterTable('id,name', 'propertyage', '', []);
      promisePropertyage.then(function(result) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.propertyageList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.propertyageList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promisePropertyage = masterTable.getMasterTable('id,name', 'rooftype', '', blank_values);
      promisePropertyage.then(function(result) {
        result.push({
          'id': 0,
          'name': 'Other'
        });
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.rooftypeList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.rooftypeList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promisePropertycondition = masterTable.getMasterTable('id,name', 'propertycondition', '', blank_values);
      promisePropertycondition.then(function(result) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.propertyconditionList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.propertyconditionList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promiseRoofmaterial = masterTable.getMasterTable('id,name', 'roofmaterial', '', blank_values);
      promiseRoofmaterial.then(function(result) {
        result.push({
          'id': 0,
          'name': 'Other'
        });
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.roofmaterialList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.roofmaterialList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promiseWalltype = masterTable.getMasterTable('id,name', 'walltype', '', blank_values);
      promiseWalltype.then(function(result) {
        result.push({
          'id': 0,
          'name': 'Other'
        });
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.wallList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.wallList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });

      //$scope.rooftypeList = $rootScope.SelectDrp($scope.masterData.roof_type, 'id', 'name', 'Select Roof Type');
      //$scope.propertyageList = $rootScope.SelectDrp($scope.masterData.property_age, 'id', 'name', 'Select Age of Property');
      //$scope.propertyconditionList = $rootScope.SelectDrp($scope.masterData.property_condition, 'id', 'name', 'Select Property Condition');
      //$scope.roofmaterialList = $rootScope.SelectDrp($scope.masterData.roof_material, 'id', 'name', 'Select Property Condition');
      //$scope.wallList = $rootScope.SelectDrp($scope.masterData.walltype, 'id', 'name', 'Select Wall Type');
      $scope.buildingList = $rootScope.SelectDrp([{
        'id': '-1',
        'name': 'Select'
      }, {
        'id': 'Yes',
        'name': 'Yes'
      }, {
        'id': 'No',
        'name': 'No'
      }], 'id', 'name', '');
      $scope.multipleList = $rootScope.SelectDrp([{
        'id': '-1',
        'name': 'Select'
      }, {
        'id': '1',
        'name': '1'
      }, {
        'id': '2',
        'name': '2'
      }, {
        'id': '3',
        'name': '3'
      }, {
        'id': '4',
        'name': '4'
      }, {
        'id': '5',
        'name': '5'
      }, {
        'id': '6',
        'name': '6'
      }, {
        'id': '7',
        'name': '7'
      }, {
        'id': '8',
        'name': '8'
      }, {
        'id': '9',
        'name': '9'
      }, {
        'id': '10',
        'name': '10'
      }, {
        'id': '0',
        'name': 'Other'
      }], 'id', 'name', '');
      $scope.storeyList = $rootScope.SelectDrp([{
        'id': '-1',
        'name': 'Select'
      }, {
        'id': '2',
        'name': '2'
      }, {
        'id': '3',
        'name': '3'
      }, {
        'id': '4',
        'name': '4'
      }, {
        'id': '5',
        'name': '5'
      }, {
        'id': '6',
        'name': '6'
      }, {
        'id': '7',
        'name': '7'
      }, {
        'id': '8',
        'name': '8'
      }, {
        'id': '9',
        'name': '9'
      }, {
        'id': '10',
        'name': '10'
      }, {
        'id': '0',
        'name': 'Other'
      }], 'id', 'name', '');
      promiseActualperil = masterTable.getMasterTable('id,name', 'actualperil', '', blank_values);
      promiseActualperil.then(function(result) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.actualperilList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.actualperilList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
      });
      promiseRoomsaffected = masterTable.getMasterTable('id,name', 'roomsaffected', '', blank_values);
      promiseRoomsaffected.then(function(result) {
        result.push({
          "id": 0,
          'name': 'Other'
        })
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.roomsaffectedList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
          });
        } else {
          $scope.roomsaffectedList = $rootScope.SelectDrp(result, 'id', 'name', 'Select');
        }
        $ionicLoading.hide();
      });
      //$scope.actualperilList = $rootScope.SelectDrp($scope.masterData.actualperil, 'id', 'name', 'Select Actual Peril');
      //$scope.roomsaffectedList = $rootScope.SelectDrp($scope.masterData.rooms_affected, 'id', 'name', 'Select Number of Rooms Affected');
      $scope.eurotempestList = $rootScope.SelectDrp([{
        'id': '-1',
        'name': 'Select'
      }, {
        'id': 'Yes',
        'name': 'Yes'
      }, {
        'id': 'No',
        'name': 'No'
      }], 'id', 'name', '');



      $scope.get_recovery_form_detail('claim_report');
    }
    $scope.healthSafetyMaster = function() { // also edit time data fill

      //alert($stateParams.room_id);
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });

      /*var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "master/health_masters";
      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          console.log(response);
          if (response.data.status == 1) {
            $scope.masterData = response.data.response;

            $scope.get_recovery_form_detail('healthsafety_form');
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Report',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Report',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );*/
      $scope.environment = null;
      $scope.substances = null;
      $scope.access_and_egress = null;
      $scope.use_of_plant_during_survey = null;
      $scope.assign_hs = null;
      $scope.part2 = null;

      promis_environment = masterTable.getParameters(3);
      promis_substances = masterTable.getParameters(4);
      promis_access_and_egress = masterTable.getParameters(5);
      promis_use_of_plant_during_survey = masterTable.getParameters(6);
      promis_assign_hs = masterTable.getParameters(7);
      promis_part2 = masterTable.getParameters(8);

      promis_environment.then(function(environment) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.environment = environment;
          });
        } else {
          $scope.environment = environment;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
      });
      promis_substances.then(function(substances) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.substances = substances;
          });
        } else {
          $scope.substances = substances;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
      });
      promis_access_and_egress.then(function(access_and_egress) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.access_and_egress = access_and_egress;
          });
        } else {
          $scope.access_and_egress = access_and_egress;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
        $ionicLoading.hide();
      });
      promis_use_of_plant_during_survey.then(function(use_of_plant_during_survey) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.use_of_plant_during_survey = use_of_plant_during_survey;
          });
        } else {
          $scope.use_of_plant_during_survey = use_of_plant_during_survey;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
        $ionicLoading.hide();
      });
      promis_assign_hs.then(function(assign_hs) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.assign_hs = assign_hs;
          });
        } else {
          $scope.assign_hs = assign_hs;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
        $ionicLoading.hide();
      });
      promis_part2.then(function(part2) {
        if (!$scope.$$phase) {
          $scope.$apply(function() {
            $scope.part2 = part2;
          });
        } else {
          $scope.part2 = part2;
        }
        if ($scope.environment != null && $scope.substances != null && $scope.access_and_egress != null && $scope.use_of_plant_during_survey != null && $scope.assign_hs != null && $scope.part2 != null) {
          $scope.get_recovery_form_detail('healthsafety_form');
        }
        $ionicLoading.hide();
      });
    }
    $scope.check_box_status_healthSafety = function(id_key, model_index) {
      if ($scope.healthsafety[model_index] == null) {
        $scope.healthsafety[model_index] = '';
      }
      if ($scope.healthsafety[model_index].indexOf(id_key + ',') >= 0) {
        $scope.healthsafety[model_index] = $scope.healthsafety[model_index].replace(id_key + ',', '');
      } else if ($scope.healthsafety[model_index].indexOf(id_key) >= 0) {
        $scope.healthsafety[model_index] = $scope.healthsafety[model_index].replace(id_key, '');
      } else {
        $scope.healthsafety[model_index] += ',' + id_key;
      }
      console.log($scope.healthsafety[model_index]);
    }

    $scope.clearCanvas = function() {
      $scope.signaturePad.clear();
    }

    $scope.saveCanvas = function() {
      var sigImg = $scope.signaturePad.toDataURL();
      $scope.signature = sigImg;
    }
    $scope.get_recovery_form_detail = function(form_type) {
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      if (localStorage.getItem("UserType") == 1) {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'claim_id': $stateParams.claim_id,
          'user_token': localStorage.getItem("UserToken")
        });

        var urlBase = Api.urlBase + "claims/details";
        var req = {
          headers: Api.HEADERS
        };

        $http.post(urlBase, pdata, req).then(
          function(response) {
            console.log(response);
            if (response.data.status == 1) {
              $scope.claim_data = response.data.response;
              if (form_type == 'recovery') {
                if ($scope.claim_data.recoveryforms.length > 0) {
                  $scope.recoverys = $scope.claim_data.recoveryforms[0];
                }
                $scope.recoverys.claim_id = $stateParams.claim_id;
                $scope.recoverys.user_token = localStorage.getItem("UserToken");
                $scope.recoverys.user_id = localStorage.getItem("UserId");
              } else if (form_type == 'pfi') {
                if ($scope.claim_data.pfiforms.length > 0) {
                  $scope.pfi = $scope.claim_data.pfiforms[0];
                }
                $scope.pfi.claim_id = $stateParams.claim_id;
                $scope.pfi.user_token = localStorage.getItem("UserToken");
                $scope.pfi.user_id = localStorage.getItem("UserId");
              } else if (form_type == 'claim_report') {
                if ($scope.claim_data.claimreports.length > 0) {
                  $scope.claim_report = $scope.claim_data.claimreports[0];
                  if (localStorage.getItem('UserType') == 1) {
                    $rootScope.app.is_admin = true;
                  } else {
                    $rootScope.app.is_admin = false;
                  }
                  $scope.claim_report.claim_id = $stateParams.claim_id,
                    $scope.claim_report.user_token = localStorage.getItem("UserToken"),
                    $scope.claim_report.user_id = localStorage.getItem("UserId"),
                    $scope.claim_report.property_id = {
                      "id": $scope.claim_report.property_id,
                      'name': ''
                    };
                  $scope.claim_report.rooftype_id = {
                    "id": $scope.claim_report.rooftype_id,
                    'name': ''
                  };
                  $scope.claim_report.propertyage_id = {
                    "id": $scope.claim_report.propertyage_id,
                    'name': ''
                  };
                  $scope.claim_report.propertycondition_id = {
                    "id": $scope.claim_report.propertycondition_id,
                    'name': ''
                  };
                  $scope.claim_report.storey_id = {
                    "id": $scope.claim_report.storey_id,
                    'name': ''
                  };
                  $scope.claim_report.walltype_id = {
                    "id": $scope.claim_report.walltype_id,
                    'name': ''
                  };
                  $scope.claim_report.listed_building = {
                    "id": $scope.claim_report.listed_building,
                    'name': ''
                  };
                  $scope.claim_report.multiple_flats = {
                    "id": $scope.claim_report.multiple_flats,
                    'name': ''
                  };
                  $scope.claim_report.roofmaterial_id = {
                    "id": $scope.claim_report.roofmaterial_id,
                    'name': ''
                  };

                  $scope.claim_report.actualperil_id = {
                    "id": $scope.claim_report.actualperil_id,
                    'name': ''
                  };
                  $scope.claim_report.roomsaffected_id = {
                    "id": $scope.claim_report.roomsaffected_id,
                    'name': ''
                  };
                  $scope.claim_report.eurotempest = {
                    "id": $scope.claim_report.eurotempest,
                    'name': ''
                  };


                  if ($scope.claim_report.recovery_opportunity == 1) {
                    $scope.claim_report.recovery_opportunity = true;
                  } else {
                    $scope.claim_report.recovery_opportunity = false;
                  }

                }

              } else if (form_type == 'drying_form') {
                console.log($scope.claim_data);
                if ($scope.claim_data.drying.length > 0) {
                  $scope.drying_form = $scope.claim_data.drying[0];
                }
                $scope.drying_form.claim_id = $stateParams.claim_id,
                  $scope.drying_form.user_token = localStorage.getItem("UserToken"),
                  $scope.drying_form.user_id = localStorage.getItem("UserId"),

                  $scope.canvas = document.getElementById('signatureCanvas');
                $scope.signaturePad = new SignaturePad($scope.canvas);
              } else if (form_type == 'healthsafety_form') {
                console.log($scope.claim_data);
                if ($scope.claim_data.healthsafety.length > 0) {
                  $scope.healthsafety = $scope.claim_data.healthsafety[0];
                }
                $scope.drying_form.claim_id = $stateParams.claim_id;
                $scope.drying_form.user_token = localStorage.getItem("UserToken");
                $scope.drying_form.user_id = localStorage.getItem("UserId");
              }
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Claim Report',
                cssClass: 'notification',
                template: response.data.message
              });
            }
            $ionicLoading.hide();
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Report',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          }
        );
      } else {
        if (form_type == 'pfi') {
          ReportClaimPFI = DbService.getClaimPFI($stateParams.claim_id);
          ReportClaimDetail = DbService.getClaimDetail($stateParams.claim_id);
          ReportClaimPFI.then(function(ClaimPFI) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.pfi = ClaimPFI[$stateParams.claim_id];
                $scope.pfi.claim_id = $stateParams.claim_id;
                $scope.pfi.user_token = localStorage.getItem("UserToken");
                $scope.pfi.user_id = localStorage.getItem("UserId");
              });
            } else {
              $scope.pfi = ClaimPFI[$stateParams.claim_id];
              $scope.pfi.claim_id = $stateParams.claim_id;
              $scope.pfi.user_token = localStorage.getItem("UserToken");
              $scope.pfi.user_id = localStorage.getItem("UserId");
            }
            $ionicLoading.hide();
          });

          ReportClaimDetail.then(function(claim_data) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_data = claim_data;
              });
            } else {
              $scope.claim_data = claim_data;
            }
          });
        } else if (form_type == 'recovery') {
          ReportClaimRecovery = DbService.getClaimRecovery($stateParams.claim_id);
          ReportClaimDetail = DbService.getClaimDetail($stateParams.claim_id);
          ReportClaimRecovery.then(function(ClaimRecovery) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.recoverys = ClaimRecovery[$stateParams.claim_id];
                $scope.recoverys.claim_id = $stateParams.claim_id;
                $scope.recoverys.user_token = localStorage.getItem("UserToken");
                $scope.recoverys.user_id = localStorage.getItem("UserId");
              });
            } else {
              $scope.recoverys = ClaimRecovery[$stateParams.claim_id];
              $scope.recoverys.claim_id = $stateParams.claim_id;
              $scope.recoverys.user_token = localStorage.getItem("UserToken");
              $scope.recoverys.user_id = localStorage.getItem("UserId");
            }
            $ionicLoading.hide();
          });
          ReportClaimDetail.then(function(claim_data) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_data = claim_data;
              });
            } else {
              $scope.claim_data = claim_data;
            }
          });
        } else if (form_type == 'drying_form') {
          ReportClaimDrying = DbService.getClaimDrying($stateParams.claim_id);
          ReportClaimDetail = DbService.getClaimDetail($stateParams.claim_id);
          ReportClaimDrying.then(function(ClaimDrying) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.drying_form = ClaimDrying[$stateParams.claim_id];
                $scope.drying_form.claim_id = $stateParams.claim_id;
                $scope.drying_form.user_token = localStorage.getItem("UserToken");
                $scope.drying_form.user_id = localStorage.getItem("UserId");

                $scope.canvas = document.getElementById('signatureCanvas');
                $scope.signaturePad = new SignaturePad($scope.canvas);
              });
            } else {
              $scope.drying_form = ClaimDrying[$stateParams.claim_id];
              $scope.drying_form.claim_id = $stateParams.claim_id;
              $scope.drying_form.user_token = localStorage.getItem("UserToken");
              $scope.drying_form.user_id = localStorage.getItem("UserId");

              $scope.canvas = document.getElementById('signatureCanvas');
              $scope.signaturePad = new SignaturePad($scope.canvas);
            }
            $ionicLoading.hide();
          });

          ReportClaimDetail.then(function(claim_data) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_data = claim_data;
              });
            } else {
              $scope.claim_data = claim_data;
            }
          });
        } else if (form_type == 'healthsafety_form') {
          ReportClaimHealthsafety = DbService.getClaimHealthSafety($stateParams.claim_id);
          ReportClaimDetail = DbService.getClaimDetail($stateParams.claim_id);
          ReportClaimHealthsafety.then(function(ClaimDrying) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.healthsafety = ClaimDrying[$stateParams.claim_id];
                $scope.healthsafety.claim_id = $stateParams.claim_id;
                $scope.healthsafety.user_token = localStorage.getItem("UserToken");
                $scope.healthsafety.user_id = localStorage.getItem("UserId");
              });
            } else {
              $scope.healthsafety = ClaimDrying[$stateParams.claim_id];
              $scope.healthsafety.claim_id = $stateParams.claim_id;
              $scope.healthsafety.user_token = localStorage.getItem("UserToken");
              $scope.healthsafety.user_id = localStorage.getItem("UserId");
            }
            $ionicLoading.hide();
          });

          ReportClaimDetail.then(function(claim_data) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_data = claim_data;
              });
            } else {
              $scope.claim_data = claim_data;
            }
          });
        } else if (form_type == 'claim_report') {
          ReportClaimReport = DbService.getClaimClaimReport($stateParams.claim_id);
          ReportClaimDetail = DbService.getClaimDetail($stateParams.claim_id);
          ReportClaimReport.then(function(ClaimReport) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_report.claim_id = $stateParams.claim_id;
                $scope.claim_report.user_token = localStorage.getItem("UserToken");
                $scope.claim_report.user_id = localStorage.getItem("UserId");
                if (Object.keys(ClaimReport[$stateParams.claim_id]).length > 0) {
                  $scope.claim_report = ClaimReport[$stateParams.claim_id];

                  $scope.claim_report.property_id = {
                    "id": $scope.claim_report.property_id,
                    'name': ''
                  };
                  $scope.claim_report.rooftype_id = {
                    "id": $scope.claim_report.rooftype_id,
                    'name': ''
                  };
                  $scope.claim_report.propertyage_id = {
                    "id": $scope.claim_report.propertyage_id,
                    'name': ''
                  };
                  $scope.claim_report.propertycondition_id = {
                    "id": $scope.claim_report.propertycondition_id,
                    'name': ''
                  };
                  $scope.claim_report.storey_id = {
                    "id": $scope.claim_report.storey_id,
                    'name': ''
                  };
                  $scope.claim_report.walltype_id = {
                    "id": $scope.claim_report.walltype_id,
                    'name': ''
                  };
                  $scope.claim_report.listed_building = {
                    "id": $scope.claim_report.listed_building,
                    'name': ''
                  };
                  $scope.claim_report.multiple_flats = {
                    "id": $scope.claim_report.multiple_flats,
                    'name': ''
                  };
                  $scope.claim_report.roofmaterial_id = {
                    "id": $scope.claim_report.roofmaterial_id,
                    'name': ''
                  };

                  $scope.claim_report.actualperil_id = {
                    "id": $scope.claim_report.actualperil_id,
                    'name': ''
                  };
                  $scope.claim_report.roomsaffected_id = {
                    "id": $scope.claim_report.roomsaffected_id,
                    'name': ''
                  };
                  $scope.claim_report.eurotempest = {
                    "id": $scope.claim_report.eurotempest,
                    'name': ''
                  };

                  if ($scope.claim_report.recovery_opportunity == 1) {
                    $scope.claim_report.recovery_opportunity = true;
                  } else {
                    $scope.claim_report.recovery_opportunity = false;
                  }
                }
              });
            } else {

              $scope.claim_report.claim_id = $stateParams.claim_id;
              $scope.claim_report.user_token = localStorage.getItem("UserToken");
              $scope.claim_report.user_id = localStorage.getItem("UserId");
              if (Object.keys(ClaimReport[$stateParams.claim_id]).length > 0) {
                $scope.claim_report = ClaimReport[$stateParams.claim_id];

                $scope.claim_report.property_id = {
                  "id": $scope.claim_report.property_id,
                  'name': ''
                };
                $scope.claim_report.rooftype_id = {
                  "id": $scope.claim_report.rooftype_id,
                  'name': ''
                };
                $scope.claim_report.propertyage_id = {
                  "id": $scope.claim_report.propertyage_id,
                  'name': ''
                };
                $scope.claim_report.propertycondition_id = {
                  "id": $scope.claim_report.propertycondition_id,
                  'name': ''
                };
                $scope.claim_report.storey_id = {
                  "id": $scope.claim_report.storey_id,
                  'name': ''
                };
                $scope.claim_report.walltype_id = {
                  "id": $scope.claim_report.walltype_id,
                  'name': ''
                };
                $scope.claim_report.listed_building = {
                  "id": $scope.claim_report.listed_building,
                  'name': ''
                };
                $scope.claim_report.multiple_flats = {
                  "id": $scope.claim_report.multiple_flats,
                  'name': ''
                };
                $scope.claim_report.roofmaterial_id = {
                  "id": $scope.claim_report.roofmaterial_id,
                  'name': ''
                };

                $scope.claim_report.actualperil_id = {
                  "id": $scope.claim_report.actualperil_id,
                  'name': ''
                };
                $scope.claim_report.roomsaffected_id = {
                  "id": $scope.claim_report.roomsaffected_id,
                  'name': ''
                };
                $scope.claim_report.eurotempest = {
                  "id": $scope.claim_report.eurotempest,
                  'name': ''
                };

                if ($scope.claim_report.recovery_opportunity == 1) {
                  $scope.claim_report.recovery_opportunity = true;
                } else {
                  $scope.claim_report.recovery_opportunity = false;
                }
              }
            }
            $ionicLoading.hide();
          });

          ReportClaimDetail.then(function(claim_data) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.claim_data = claim_data;
              });
            } else {
              $scope.claim_data = claim_data;
            }
          });
        }
      }
    };


    $scope.manageRecovery = function(addRecovery) {
      if (addRecovery.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        if (check_connection()) {
          pdata = [];
          $scope.ServererrMsg = false;
          $scope.recoverys.live_id = $scope.recoverys.id;
          delete $scope.recoverys.id;

          var pdata = jQuery.param($scope.recoverys);
          console.log(pdata);

          var urlBase = Api.urlBase + "recoveryform/add";

          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              var alertTitle = "Recovery from";
              if (response.data.status == 1) {
                $state.go('app.detailview', {
                  'id': $stateParams.claim_id
                });
                if ($rootScope.app.is_admin == false) {
                  DbService.addClaimRecovery($scope.recoverys);
                  $state.go('app.claimview', {
                    'id': $stateParams.claim_id
                  });
                } else {
                  $state.go('app.detailview', {
                    'id': $stateParams.claim_id
                  });
                }

              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          if (localStorage.getItem("UserType") != 1) {
            $scope.recoverys.live_id = $scope.recoverys.id;
            $scope.recoverys.created = Api.formatDate('');
            DbService.addClaimRecovery($scope.recoverys);

            DbService.syncStatusClaimsData($stateParams.claim_id, 0);
            $state.go('app.claimview', {
              'id': $stateParams.claim_id
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }

    $scope.managePfi = function(addPfi) {
      if (addPfi.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        if (check_connection()) {
          pdata = [];
          $scope.ServererrMsg = false;
          $scope.pfi.live_id = $scope.pfi.id;
          if ($scope.pfi.id == '' || typeof $scope.pfi.id == 'undefined') {
            delete $scope.pfi.id;
          }
          $scope.pfi.created = Api.formatDate('');
          var pdata = jQuery.param($scope.pfi);

          var urlBase = Api.urlBase + "pfiform/add";

          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {

              $ionicLoading.hide();
              var alertTitle = "PFI from";
              if (response.data.status == 1) {
                $scope.pfi.live_id = $scope.pfi.id;
                if ($rootScope.app.is_admin == false) {
                  DbService.addClaimPFI($scope.pfi);
                  $state.go('app.claimview', {
                    'id': $stateParams.claim_id
                  });
                } else {
                  $state.go('app.detailview', {
                    'id': $stateParams.claim_id
                  });
                }
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          if (localStorage.getItem("UserType") != 1) {
            $scope.pfi.live_id = $scope.pfi.id;
            $scope.pfi.created = Api.formatDate('');
            DbService.addClaimPFI($scope.pfi);
            DbService.syncStatusClaimsData($stateParams.claim_id, 0);
            $state.go('app.claimview', {
              'id': $stateParams.claim_id
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }
    $scope.manageHealthSafety = function(addAddHealthSafety) {
      if (addAddHealthSafety.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        if (check_connection()) {
          pdata = [];
          $scope.ServererrMsg = false;
          $scope.healthsafety.live_id = $scope.healthsafety.id;
          delete $scope.healthsafety.id;
          $scope.healthsafety.user_token = localStorage.getItem("UserToken");
          $scope.healthsafety.user_id = localStorage.getItem("UserId");
          var pdata = jQuery.param($scope.healthsafety);
          console.log(pdata);

          var urlBase = Api.urlBase + "healthsafety/add";

          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();


              var alertTitle = "Health & Safety from";
              if (response.data.status == 1) {
                if ($rootScope.app.is_admin == false) {
                  DbService.addClaimHealthSafety($scope.healthsafety);
                  //DbService.addClaimPFI($scope.pfi);
                  $state.go('app.claimview', {
                    'id': $stateParams.claim_id
                  });
                } else {
                  $state.go('app.detailview', {
                    'id': $stateParams.claim_id
                  });
                }
                /*$state.go('app.detailview', {
                  'id': $stateParams.claim_id
                });*/
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          if (localStorage.getItem("UserType") != 1) {
            $scope.healthsafety.live_id = $scope.healthsafety.id;
            $scope.healthsafety.created = Api.formatDate('');
            DbService.addClaimHealthSafety($scope.healthsafety);

            DbService.syncStatusClaimsData($stateParams.claim_id, 0);
            $state.go('app.claimview', {
              'id': $stateParams.claim_id
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }
    $scope.manageDrying = function(addDrying) {
      if (addDrying.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;
        $scope.drying_form.live_id = $scope.drying_form.id;
        if ($scope.drying_form.id == '' || typeof $scope.drying_form.id == 'undefined') {
          delete $scope.drying_form.id;
        }
        var alertTitle = "Drying from";
        var error_message = [];
        if ($scope.drying_form.property_address == '') {
          error_message.push('Please entere property address');
        }
        if ($scope.drying_form.customer_name == '') {
          error_message.push('Please entere customer name');
        }
        if ($scope.drying_form.firstcall_name == '') {
          error_message.push('Please entere firstcall name');
        }
        if ($scope.drying_form.firstcall_position == '') {
          error_message.push('Please entere firstcall position');
        }
        if (error_message.length > 0) {
          $ionicLoading.hide();
          message = "Please fill up required fields or check validation";
          message += '<br/>' + error_message.join('</br>');
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: message
          });
          alertPopup.then(function(res) {});
          return;
        }
        //console.log($scope.drying_form.firstcall_signature);
        if ($scope.signaturePad.toDataURL() != "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAC0CAYAAAAuPxHvAAAAAXNSR0IArs4c6QAAA8VJREFUeAHt0IEAAAAAw6D5Ux/khVBhwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAwYMGDBgwIABAy8DA0yhAAG/dsitAAAAAElFTkSuQmCC" ||
          $scope.drying_form.firstcall_signature == '') {
          $scope.drying_form.firstcall_signature = $scope.signaturePad.toDataURL();
          console.log("come- in if");
        }
        if (check_connection()) {
          var pdata = jQuery.param($scope.drying_form);
          var urlBase = Api.urlBase + "drying/add";

          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();

              if (response.data.status == 1) {

                $scope.drying_form.live_id = $scope.drying_form.id;
                if ($rootScope.app.is_admin == false) {
                  DbService.addClaimDrying($scope.drying_form);
                  $state.go('app.claimview', {
                    'id': $stateParams.claim_id
                  });
                } else {
                  $state.go('app.detailview', {
                    'id': $stateParams.claim_id
                  });
                }
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          if (localStorage.getItem("UserType") != 1) {

            $scope.drying_form.live_id = $scope.drying_form.id;
            $scope.drying_form.created = Api.formatDate('');
            DbService.addClaimDrying($scope.drying_form);

            DbService.syncStatusClaimsData($stateParams.claim_id, 0);
            $state.go('app.claimview', {
              'id': $stateParams.claim_id
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }
    $scope.manageClaimReport = function(addClaimReport, position) {
      if (addClaimReport.$valid) {
        console.log($scope.claim_report);
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        var alertTitle = "Claim report";
        var error_message = [];
        if (position == 1) {
          if ($scope.claim_report.property_id['id'] <= 0 || $scope.claim_report.property_id['id'] == '') {
            error_message.push('Please select property');
          }
          if ($scope.claim_report.propertyage_id['id'] <= 0 || $scope.claim_report.propertyage_id['id'] == '') {
            error_message.push('Please select property age');
          }
          if ($scope.claim_report.propertycondition_id['id'] <= 0 || $scope.claim_report.propertycondition_id['id'] == '') {
            error_message.push('Please select property condition');
          }
          if (error_message.length > 0) {
            $ionicLoading.hide();
            message = "Please fill up required fields or check validation";
            message += '<br/>' + error_message.join('</br>');
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: message
            });
            alertPopup.then(function(res) {});
            return;
          }
        }
        $scope.claim_report.user_id = localStorage.getItem("UserId");
        $scope.claim_report.user_token = localStorage.getItem("UserToken");
        $scope.claim_report.property_id = $scope.claim_report.property_id['id'];
        $scope.claim_report.rooftype_id = $scope.claim_report.rooftype_id['id'];
        $scope.claim_report.propertyage_id = $scope.claim_report.propertyage_id['id'];
        $scope.claim_report.propertycondition_id = $scope.claim_report.propertycondition_id['id'];
        $scope.claim_report.storey_id = $scope.claim_report.storey_id['id'];
        $scope.claim_report.walltype_id = $scope.claim_report.walltype_id['id'];
        $scope.claim_report.listed_building = $scope.claim_report.listed_building['id'];
        $scope.claim_report.multiple_flats = $scope.claim_report.multiple_flats['id'];
        $scope.claim_report.roofmaterial_id = $scope.claim_report.roofmaterial_id['id'];

        $scope.claim_report.actualperil_id = $scope.claim_report.actualperil_id['id'];
        $scope.claim_report.roomsaffected_id = $scope.claim_report.roomsaffected_id['id'];
        $scope.claim_report.eurotempest = $scope.claim_report.eurotempest['id'];
        if ($scope.claim_report.recovery_opportunity == true) {
          $scope.claim_report.recovery_opportunity = 1;
        } else {
          $scope.claim_report.recovery_opportunity = 0;
        }

        pdata = [];
        $scope.ServererrMsg = false;
        if (check_connection()) {
          $scope.claim_report.live_id = $scope.claim_report.id;
          if ($scope.claim_report.id == '' || typeof $scope.claim_report.id == 'undefined') {
            delete $scope.claim_report.id;
          }
          var pdata = jQuery.param($scope.claim_report);
          console.log(pdata);

          var urlBase = Api.urlBase + "claimreports/add";

          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();

              if ($rootScope.app.is_admin == false) {
                return_promis = DbService.addClaimreports($scope.claim_report);
                return_promis.then(function() {
                  if (!$scope.$$phase) {
                    $scope.$apply(function() {
                      $scope.claim_report.property_id = {
                        "id": $scope.claim_report.property_id,
                        'name': ''
                      };
                      $scope.claim_report.rooftype_id = {
                        "id": $scope.claim_report.rooftype_id,
                        'name': ''
                      };
                      $scope.claim_report.propertyage_id = {
                        "id": $scope.claim_report.propertyage_id,
                        'name': ''
                      };
                      $scope.claim_report.propertycondition_id = {
                        "id": $scope.claim_report.propertycondition_id,
                        'name': ''
                      };
                      $scope.claim_report.storey_id = {
                        "id": $scope.claim_report.storey_id,
                        'name': ''
                      };
                      $scope.claim_report.walltype_id = {
                        "id": $scope.claim_report.walltype_id,
                        'name': ''
                      };
                      $scope.claim_report.listed_building = {
                        "id": $scope.claim_report.listed_building,
                        'name': ''
                      };
                      $scope.claim_report.multiple_flats = {
                        "id": $scope.claim_report.multiple_flats,
                        'name': ''
                      };
                      $scope.claim_report.roofmaterial_id = {
                        "id": $scope.claim_report.roofmaterial_id,
                        'name': ''
                      };

                      $scope.claim_report.actualperil_id = {
                        "id": $scope.claim_report.actualperil_id,
                        'name': ''
                      };
                      $scope.claim_report.roomsaffected_id = {
                        "id": $scope.claim_report.roomsaffected_id,
                        'name': ''
                      };
                      $scope.claim_report.eurotempest = {
                        "id": $scope.claim_report.eurotempest,
                        'name': ''
                      };
                      if ($scope.claim_report.recovery_opportunity == 1) {
                        $scope.claim_report.recovery_opportunity = true;
                      } else {
                        $scope.claim_report.recovery_opportunity = false;
                      }
                    });
                  } else {
                    $scope.claim_report.property_id = {
                      "id": $scope.claim_report.property_id,
                      'name': ''
                    };
                    $scope.claim_report.rooftype_id = {
                      "id": $scope.claim_report.rooftype_id,
                      'name': ''
                    };
                    $scope.claim_report.propertyage_id = {
                      "id": $scope.claim_report.propertyage_id,
                      'name': ''
                    };
                    $scope.claim_report.propertycondition_id = {
                      "id": $scope.claim_report.propertycondition_id,
                      'name': ''
                    };
                    $scope.claim_report.storey_id = {
                      "id": $scope.claim_report.storey_id,
                      'name': ''
                    };
                    $scope.claim_report.walltype_id = {
                      "id": $scope.claim_report.walltype_id,
                      'name': ''
                    };
                    $scope.claim_report.listed_building = {
                      "id": $scope.claim_report.listed_building,
                      'name': ''
                    };
                    $scope.claim_report.multiple_flats = {
                      "id": $scope.claim_report.multiple_flats,
                      'name': ''
                    };
                    $scope.claim_report.roofmaterial_id = {
                      "id": $scope.claim_report.roofmaterial_id,
                      'name': ''
                    };

                    $scope.claim_report.actualperil_id = {
                      "id": $scope.claim_report.actualperil_id,
                      'name': ''
                    };
                    $scope.claim_report.roomsaffected_id = {
                      "id": $scope.claim_report.roomsaffected_id,
                      'name': ''
                    };
                    $scope.claim_report.eurotempest = {
                      "id": $scope.claim_report.eurotempest,
                      'name': ''
                    };
                    if ($scope.claim_report.recovery_opportunity == 1) {
                      $scope.claim_report.recovery_opportunity = true;
                    } else {
                      $scope.claim_report.recovery_opportunity = false;
                    }
                  }
                });
              } else {
                $scope.claim_report.property_id = {
                  "id": $scope.claim_report.property_id,
                  'name': ''
                };
                $scope.claim_report.rooftype_id = {
                  "id": $scope.claim_report.rooftype_id,
                  'name': ''
                };
                $scope.claim_report.propertyage_id = {
                  "id": $scope.claim_report.propertyage_id,
                  'name': ''
                };
                $scope.claim_report.propertycondition_id = {
                  "id": $scope.claim_report.propertycondition_id,
                  'name': ''
                };
                $scope.claim_report.storey_id = {
                  "id": $scope.claim_report.storey_id,
                  'name': ''
                };
                $scope.claim_report.walltype_id = {
                  "id": $scope.claim_report.walltype_id,
                  'name': ''
                };
                $scope.claim_report.listed_building = {
                  "id": $scope.claim_report.listed_building,
                  'name': ''
                };
                $scope.claim_report.multiple_flats = {
                  "id": $scope.claim_report.multiple_flats,
                  'name': ''
                };
                $scope.claim_report.roofmaterial_id = {
                  "id": $scope.claim_report.roofmaterial_id,
                  'name': ''
                };

                $scope.claim_report.actualperil_id = {
                  "id": $scope.claim_report.actualperil_id,
                  'name': ''
                };
                $scope.claim_report.roomsaffected_id = {
                  "id": $scope.claim_report.roomsaffected_id,
                  'name': ''
                };
                $scope.claim_report.eurotempest = {
                  "id": $scope.claim_report.eurotempest,
                  'name': ''
                };
                if ($scope.claim_report.recovery_opportunity == 1) {
                  $scope.claim_report.recovery_opportunity = true;
                } else {
                  $scope.claim_report.recovery_opportunity = false;
                }
              }

              if (response.data.status == 1) {
                if (position == 1) {
                  $("#form-propertydetail-tab").addClass('form-hide').removeClass('form-show');
                  $("#form-loss-tab").addClass('form-show').removeClass('form-hide');
                  $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                } else if (position == 2) {
                  $("#form-loss-tab").addClass('form-hide').removeClass('form-show');
                  $("#form-keysystem-tab").addClass('form-show').removeClass('form-hide');
                  $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                } else if (position == 3) {
                  $("#form-keysystem-tab").addClass('form-hide').removeClass('form-show');
                  $("#form-report-tab").addClass('form-show').removeClass('form-hide');
                  $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                } else if (position == 4) {
                  $("#form-report-tab").addClass('form-hide').removeClass('form-show');
                  $("#form-requirement-tab").addClass('form-show').removeClass('form-hide');
                  $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                } else {
                  if ($rootScope.app.is_admin == false) {
                    $state.go('app.claimview', {
                      'id': $stateParams.claim_id
                    });
                  } else {
                    $state.go('app.detailview', {
                      'id': $stateParams.claim_id
                    });
                  }
                }
                $(".tab-container li").removeClass("selected");
                $(".tab-container li").eq(position).addClass('selected');
                //console.log($scope.claim_report);
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();

                });
              }

            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          if (localStorage.getItem("UserType") != 1) {
            $scope.claim_report.live_id = $scope.claim_report.id;
            $scope.claim_report.created = Api.formatDate('');
            return_promis = DbService.addClaimreports($scope.claim_report);

            DbService.syncStatusClaimsData($stateParams.claim_id, '0');

            /*$state.go('app.claimview', {
              'id': $stateParams.claim_id
            });*/

            if (position == 1) {
              $("#form-propertydetail-tab").addClass('form-hide').removeClass('form-show');
              $("#form-loss-tab").addClass('form-show').removeClass('form-hide');
              $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
            } else if (position == 2) {
              $("#form-loss-tab").addClass('form-hide').removeClass('form-show');
              $("#form-keysystem-tab").addClass('form-show').removeClass('form-hide');
              $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
            } else if (position == 3) {
              $("#form-keysystem-tab").addClass('form-hide').removeClass('form-show');
              $("#form-report-tab").addClass('form-show').removeClass('form-hide');
              $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
            } else if (position == 4) {
              $("#form-report-tab").addClass('form-hide').removeClass('form-show');
              $("#form-requirement-tab").addClass('form-show').removeClass('form-hide');
              $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
            } else {
              if (localStorage.getItem('UserType') != 1) {
                $state.go('app.claimview', {
                  'id': $stateParams.claim_id
                });
              } else {
                $state.go('app.detailview', {
                  'id': $stateParams.claim_id
                });
              }
            }
            $(".tab-container li").removeClass("selected");
            $(".tab-container li").eq(position).addClass('selected');
            return_promis.then(function() {
              if (!$scope.$$phase) {
                $scope.$apply(function() {
                  $scope.claim_report.property_id = {
                    "id": $scope.claim_report.property_id,
                    'name': ''
                  };
                  $scope.claim_report.rooftype_id = {
                    "id": $scope.claim_report.rooftype_id,
                    'name': ''
                  };
                  $scope.claim_report.propertyage_id = {
                    "id": $scope.claim_report.propertyage_id,
                    'name': ''
                  };
                  $scope.claim_report.propertycondition_id = {
                    "id": $scope.claim_report.propertycondition_id,
                    'name': ''
                  };
                  $scope.claim_report.storey_id = {
                    "id": $scope.claim_report.storey_id,
                    'name': ''
                  };
                  $scope.claim_report.walltype_id = {
                    "id": $scope.claim_report.walltype_id,
                    'name': ''
                  };
                  $scope.claim_report.listed_building = {
                    "id": $scope.claim_report.listed_building,
                    'name': ''
                  };
                  $scope.claim_report.multiple_flats = {
                    "id": $scope.claim_report.multiple_flats,
                    'name': ''
                  };
                  $scope.claim_report.roofmaterial_id = {
                    "id": $scope.claim_report.roofmaterial_id,
                    'name': ''
                  };

                  $scope.claim_report.actualperil_id = {
                    "id": $scope.claim_report.actualperil_id,
                    'name': ''
                  };
                  $scope.claim_report.roomsaffected_id = {
                    "id": $scope.claim_report.roomsaffected_id,
                    'name': ''
                  };
                  $scope.claim_report.eurotempest = {
                    "id": $scope.claim_report.eurotempest,
                    'name': ''
                  };
                  if ($scope.claim_report.recovery_opportunity == 1) {
                    $scope.claim_report.recovery_opportunity = true;
                  } else {
                    $scope.claim_report.recovery_opportunity = false;
                  }
                });
              } else {
                $scope.claim_report.property_id = {
                  "id": $scope.claim_report.property_id,
                  'name': ''
                };
                $scope.claim_report.rooftype_id = {
                  "id": $scope.claim_report.rooftype_id,
                  'name': ''
                };
                $scope.claim_report.propertyage_id = {
                  "id": $scope.claim_report.propertyage_id,
                  'name': ''
                };
                $scope.claim_report.propertycondition_id = {
                  "id": $scope.claim_report.propertycondition_id,
                  'name': ''
                };
                $scope.claim_report.storey_id = {
                  "id": $scope.claim_report.storey_id,
                  'name': ''
                };
                $scope.claim_report.walltype_id = {
                  "id": $scope.claim_report.walltype_id,
                  'name': ''
                };
                $scope.claim_report.listed_building = {
                  "id": $scope.claim_report.listed_building,
                  'name': ''
                };
                $scope.claim_report.multiple_flats = {
                  "id": $scope.claim_report.multiple_flats,
                  'name': ''
                };
                $scope.claim_report.roofmaterial_id = {
                  "id": $scope.claim_report.roofmaterial_id,
                  'name': ''
                };

                $scope.claim_report.actualperil_id = {
                  "id": $scope.claim_report.actualperil_id,
                  'name': ''
                };
                $scope.claim_report.roomsaffected_id = {
                  "id": $scope.claim_report.roomsaffected_id,
                  'name': ''
                };
                $scope.claim_report.eurotempest = {
                  "id": $scope.claim_report.eurotempest,
                  'name': ''
                };
                if ($scope.claim_report.recovery_opportunity == 1) {
                  $scope.claim_report.recovery_opportunity = true;
                } else {
                  $scope.claim_report.recovery_opportunity = false;
                }
              }
            });

          } else {
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }

    $scope.change_enable = function(current_model, model_var, check_value) {
      if (current_model['id'] != check_value) {
        $scope.claim_report[model_var] = '';
        $('#' + model_var).attr("readonly", 'readonly');
      } else {
        $('#' + model_var).removeAttr("readonly");
      }
    }
    var ipObjDP = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        selected_date = new Date(val);
        $scope.claim_report[$scope.model_txt] = $filter('date')(selected_date, 'dd-MMM-yyyy');
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      //disableWeekdays: [0],       //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    $scope.openDatePicker = function(date_field) {
      $scope.model_txt = date_field;
      ionicDatePicker.openDatePicker(ipObjDP);
    };
    var ipObjDDP = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        selected_date = new Date(val);
        $scope.drying_form[$scope.model_txt] = $filter('date')(selected_date, 'dd-MMM-yyyy');
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      //disableWeekdays: [0],       //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    $scope.openDatePickerDrying = function(date_field) {
      $scope.model_txt = date_field;
      ionicDatePicker.openDatePicker(ipObjDDP);
    };
    $scope.toggleSelection = function toggleSelection(fruitName) {
      if ($scope.claim_report.requirements.indexOf(',' + fruitName) >= 0) {
        $scope.claim_report.requirements = $scope.claim_report.requirements.replace(',' + fruitName, '');
      } else if ($scope.claim_report.requirements.indexOf(fruitName) >= 0) {
        $scope.claim_report.requirements = $scope.claim_report.requirements.replace(fruitName, '');
      } else {
        if ($scope.claim_report.requirements != '') {
          $scope.claim_report.requirements += ',' + fruitName;
        } else {
          $scope.claim_report.requirements += fruitName;
        }
      }
      //$scope.claim_report.requirements = $scope.claim_report.requirements.join(',');
      console.log($scope.claim_report.requirements.trim(','));
    };
  }]);;
