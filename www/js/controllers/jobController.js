//angular.module('starter.jobcontrollers', ['starter.services','ionic', 'googlechart'])
angular.module('starter.jobcontrollers', ['ionic', 'ngCordova', 'googlechart', 'ionic-timepicker', 'ionic-datepicker'])
  .controller('jobpreviewCtrl', ['Api', '$filter', '$stateParams', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', 'ionicDatePicker', 'ionicTimePicker', function(Api, $filter, $stateParams, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, ionicDatePicker, ionicTimePicker) {
    assign_surveryor($scope, $ionicPopup);
    setPopupAddClaim($scope, $state, $ionicPopup, $rootScope);
    setPopupAddSurveryor($scope, $ionicPopup, $rootScope);
    setClaimLocationImageInDashboard($scope, $ionicPopup, $rootScope);
    setPopupshowMapInPopUp($scope, $ionicPopup, $rootScope);
    $scope.claim = {
      booked_date: '',
      booked_time: ''
    };
    $scope.clientList = {
      fnol: ''
    };
    var ipObjDP = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        selected_date = new Date(val);
        $scope.claim[$scope.model_txt] = $filter('date')(selected_date, 'dd-MMM-yyyy');
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      //disableWeekdays: [0],       //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    if ($scope.claim["booked_time"] != "") {
      var curTime = (((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getHours() * 60 * 60) + ((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getMinutes() * 60));
    } else {
      var curTime = (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60));
    }

    var ipObjTP = {
      callback: function(val) { //Mandatory
        if (typeof(val) === 'undefined') {
          /** nothing to do */
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.claim["booked_time"] = ((selectedTime.getUTCHours() > 9) ? selectedTime.getUTCHours() : "0" + selectedTime.getUTCHours()) + ":" + (selectedTime.getUTCMinutes() > 9 ? selectedTime.getUTCMinutes() : "0" + selectedTime.getUTCMinutes());
        }
      },
      inputTime: curTime, //Optional
      format: 24, //Optional
      step: 15, //Optional
      setLabel: 'Set' //Optional
    };
    $scope.serch_box = {
      type: 'all',
      searchtxt: ''
    }
    var req = {
      headers: Api.HEADERS
    };
    $scope.openDatePicker = function(date_field) {
      $scope.model_txt = date_field;
      ionicDatePicker.openDatePicker(ipObjDP);
    };

    $scope.openTimePicker = function(time_field) {
      if ($scope.claim["booked_time"] != "") {
        var curTime = (((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getHours() * 60 * 60) + ((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getMinutes() * 60));
      } else {
        var curTime = (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60));
      }
      var ipObjTP = {
        callback: function(val) { //Mandatory
          if (typeof(val) === 'undefined') {
            /** nothing to do */
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.claim["booked_time"] = ((selectedTime.getUTCHours() > 9) ? selectedTime.getUTCHours() : "0" + selectedTime.getUTCHours()) + ":" + (selectedTime.getUTCMinutes() > 9 ? selectedTime.getUTCMinutes() : "0" + selectedTime.getUTCMinutes());
          }
        },
        inputTime: curTime, //Optional
        format: 24, //Optional
        step: 15, //Optional
        setLabel: 'Set' //Optional
      };
      ionicTimePicker.openTimePicker(ipObjTP);
    };
    $scope.getClientName = function() { // also edit time data fill

      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");

      $("#claim_phone").mask("9999 999 9999");
      $("#claim_mobile").mask("99999 999 999");

      if ($stateParams.claim_id != '') {
        $rootScope.edit_claim_id = $stateParams.claim_id;
      } else {
        $rootScope.edit_claim_id = 0;
      }

      if ($stateParams.type == 1) {
        $scope.serch_box.type = 'all';
      } else if ($stateParams.type == 2) {
        $scope.serch_box.type = 'completed';
      } else if ($stateParams.type == 3) {
        $scope.serch_box.type = 'assigned';
      } else if ($stateParams.type == 4) {
        $scope.serch_box.type = 'unassigned';
      }

      $scope.claim = {};

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      if ($stateParams.claim_id != '') {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType"),
          'claim_id': $stateParams.claim_id
        });
      } else {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType")
        });
      }
      var urlBase = Api.urlBase + "claims/mastersforadd";

      $http.post(urlBase, pdata, req).then(
        function(response) {
          if (response.data.status == 1) {
            $scope.clientList = response.data.response;
            //servier dropdown
            var surveyorsList = new Array();
            $scope.selectsurveyors = {};
            surveyorsList.push({
              'id': '0',
              'name': 'Select Surveyors'
            });
            $scope.selectsurveyors.selected = {
              id: 0,
              name: ''
            };
            $.each($scope.clientList.surveyors_list, function(k, v) {
              surveyorsList.push({
                'id': v.id,
                'name': v.name
              });
            });
            $scope.surveyorsList = surveyorsList;


            //client dropdown
            var clientsList = new Array();
            $scope.selectclient = {};

            clientsList.push({
              'id': '0',
              'name': 'Select Client'
            });
            $scope.selectclient.selected = {
              id: '0'
            };
            $.each($scope.clientList.client_list, function(k, v) {
              clientsList.push({
                'id': v.id,
                'name': v.name
              });
            });
            $scope.clientsList = clientsList;

            if (typeof $stateParams.client_id != 'undefined') {
              if ($stateParams.client_id == '') {
                $scope.selectclient.selected = {
                  id: '0',
                  name: ''
                };
              } else {
                $scope.selectclient.selected = {
                  id: $stateParams.client_id,
                  name: ''
                };
              }
              //add droup down in calim listing
              var start = moment().subtract(29, 'days');
              var end = moment();

              function cb(start, end) {
                $('#reportrange-claim span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //$scope.search_dashbord();
                //delete $scope.claimList_box;
                //$scope.getClientwiseClaims();
                $scope.searchClaim();
              }
              if ($stateParams.daterange != '') {
                $("#reportrange-claim .current-date").html($stateParams.daterange);
              } else {
                cb(start, end);
              }
              $('#reportrange-claim').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
              }, cb);


              //cb(start, end);
              $(".daterangepicker.dropdown-menu.ltr.opensleft").css({
                top: '182px',
                right: '5px',
                left: 'auto',
                display: 'none'
              });
              $scope.searchClaim();
            }

            //reported_perill dropdown
            var reported_perillList = new Array();
            $scope.selectreported_perill = {};

            reported_perillList.push({
              'id': '0',
              'name': 'select reported perill'
            });
            $scope.selectreported_perill.selected = {
              id: 0,
              name: ''
            };
            $.each($scope.clientList.reportedperill_list, function(k, v) {
              reported_perillList.push({
                'id': v.id,
                'name': v.name
              });
            });
            $scope.reported_perillList = reported_perillList;

            if ($stateParams.claim_id) {
              $scope.claim = $scope.clientList.claim_data;
              $scope.claim.internal_notes = '';
              $scope.claim.user_id = '';
              $.each($scope.claim.claimnotes, function(k, v) {
                $scope.claim.internal_notes += v.created + ' ' + v.internal_notes + '\n';
              });
              if ($scope.claim.assignclaims.length > 0) {
                drop_user_id = $scope.claim.assignclaims[$scope.claim.assignclaims.length - 1]['user_id'];
              } else {
                drop_user_id = 0;
              }
              $scope.selectsurveyors.selected = {
                id: drop_user_id,
                name: ''
              };
              $scope.selectclient.selected = {
                id: $scope.claim.client_id,
                name: ''
              };
              $scope.selectreported_perill.selected = {
                id: $scope.claim.reported_perill,
                name: ''
              };

              delete $scope.claim.claimnotes, $scope.claim.assignclaims;
            } else {
              $scope.claim.booked_time = '00:00';
            }
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Add Claim',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Add Claim',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );
    }



    $scope.searchClaim = function(keyword, type) { //on dasbord load
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      console.log($scope.selectclient.selected.id);

      //$scope.selectclient.selected = { id: $scope.claim.client_id, name: ''};
      if ($scope.selectclient.selected.id != 0) {
        var client_id = $scope.selectclient.selected.id;
      } else {
        var client_id = '';
      }
      console.log($stateParams);

      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'keyword': keyword,
        'type': $scope.serch_box.type,
        'user_token': localStorage.getItem("UserToken"),
        'user_type': localStorage.getItem("UserType"),
        'client_id': client_id,
        daterange: $("#reportrange-claim .current-date").html()
      });

      var urlBase = Api.urlBase + "claims";

      $http.post(urlBase, pdata, req).then(

        function(response) {
          $ionicLoading.hide();

          //$scope.Download('http://seegatesite.com/wp-content/uploads/2016/06/how-to-create-amazon-affiliate-site-just-5-minutes.png');

          if (response.data.status == 1) {
            if (Object.keys(response.data.response).length > 0) {
              delete $scope.message;
              $scope.claimList = response.data.response;
            } else {
              delete $scope.claimList;
              $scope.message = response.data.message;
            }
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Preview',
              cssClass: 'notification',
              template: response.data.message
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Preview',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $rootScope.deleteClaim = function(claim_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete claim',
        template: 'Are you sure you want to delete this ?'
      });

      confirmPopup.then(function(res) {
        if (res) {

          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'claim_id': claim_id
          });
          var urlBase = Api.urlBase + "claims/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  //console.log($rootScope);
                  //$rootScope.AddStructur.close();
                  //$rootScope.get_detail_live($stateParams.id);
                  $scope.searchClaim($scope.serch_box.searchtxt, $scope.serch_box.type);
                  //$state.go("app.detailview",{'id':$stateParams.id});
                });


              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Claim',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
    $scope.addClaim = function(addClaim_frm) {
      if (addClaim_frm.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;
        if (check_connection()) {

          var pdata = ($('#frmAddClaim').serialize());

          if ($stateParams.claim_id > 0) {
            var urlBase = Api.urlBase + "claims/edit";
          } else {
            var urlBase = Api.urlBase + "claims/add";
          }
          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertTitle = "Add Claim";
                if ($stateParams.claim_id > 0) {
                  alertTitle = "Edit Claim";
                }
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  $scope.go_claim_list($scope.menu_date);
                  //$scope.searchClaim($scope.serch_box.searchtxt,$scope.serch_box.type);
                });
              } else {
                $ionicLoading.hide();
                var error_message = response.data.message;
                $.each(response.data.response, function(k, v) {
                  error_message += '<br/>' + v;
                });
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: error_message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();

              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'No internet connection !'
          });
        }
      }
    }
    //search box
    $scope.show_search_box = function() {
      $(".search-box").show(200);
    }
    $scope.serch_box = {
      type: 'all',
      searchtxt: ''
    }
    $scope.hide_search_box = function() {
      $scope.searchClaim();
      $(".search-box").hide(200);
    }
    $scope.search = function() {
      $scope.searchClaim($scope.serch_box.searchtxt, $scope.serch_box.type);
    }
    $scope.equalHeight = function() {
      setTimeout(function() {
        equalHeight($(".caption"));
      }, 500);
    }
  }])
  .controller('jobdetailCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {

    setPopupAddNote($scope, $ionicPopup, $rootScope);
    setPopupAddStructur($scope, $ionicPopup, $rootScope);
    setPopupshowMapInPopUp($scope, $ionicPopup, $rootScope);
    setPopupAddClaim($scope, $state, $ionicPopup, $rootScope);

    $scope.$on("$ionicView.beforeEnter", function(event, data) { //on dasbord load

      if ($stateParams.id > 0) { // id of claim
        $rootScope.get_detail_live($stateParams.id, $scope);
      }
    });
    //$rootScope.get_detail_live($stateParams.id);
    $scope.claimPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "claimreports/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.chnage_status_assign_claim_message = function() {
      var alertPopup = $ionicPopup.alert({
        title: 'Claim status',
        cssClass: 'notification',
        template: 'Please assign claim to surveyor.'
      });
    }

    $scope.claimDrying = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "drying/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim drying pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.healthSafetyPDF = function(claim_id) {
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "healthsafety/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim Health & Safety pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.open_web_view = function(url) {
      window.open(url, '_blank');
    }
    $scope.pfiPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "pfiform/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.saveFnol = function() {

      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'fnol': $scope.clientList.fnol,
        'claim_id': $stateParams.id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "claims/updatefnol";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
        });
    }
    $scope.recoveryFormPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "recoveryform/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.openSow = function(room_id, claim_id, open_tab) {
      $state.go("app.sow", {
        'room_id': room_id,
        'claim_id': claim_id,
        'open_tab': open_tab
      });
    };
    $scope.editStructure = function(structure_id, structure_name) {
      /*console.log(structure_id);
      console.log(structure_name);
      return false;*/
      claim_id = $stateParams.id;
      if (structure_id > 0) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;

        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claim_id': claim_id,
          'id': structure_id,
          'structure_id': structure_id,
          'name': structure_name
        });;
        var urlBase = Api.urlBase + "claimstructures/edit";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            $ionicLoading.hide();
            if (response.data.status == 1) {
              //console.log($("input[structure_id='"+structure_id+"']").html());
              $("input[structure_id='" + structure_id + "']").removeAttr('style');
              $("input[structure_id='" + structure_id + "']").parent('.cfontweight').find('a').show();
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Edit Structure',
                cssClass: 'notification',
                template: response.data.message
              });
              alertPopup.then(function(res) {
                //location.reload();
              });
            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Edit Structure',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      }
    }
    $rootScope.edit_btn = function(edit_btn_id) {
      $("input[structure_id='" + edit_btn_id + "']").attr("style", "display:block !important;");
      $("input[structure_id='" + edit_btn_id + "']").parent('.cfontweight').find('a').hide();
    }

    $scope.chnage_status = function() {
      claim_id = $stateParams.id;
      pass_status = $scope.claim_status;

      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");
      $scope.claim_id = localStorage.getItem("UserType");

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id,
        'status': pass_status
      });
      var urlBase = Api.urlBase + "claims/changestatus";
      var req = {
        headers: Api.HEADERS
      };

      $http.post(urlBase, pdata, req).then(
        function(response) {
          if (response.data.status == 1) {
            console.log('----------------status responce -----------------');
            console.log(response.data);
            //alert($stateParams.id);
            $rootScope.get_detail_live($stateParams.id, $scope);
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );
    }

    $scope.deleteStructure = function(structure_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete structure',
        template: 'Are you sure you want to delete structure ?'
      });

      confirmPopup.then(function(res) {
        if (res) {

          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'id': structure_id
          });
          var urlBase = Api.urlBase + "claimstructures/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              if (response.data.status == 1) {
                console.log('----------------delete structure -----------------');
                console.log(response.data);
                //alert($stateParams.id);
                //$state.go("app.detailview",{'id':$stateParams.id});
                $rootScope.get_detail_live($stateParams.id, $scope);
              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Structure',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
              $ionicLoading.hide();
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Structure',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
    $rootScope.get_detail_live = function(claim_id, scope_var) {
      $rootScope.from_dashboard = $stateParams.from_dashboard;

      $rootScope.UserToken = localStorage.getItem("UserToken");
      $rootScope.UserId = localStorage.getItem("UserId");
      $rootScope.UserType = localStorage.getItem("UserType");
      //claim_id = $stateParams.id;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id
      });
      var urlBase = Api.urlBase + "claims/details";
      var req = {
        headers: Api.HEADERS
      };

      $http.post(urlBase, pdata, req).then(

        function(response) {
          $ionicLoading.hide();
          if (response.data.status == 1) {

            $rootScope.clientList = response.data.response;
            $rootScope.claim_id = $rootScope.clientList.id;
            $rootScope.claim_status = $rootScope.clientList.status;

            setTimeout(function() {
              //console.log($('.claimclientdetail').eq(0).html());
              //console.log($('.claimclientdetail').eq(1).html());
              var heights = angular.element('.claimclientdetail').eq(0).innerHeight();
              $('.claimclientdetail').eq(0).find(".cmob640").css({
                "height": heights
              });
              $('.claimclientdetail').eq(0).find(".box-border").css({
                "margin": "0px"
              });
              $('.claimclientdetail').eq(0).css({
                "margin": "0px"
              });
              //alert(heights);
              var heights = angular.element('.claimclientdetail').eq(1).innerHeight();
              $('.claimclientdetail').eq(1).find(".cmob640").css({
                "height": heights
              });
              $('.claimclientdetail').eq(1).find(".box-border").css({
                "margin": "0px"
              });
              $('.claimclientdetail').eq(1).css({
                "margin": "0px"
              });
              //alert(heights);
            }, 1000);
            //return d.resolve(scope_var);
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });

        }
      )

    }
    $rootScope.deleteClaim = function(claim_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete claim',
        template: 'Are you sure you want to delete this ?'
      });

      confirmPopup.then(function(res) {
        if (res) {

          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'claim_id': claim_id
          });
          var urlBase = Api.urlBase + "claims/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  //console.log($rootScope);
                  //$rootScope.AddStructur.close();
                  //$rootScope.get_detail_live($stateParams.id);
                  //$scope.searchClaim($scope.serch_box.searchtxt,$scope.serch_box.type);
                  $state.go("app.jobpreview");
                });


              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Claim',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
  }])
  /*.controller('claimNotesCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {

    $scope.$on("$ionicView.beforeEnter", function(event, data) { //on dasbord load

    });
    $scope.addClaimNotes = function(addClaimNotefrm) {
      if (addClaimNotefrm.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;

        var pdata = $('#frmAddClaimNote').serialize();
        var urlBase = Api.urlBase + "claims/addnotes";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            $ionicLoading.hide();
            if (response.data.status == 1) {
              var alertPopup = $ionicPopup.alert({
                title: 'Claim Notes',
                cssClass: 'notification',
                template: response['data']['message']
              });
              alertPopup.then(function(res) {
                $rootScope.AddClaimNotePopup.close();
                $rootScope.get_detail_live($stateParams.id, $rootScope).then(function(scope_sope_data_res) {
                  $rootScope.$apply(function() {
                    $rootScope.clientList = scope_sope_data_res.clientList;
                    $rootScope.claim_id = scope_sope_data_res.claim_id;
                    $rootScope.claim_status = scope_sope_data_res.claim_status;
                  });
                  /*$rootScope.$watch(function($rootScope) {
                    $rootScope.clientList = scope_sope_data_res.clientList;
                    $rootScope.claim_id = scope_sope_data_res.claim_id;
                    $rootScope.claim_status = scope_sope_data_res.claim_status;
                    return $rootScope;
                  });*/
  /*});
              });
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Internal Notes',
                cssClass: 'notification',
                template: response.data.message
              });
              $rootScope.get_detail_live($stateParams.id, $scope).then(function(scope_sope_data_res) {
                $scope.$apply(function() {
                  $scope = scope_sope_data_res;
                });
              });
              alertPopup.then(function(res) {
                //location.reload();
              });
            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Internal Notes',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      }
    }
  }])*/
  .controller('structureCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {
    $scope.addStructure = function(structure_id, structure_name) {
      claim_id = $stateParams.id;
      if (structure_id > 0) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;

        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claim_id': claim_id,
          'structure_id': structure_id,
          'name': structure_name
        });;
        var urlBase = Api.urlBase + "claimstructures/add";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            $ionicLoading.hide();
            if (response.data.status == 1) {
              var alertPopup = $ionicPopup.alert({
                title: 'Add Structure',
                cssClass: 'notification',
                template: response['data']['message']
              });
              $ionicLoading.hide();
              alertPopup.then(function(res) {
                console.log($rootScope);
                $rootScope.AddStructur.close();
                $rootScope.get_detail_live($stateParams.id, $scope);
                //$state.go("app.detailview",{'id':$stateParams.id});
              });
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Structure',
                cssClass: 'notification',
                template: response.data.message
              });
              alertPopup.then(function(res) {
                //location.reload();
              });
            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Structure',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      }
    }

    $scope.addStructureList = function() {
      claim_id = $stateParams.id;

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      pdata = [];
      $scope.ServererrMsg = false;

      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id,
        'parent_id': $state.current_structure_page,
        'name': $("input[name='name']").val()
      });;
      var urlBase = Api.urlBase + "claimstructures/addstructure";
      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          console.log(response.data);
          if (response.data.status == 1) {
            $scope.getStructure($rootScope.current_structure_page);
            $("input[name='name']").val('');
          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Structure',
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Add Structure',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.getStructure = function(parent_id) {

      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");
      claim_id = $stateParams.id;
      $rootScope.current_structure_page = parent_id;

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id,
        'parent_id': parent_id
      });
      var urlBase = Api.urlBase + "claimstructures/getStructuredata";
      var req = {
        headers: Api.HEADERS
      };

      $http.post(urlBase, pdata, req).then(

        function(response) {
          console.log(response);
          if (response.data.status == 1) {
            $scope.StructuretList = response.data.response;

            if ($scope.StructuretList.length > 0 && parent_id != 0) {
              $scope.backParentId = $scope.StructuretList[0]['previous_parent_id'];
            } else {
              $scope.backParentId = null
            }
            $('.popup-title.ng-binding').html(response.data.resposetitle.title.toUpperCase());
            $scope.button_title = response.data.resposetitle.btntitle;
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );
    }

    $scope.getStructure(0);
  }])
  .controller('pdfCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {

    $scope.claim_id = $stateParams.claim_id;
    $scope.url = $stateParams.api_request;
    $ionicLoading.show({
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    var pdata = jQuery.param({
      'user_id': localStorage.getItem("UserId"),
      'claim_id': $stateParams.claim_id,
      'user_token': localStorage.getItem("UserToken")
    });

    var urlBase = Api.urlBase + "claimreports/pdf";

    var req = {
      headers: Api.HEADERS
    };
    $http.post(urlBase, pdata, req).then(
      function(response) {
        $ionicLoading.hide();
        var alertTitle = "Claim report pdf";
        if (response.data.status == 1) {
          $scope.claim_report_url = response.data.response.url;
          window.open($scope.claim_report_url, '_system', 'location=yes');
          //$('iframe').attr('src',$scope.claim_report_url);
          //$('iframe').css('height',$(window).height() - $('.inner-header').height() - $('.bar-stable.bar.bar-header').height() - 30);
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: response.data.message
          });
          alertPopup.then(function(res) {
            //location.reload();
          });
        }
      },
      function(error) {
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: alertTitle,
          cssClass: 'notification',
          template: 'There is some server issue, please try again after some time'
        });
      });
  }])
  .controller('CarouselDemoCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {
    $scope.myInterval = 1000;
    $scope.slides = [{
        image: 'http://lorempixel.com/400/200/'
      },
      {
        image: 'http://lorempixel.com/400/200/food'
      },
      {
        image: 'http://lorempixel.com/400/200/sports'
      },
      {
        image: 'http://lorempixel.com/400/200/people'
      }
    ];
  }])
  .directive('repeatDone', function() {
    return function(scope, element, attrs) {
      if (scope.$last) { // all are rendered
        scope.$eval(attrs.repeatDone);
      }
    }
  });
