angular.module('starter.claimcontrollers', ['ionic', 'ngCordova', 'googlechart', 'ionic-timepicker', 'ionic-datepicker'])
  .controller('claimviewCtrl', ['Api', 'DbService', '$filter', '$stateParams', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', 'ionicDatePicker', 'ionicTimePicker', '$cordovaFile', function(Api, DbService, $filter, $stateParams, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, ionicDatePicker, ionicTimePicker, $cordovaFile) {
    assign_surveryor($scope, $ionicPopup);
    setPopupAddClaim($scope, $state, $ionicPopup, $rootScope);
    setPopupAddSurveryor($scope, $ionicPopup, $rootScope);
    setClaimLocationImageInDashboard($scope, $ionicPopup, $rootScope);
    setPopupshowMapInPopUp($scope, $ionicPopup, $rootScope);
    $scope.claim = {
      booked_date: '',
      booked_time: ''
    };
    $scope.clientList = {
      fnol: ''
    };
    if (localStorage.getItem("UserType") == 1) {
      $rootScope.app.is_admin = true;
    } else {
      $rootScope.app.is_admin = false;
    }
    var ipObjDP = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        selected_date = new Date(val);
        $scope.claim[$scope.model_txt] = $filter('date')(selected_date, 'dd-MMM-yyyy');
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      //disableWeekdays: [0],       //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    if ($scope.claim["booked_time"] != "") {
      var curTime = (((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getHours() * 60 * 60) + ((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getMinutes() * 60));
    } else {
      var curTime = (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60));
    }

    var ipObjTP = {
      callback: function(val) { //Mandatory
        if (typeof(val) === 'undefined') {
          /** nothing to do */
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.claim["booked_time"] = ((selectedTime.getUTCHours() > 9) ? selectedTime.getUTCHours() : "0" + selectedTime.getUTCHours()) + ":" + (selectedTime.getUTCMinutes() > 9 ? selectedTime.getUTCMinutes() : "0" + selectedTime.getUTCMinutes());
        }
      },
      inputTime: curTime, //Optional
      format: 24, //Optional
      step: 15, //Optional
      setLabel: 'Set' //Optional
    };
    $scope.serch_box = {
      type: 'all',
      searchtxt: ''
    }
    var req = {
      headers: Api.HEADERS
    };
    $scope.openDatePicker = function(date_field) {
      $scope.model_txt = date_field;
      ionicDatePicker.openDatePicker(ipObjDP);
    };

    $scope.openTimePicker = function(time_field) {
      if ($scope.claim["booked_time"] != "") {
        var curTime = (((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getHours() * 60 * 60) + ((new Date("January 01, 2017 " + $scope.claim["booked_time"] + ":00")).getMinutes() * 60));
      } else {
        var curTime = (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60));
      }
      var ipObjTP = {
        callback: function(val) { //Mandatory
          if (typeof(val) === 'undefined') {
            /** nothing to do */
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.claim["booked_time"] = ((selectedTime.getUTCHours() > 9) ? selectedTime.getUTCHours() : "0" + selectedTime.getUTCHours()) + ":" + (selectedTime.getUTCMinutes() > 9 ? selectedTime.getUTCMinutes() : "0" + selectedTime.getUTCMinutes());
          }
        },
        inputTime: curTime, //Optional
        format: 24, //Optional
        step: 15, //Optional
        setLabel: 'Set' //Optional
      };
      ionicTimePicker.openTimePicker(ipObjTP);
    };
    $scope.getClientName = function() { // also edit time data fill

      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");

      $("#claim_phone").mask("9999 999 9999");
      $("#claim_mobile").mask("99999 999 999");

      if ($stateParams.claim_id != '') {
        $rootScope.edit_claim_id = $stateParams.claim_id;
      } else {
        $rootScope.edit_claim_id = 0;
      }

      if ($stateParams.type == 1) {
        $scope.serch_box.type = 'all';
      } else if ($stateParams.type == 2) {
        $scope.serch_box.type = 'completed';
      } else if ($stateParams.type == 3) {
        $scope.serch_box.type = 'assigned';
      } else if ($stateParams.type == 4) {
        $scope.serch_box.type = 'unassigned';
      }

      $scope.claim = {};


      if ($stateParams.claim_id != '') {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType"),
          'claim_id': $stateParams.claim_id
        });
      } else {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType")
        });
      }
      if (check_connection()) {
        var urlBase = Api.urlBase + "claims/mastersforadd";
        $http.post(urlBase, pdata, req).then(
          function(response) {
            if (response.data.status == 1) {
              $scope.clientList = response.data.response;
              //servier dropdown
              var surveyorsList = new Array();
              $scope.selectsurveyors = {};
              surveyorsList.push({
                'id': '0',
                'name': 'Select Surveyors'
              });
              $scope.selectsurveyors.selected = {
                id: 0,
                name: ''
              };
              $.each($scope.clientList.surveyors_list, function(k, v) {
                surveyorsList.push({
                  'id': v.id,
                  'name': v.name
                });
              });
              $scope.surveyorsList = surveyorsList;


              //client dropdown
              var clientsList = new Array();
              $scope.selectclient = {};

              clientsList.push({
                'id': '0',
                'name': 'Select Client'
              });
              $scope.selectclient.selected = {
                id: '0'
              };
              $.each($scope.clientList.client_list, function(k, v) {
                clientsList.push({
                  'id': v.id,
                  'name': v.name
                });
              });
              $scope.clientsList = clientsList;

              if (typeof $stateParams.client_id != 'undefined') {
                if ($stateParams.client_id == '') {
                  $scope.selectclient.selected = {
                    id: '0',
                    name: ''
                  };
                } else {
                  $scope.selectclient.selected = {
                    id: $stateParams.client_id,
                    name: ''
                  };
                }
                //add droup down in calim listing
                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                  $('#reportrange-claim span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                  //$scope.search_dashbord();
                  //delete $scope.claimList_box;
                  //$scope.getClientwiseClaims();
                  DbService.deleteClaim('', 0).then(function() {
                    //$scope.selectListingClaims();
                    $scope.searchClaim();
                  });

                }
                if ($stateParams.daterange != '') {
                  $("#reportrange-claim .current-date").html($stateParams.daterange);
                  DbService.deleteClaim('', 0).then(function() {
                    $scope.searchClaim();
                  });
                } else {
                  cb(start, end);
                }
                $('#reportrange-claim').daterangepicker({
                  startDate: start,
                  endDate: end,
                  ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  }
                }, cb);


                //cb(start, end);
                $(".daterangepicker.dropdown-menu.ltr.opensleft").css({
                  top: '182px',
                  right: '5px',
                  left: 'auto',
                  display: 'none'
                });
                //
              }

              //reported_perill dropdown
              var reported_perillList = new Array();
              $scope.selectreported_perill = {};

              reported_perillList.push({
                'id': '0',
                'name': 'select reported perill'
              });
              $scope.selectreported_perill.selected = {
                id: 0,
                name: ''
              };
              $.each($scope.clientList.reportedperill_list, function(k, v) {
                reported_perillList.push({
                  'id': v.id,
                  'name': v.name
                });
              });
              $scope.reported_perillList = reported_perillList;

              if ($stateParams.claim_id) {
                $scope.claim = $scope.clientList.claim_data;
                $scope.claim.internal_notes = '';
                $scope.claim.user_id = '';
                $.each($scope.claim.claimnotes, function(k, v) {
                  $scope.claim.internal_notes += v.created + ' ' + v.internal_notes + '\n';
                });
                if ($scope.claim.assignclaims.length > 0) {
                  drop_user_id = $scope.claim.assignclaims[$scope.claim.assignclaims.length - 1]['user_id'];
                } else {
                  drop_user_id = 0;
                }
                $scope.selectsurveyors.selected = {
                  id: drop_user_id,
                  name: ''
                };
                $scope.selectclient.selected = {
                  id: $scope.claim.client_id,
                  name: ''
                };
                $scope.selectreported_perill.selected = {
                  id: $scope.claim.reported_perill,
                  name: ''
                };

                delete $scope.claim.claimnotes, $scope.claim.assignclaims;
              } else {
                $scope.claim.booked_time = '00:00';
              }
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Add Claim',
                cssClass: 'notification',
                template: response.data.message
              });
            }
          },
          function(error) {
            $scope.searchClaim();
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Claim',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          }
        );
      } else {
        DbService.getClientList().then(function(dbclient_list) {
          $scope.selectclient = {};
          var clientsList = [];
          clientsList.push({
            'id': '0',
            'name': 'Select Client'
          });
          $scope.selectclient.selected = {
            id: '0'
          };
          $.each(dbclient_list, function(k, v) {
            clientsList.push({
              'id': v.id,
              'name': v.name
            });
          });
          $scope.clientsList = clientsList;
        });

        if ($stateParams.daterange != '') {
          $("#reportrange-claim .current-date").html($stateParams.daterange);
          $scope.page_counter = 0
          $scope.searchClaim($scope.page_counter);
        } else {
          cb(start, end);
        }
      }
    }
    $scope.page_counter = 0;
    $scope.loadMore = function() {
      $scope.page_counter = $scope.page_counter + 1;
      $scope.searchClaim($scope.page_counter);
    }
    $scope.claimList = null;
    var claims_ids = '';
    $scope.searchClaim = function(page_no) { //on dasbord load
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      $scope.ionicLoading = $ionicLoading;
      if (page_no != null && typeof page_no != 'undefined' && page_no != '') {
        page_no = page_no;
      } else {
        page_no = 0;
      }

      if (typeof $scope.selectclient != 'undefined' && $scope.selectclient.selected.id != 0) {
        var client_id = $scope.selectclient.selected.id;
      } else {
        var client_id = '';
      }
      console.log($stateParams);
      if (check_connection()) {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'keyword': $scope.serch_box.searchtxt,
          'type': $scope.serch_box.type,
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType"),
          'client_id': client_id,
          'daterange': $("#reportrange-claim .current-date").html(),
          'page': page_no
        });

        var urlBase = Api.urlBase + "claims";
        DbService.syncClaimsData().then(function() {
          $http.post(urlBase, pdata, req).then(

            function(response) {
              if (response['data']['status'] == 1) {
                if (localStorage.getItem("UserType") == 3) { //for sarvivor
                  DbService.removeDashbordClaimsAsClaimList().then(function() {
                    if (objSize(response['data']['response']) > 0) {
                      var claim_detail = response['data']['response'];
                      var constant_media = response['data']['response']['constant_media'];
                      delete response['data']['response']['claims'];

                      $.each(claim_detail, function(k, v) {
                        claims_ids = claims_ids + v['id'] + ',';
                        claim_id = v['id'];
                        profile_pick_full_url = v['surveyor_image'];
                        client_image_full_url = v['client_image'];
                        users_assigned_date = v['assigned_date'];
                        reported_perill_value = v['reported_perill_value'];
                        var claim_values = v;
                        // insert or update dashboard data
                        var assign_user_id = '';
                        if (claim_values['assignclaims'].length > 0) {
                          assign_user_id = claim_values['assignclaims'][0]['user']['id'];
                        }
                        DbService.addClaim(v['id'], v['user_id'], v['client_id'], v['customer_name'], v['address'], v['address2'], v['city'], v['state'], v['zipcode'], '', '', v['phone'], v['mobile'], v['email'], v['policy_number'], v['reported_perill'], v['st_reference'], v['customer_reference'], v['incident_date'], v['booked_date'], v['booked_time'], '', v['fnol'], 0, v['status'], v['job_number'], assign_user_id, reported_perill_value, v['created'], 1);

                        var client_detail = v['client'];
                        v = client_detail;
                        DbService.addClient(v['id'], v['client_name'], v['contact_person_name'], v['client_address'], v['client_city'], v['client_state'], v['client_country'], v['client_zipcode'], v['client_phone_no'], v['client_fax_no'], v['client_email'], v['client_warranty'], v['client_show_info'], v['client_image'], client_image_full_url, claim_id);
                        //v = claim_values;
                        if (claim_values['assignclaims'].length > 0) {
                          v = claim_values['assignclaims'][0]['user'];
                          //users_assigned_date = claim_values['assignclaims'][0]['created'];
                          DbService.addUsers(v['id'], v['id_number'], v['firstname'], v['lastname'], v['address1'], v['address2'], v['city'], v['state'], v['country'], v['zip'], v['email'], v['mobile'], v['vehical_reg'], v['homephone'], v['profile_pic'], profile_pick_full_url, claim_id, users_assigned_date);
                        }
                        $.each(claim_values['claimnotes'], function(notesk, notesv) {
                          user_id = notesv['user']['id'];
                          user_name = notesv['user']['firstname'] + notesv['user']['lastname'];

                          DbService.addClaimInternalnote(notesv['id'], notesv['claim_id'], notesv['internal_notes'], notesv['created'], user_name, user_id);
                        });
                        $.each(claim_values['pfiforms'], function(pfik, pfiv) {
                          pfiv['live_id'] = pfiv['id'];

                          DbService.addClaimPFI(pfiv);
                        });
                        $.each(claim_values['recoveryforms'], function(recoveryk, recoveryv) {
                          recoveryv['live_id'] = recoveryv['id'];

                          DbService.addClaimRecovery(recoveryv);
                        });
                        $.each(claim_values['drying'], function(Dryingk, Dryingv) {
                          Dryingv['live_id'] = Dryingv['id'];

                          DbService.addClaimDrying(Dryingv);
                        });
                        $.each(claim_values['healthsafety'], function(healthsafetyk, healthsafetyv) {
                          healthsafetyv['live_id'] = healthsafetyv['id'];

                          DbService.addClaimHealthSafety(healthsafetyv);
                        });
                        $.each(claim_values['claimreports'], function(claimreportk, claimreportv) {
                          claimreportv['live_id'] = claimreportv['id'];

                          DbService.addClaimreports(claimreportv);
                        });
                        $.each(claim_values['claimstructures'], function(claimstructuresk, claimstructurestv) {

                          DbService.setClaimStructure(claimstructurestv.id, claimstructurestv.user_id, claimstructurestv.claim_id, claimstructurestv.structure_id, claimstructurestv.name, Api.formatDateDatabase(claimstructurestv.created), Api.formatDateDatabase(claimstructurestv.modified), '', claimstructurestv.modified_by, claimstructurestv.deleted_by, 1).then(function(result) {
                            console.log(result);
                            if (result.insertId > 0) {
                              DbService.getStructureId(claimstructurestv.id).then(function(res) {
                                var fullPath = "/",
                                  folder = 'claims/' + res['claim_id'] + "/" + res['claimstructurest_insertId'] + "/",
                                  folders = folder.split('/'),
                                  fullPath = '/',
                                  i = 0;
                                while (i < folders.length) {
                                  //$cordovaFile.checkDir(cordova.file.dataDirectory + fullPath, folders[i]).then(function() {}, function(error) {
                                  fullPath = fullPath + '/' + folders[i];
                                  $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false);
                                  //});
                                  i = i + 1;
                                }
                                $.each(claimstructurestv.structuremedia, function(claimmediak, claimmediav) {
                                  var media_file = claimmediav.media_file;
                                  var media_file_full = claimmediav.media_file;
                                  var media_file_split = claimmediav.media_file.split('/');
                                  claimmediav.media_file = media_file_split[media_file_split.length - 1];

                                  DbService.setStructureMedia(claimmediav.id, claimmediav.claimstructure_id, claimmediav.user_id, claimmediav.media_file, claimmediav.filetype, Api.formatDate(claimmediav.created), Api.formatDate(claimmediav.modified), '', claimmediav.modified_by, '', claimmediav.status, claimstructurestv.claim_id, constant_media, media_file_full, result.insertId, folder, claimmediav.media_file)
                                    .then(function(resultMedia) {
                                      console.log(resultMedia);
                                    })
                                });
                              });
                              $.each(claimstructurestv.structurenotes, function(structurenotesk, structurenotesv) {
                                //structurenotesv.username = structurenotesv.user['firstname'] + ' ' + structurenotesv.user['lastname'];
                                DbService.addStructurnotes(structurenotesv);
                              });
                              $.each(claimstructurestv.sowtask, function(sowtaskk, sowtaskv) {
                                DbService.addSowtask(sowtaskv);
                              });
                            }
                          })

                        });
                        $scope.selectListingClaims();
                      });

                      //delete claim if not come from listing of dashboard service
                      function removeLastComma(strng) {
                        var n = strng.lastIndexOf(",");
                        var a = strng.substring(0, n)
                        return a;
                      }
                    } else {
                      var alertPopup = $ionicPopup.alert({
                        title: 'Claims List',
                        cssClass: 'notification',
                        template: 'No more data'
                      });
                      $ionicLoading.hide();
                    }
                  });
                } else { //is admin
                  if (objSize(response['data']['response']) > 0) {
                    if ($scope.claimList == null) {
                      $scope.claimList = response['data']['response'];
                    } else {

                      if (!$scope.$$phase) {
                        $scope.$apply(function() {
                          if (page_no != 0) {
                            $scope.claimList = $scope.claimList.concat(response['data']['response']);
                          } else {
                            $scope.claimList = response['data']['response'];
                            $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                          }
                        });
                      } else {
                        if (page_no != 0) {
                          $scope.claimList = $scope.claimList.concat(response['data']['response']);
                        } else {
                          $scope.claimList = response['data']['response'];
                          $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
                        }
                      }
                    }
                  } else {
                    var alertPopup = $ionicPopup.alert({
                      title: 'Claims List',
                      cssClass: 'notification',
                      template: 'No more data'
                    });
                    $ionicLoading.hide();
                  }
                  $ionicLoading.hide();
                }

              }
            },
            function(error) {
              $ionicLoading.hide();
              //$scope.set_dashbordData(localStorage.getItems('dashboardDetail'));
              var alertPopup = $ionicPopup.alert({
                title: 'Claim list',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        });
      } else { //claim offline
        if (localStorage.getItem('UserType') == 3) { //on servior
          $scope.selectListingClaims();
          //getClientwiseClaims
        } else {
          var alertPopup = $ionicPopup.alert({
            title: 'Dashboard',
            cssClass: 'notification',
            template: $rootScope.app['internet_connection_message']
          });
        }
        $ionicLoading.hide();
      }
    }
    var objSize = function(obj) {
      var count = 0;

      if (typeof obj == "object") {

        if (Object.keys) {
          count = Object.keys(obj).length;
        } else if (window._) {
          count = _.keys(obj).length;
        } else if (window.$) {
          count = $.map(obj, function() {
            return 1;
          }).length;
        } else {
          for (var key in obj)
            if (obj.hasOwnProperty(key)) count++;
        }

      }

      return count;
    };
    $scope.selectListingClaims = function() {
      DbService.getListingClaim(1).then(function(claimsList) {
        $scope.claimsList = claimsList;
        if (claimsList.length == 0) {
          $ionicLoading.hide();
        }
      });
    }
    $rootScope.deleteClaim = function(claim_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete claim',
        template: 'Are you sure you want to delete this ?'
      });

      confirmPopup.then(function(res) {
        if (res) {

          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'claim_id': claim_id
          });
          var urlBase = Api.urlBase + "claims/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  //console.log($rootScope);
                  //$rootScope.AddStructur.close();
                  //$rootScope.get_claimdetail($stateParams.id);
                  $scope.searchClaim();
                  //$state.go("app.detailview",{'id':$stateParams.id});
                });


              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Claim',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
    $scope.addClaim = function(addClaim_frm) {
      if (addClaim_frm.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        if (check_connection()) {
          pdata = [];
          $scope.ServererrMsg = false;

          var pdata = ($('#frmAddClaim').serialize());

          if ($stateParams.claim_id > 0) {
            var urlBase = Api.urlBase + "claims/edit";
          } else {
            var urlBase = Api.urlBase + "claims/add";
          }
          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertTitle = "Add Claim";
                if ($stateParams.claim_id > 0) {
                  alertTitle = "Edit Claim";
                }
                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  $state.go('app.jobpreview');
                  //$scope.searchClaim($scope.serch_box.searchtxt,$scope.serch_box.type);
                });
              } else {
                $ionicLoading.hide();
                //response.data.message + '<br/>';
                $.each(response.data.response, function(k, v) {
                  response.data.message + '<br/>' + v;
                });

                var alertPopup = $ionicPopup.alert({
                  title: alertTitle,
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: alertTitle,
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'No intarnet connection !'
          });
        }
      }
    }
    //search box
    $scope.show_search_box = function() {
      $(".search-box").show(200);
    }
    $scope.serch_box = {
      type: 'all',
      searchtxt: ''
    }
    $scope.hide_search_box = function() {
      $scope.searchClaim();
      $(".search-box").hide(200);
    }
    $scope.search = function() {
      DbService.deleteClaim('', 0).then(function() {
        $scope.searchClaim();
      });
    }
    $scope.equalHeight = function() {
      setTimeout(function() {
        equalHeight($(".nk-col .caption"));
      }, 500);
      //$ionicLoading.hide();
    }
  }])
  .directive('myRepeatDirective', function() {
    return function(scope, element, attrs) {
      if (scope.$last) {
        equalHeight($(".nk-col .caption"));
        scope.ionicLoading.hide();
      }
    };
  })
  .controller('claimdetailCtrl', ['Api', 'DbService', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', '$cordovaSQLite', '$filter', function(Api, DbService, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams, $cordovaSQLite, $filter) {

    setPopupAddNote($scope, $ionicPopup, $rootScope);
    setPopupAddStructur($scope, $ionicPopup, $rootScope);
    setPopupshowMapInPopUp($scope, $ionicPopup, $rootScope);
    setPopupAddClaim($scope, $state, $ionicPopup, $rootScope);

    $scope.$on("$ionicView.beforeEnter", function(event, data) { //on dasbord load
      console.log($stateParams);
      if ($stateParams.id > 0) { // id of claim
        $rootScope.get_claimdetail($stateParams.id);
      }
    });
    //$rootScope.get_claimdetail($stateParams.id);
    $scope.claimPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "claimreports/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.claimDrying = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "drying/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim drying pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.pfiPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "pfiform/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.healthSafetyPDF = function(claim_id) {
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "healthsafety/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim Health & Safety pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }
    $scope.open_web_view = function(url) {
      window.open(url, '_blank');
    }
    $scope.saveFnol = function() {

      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      if (check_connection()) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'fnol': $rootScope.clientList.claims_fnol,
          'claim_id': $stateParams.id,
          'user_token': localStorage.getItem("UserToken")
        });

        var urlBase = Api.urlBase + "claims/updatefnol";

        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            DbService.UpdateFnol($rootScope.clientList.claims_fnol, $stateParams.id);
            $ionicLoading.hide();
          });
      } else if (localStorage.getItem("UserType") != 1) {
        DbService.UpdateFnol($rootScope.clientList.claims_fnol, $stateParams.id);
        DbService.syncStatusClaimsData($stateParams.id, '0');
        $ionicLoading.hide();
      } else {
        var alertPopup = $ionicPopup.alert({
          title: alertTitle,
          cssClass: 'notification',
          template: 'No internet connection !'
        });
        $ionicLoading.hide();
      }
    }
    $scope.recoveryFormPdf = function(claim_id) {
      //$scope.claim_id = $stateParams.claim_id;
      //$scope.url      = $stateParams.api_request;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'claim_id': claim_id,
        'user_token': localStorage.getItem("UserToken")
      });

      var urlBase = Api.urlBase + "recoveryform/pdf";

      var req = {
        headers: Api.HEADERS
      };
      $http.post(urlBase, pdata, req).then(
        function(response) {
          $ionicLoading.hide();
          var alertTitle = "Claim report pdf";
          if (response.data.status == 1) {
            $scope.claim_report_url = response.data.response.url;
            window.open($scope.claim_report_url, '_blank');

          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: alertTitle,
              cssClass: 'notification',
              template: response.data.message
            });
            alertPopup.then(function(res) {
              //location.reload();
            });
          }
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        });
    }

    $scope.editStructure = function(structure_id, structure_name) {
      /*console.log(structure_id);
      console.log(structure_name);
      return false;*/
      claim_id = $stateParams.id;
      if (structure_id > 0) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        if (check_connection()) {
          pdata = [];
          $scope.ServererrMsg = false;

          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'claim_id': claim_id,
            'id': structure_id,
            'structure_id': structure_id,
            'name': structure_name
          });
          var urlBase = Api.urlBase + "claimstructures/edit";
          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {

              if (response.data.status == 1) {
                if (localStorage.getItem("UserType") != 1) {
                  DbService.ClaimStructuresUpdate(structure_id, Api.formatDateDatabase(), localStorage.getItem("UserId"), structure_name)
                    .then(function(response) {
                      $("input[structure_id='" + structure_id + "']").removeAttr('style');
                      $("input[structure_id='" + structure_id + "']").parent('.cfontweight').find('a').show();
                      $ionicLoading.hide();
                    });
                }
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Edit Structure',
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Edit Structure',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else if (localStorage.getItem("UserType") != 1) {
          DbService.ClaimStructuresUpdate(structure_id, Api.formatDateDatabase(), localStorage.getItem("UserId"), structure_name)
            .then(function(response) {
              DbService.syncStatusClaimsData(claim_id, '0');
              $("input[structure_id='" + structure_id + "']").removeAttr('style');
              $("input[structure_id='" + structure_id + "']").parent('.cfontweight').find('a').show();
              $ionicLoading.hide();
            });
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Edit Structure',
            cssClass: 'notification',
            template: 'No internet connection !'
          });
        }
      }
    }
    $rootScope.edit_btn = function(edit_btn_id) {
      $("input[structure_id='" + edit_btn_id + "']").attr("style", "display:block !important;");
      $("input[structure_id='" + edit_btn_id + "']").parent('.cfontweight').find('a').hide();
    }
    $rootScope.get_claimdetail = function(claim_id) {
      $scope.from_dashboard = $stateParams.from_dashboard;
      //alert($stateParams.from_dashboard);
      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");
      claim_id = $stateParams.id;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });

      var db_survey = $cordovaSQLite.openDB({
        name: "survey.db",
        location: 'default'
      });

      /*var db = $cordovaSQLite.openDB({
        name: "survey.db",
        location: 'default'
      });*/
      var query = "SELECT " +
        "claims.id	as clsim_id,claims.db_id	as claims_db_id,claims.user_id	as claim_user_id," +
        "claims.client_id	as claim_client_id,claims.customer_name as claims_customer_name ,claims.address as claims_address," +
        "claims.address2 as claims_address2	,claims.city	as claims_city,claims.state as claims_state	,claims.zipcode as claims_zipcode," +
        "claims.phone as claims_phone	,claims.mobile as claims_mobile	,claims.email as claims_email	,claims.policy_number as claims_policy_number	," +
        "claims.claims_reported_perill as claims_reported_perill	,claims.st_reference as claims_st_reference	,claims.customer_reference as claims_customer_reference	," +
        "claims.incident_date as claims_incident_date	,claims.booked_date as claims_booked_date	,claims.booked_time as claims_booked_time	," +
        "claims.internal_notes as claims_internal_notes	,claims.fnol as claims_fnol	,claims.is_dashboard as claims_is_dashboard,claims.job_no as claims_job_no," +
        "claims.status as claims_status	,claims.assign_to	 as claims_assign_to,clients.contact_person_name," +
        "clients.id as clients_id,clients.db_id as clients_db_id,clients.client_name	as clients_client_name ,clients.contact_person_name as clients_contact_person_name,clients.client_address as clients_client_address,clients.client_city	 as clients_client_city,clients.client_state as	clients_client_state,clients.client_country as clients_client_country,clients.client_zipcode as clients_client_zipcode,clients.client_phone_no as clients_client_phone_no	,clients.client_fax_no as clients_client_fax_no	,clients.client_email as clients_client_email	,clients.client_warranty as clients_client_warranty	 ,clients.client_show_info as clients_client_show_info	,clients.client_image as	clients_client_image," +
        "users.id as users_id,users.assigned_date as users_assigned_date, users.db_id as users_db_id	,users.id_number as users_id_number	,users.firstname as users_firstname	,users.lastname as users_lastname	,users.address1 as users_address1	,users.address2 as users_address2	,users.city as users_city	,users.state as users_state	,users.country as users_country	,users.zip as users_zip	,users.email as users_email	,users.mobile as users_mobile	,users.vehical_reg as users_vehical_reg	,users.homephone as users_homephone	,users.profile_pic as users_profile_pic	" + " FROM claims " +
        "left join clients on clients.db_id = claims.client_id " +
        "left join users on users.db_id = claims.assign_to " +
        "where claims.db_id = ?";
      $cordovaSQLite.execute(db_survey, query, [claim_id]).then(function(res) {
        $rootScope.clientList = res['rows']['item'](0);

        //console.log($scope.clientList);
        $scope.claim_id = claim_id;
        $scope.file_path = cordova.file.dataDirectory + '/claims/' + claim_id;
        $scope.claim_status = $scope.clientList.claim_status;

        query = "select * from claim_internalnote where claim_id = ? order by created desc ";
        $cordovaSQLite.execute(db_survey, query, [claim_id]).then(function(res) {
          console.log(res);
          if (res['rows']['length'] > 0) {
            var i = 0;
            var claim_internalnotes = {};
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            while (res['rows']['length'] > i) {
              claim_internalnotes[i] = res['rows']['item'](i);
              var date_created = claim_internalnotes[i]['created'].split('-');
              claim_internalnotes[i]['created'] = date_created[2].split(' ')[0] + ' ' + monthNames[parseInt(date_created[1]) - 1] + ' ' + date_created[0] + ' ' + date_created[2].split(' ')[1];
              i = i + 1;
            }
            $rootScope.claim_internalnotes = claim_internalnotes;
          } else {
            $rootScope.claim_internalnotes = [];
          }

        });
        DbService.getReportStatus(claim_id)
          .then(function(response_report_status) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.response_report_status = response_report_status;
                console.log('$scope.response_report_status');
                console.log($scope.response_report_status);
              });
            } else {
              $scope.response_report_status = response_report_status;
              console.log('$scope.response_report_status');
              console.log($scope.response_report_status);
            }
          });
        DbService.getClaimStructures(claim_id)
          .then(function(ClaimStructures) {
            if (!$scope.$$phase) {
              $scope.$apply(function() {
                $scope.ClaimStructures = ClaimStructures;
                console.log('$scope.ClaimStructures');
                console.log($scope.ClaimStructures);
              });
            } else {
              $scope.ClaimStructures = ClaimStructures;
              console.log('$scope.ClaimStructures');
              console.log($scope.ClaimStructures);
            }
          });
        setTimeout(function() {
          //console.log($('.claimclientdetail').eq(0).html());
          //console.log($('.claimclientdetail').eq(1).html());
          var heights = angular.element('.claimclientdetail').eq(0).innerHeight();
          $('.claimclientdetail').eq(0).find(".cmob640").css({
            "height": heights
          });
          $('.claimclientdetail').eq(0).find(".box-border").css({
            "margin": "0px"
          });
          $('.claimclientdetail').eq(0).css({
            "margin": "0px"
          });
          //alert(heights);
          var heights = angular.element('.claimclientdetail').eq(1).innerHeight();
          $('.claimclientdetail').eq(1).find(".cmob640").css({
            "height": heights
          });
          $('.claimclientdetail').eq(1).find(".box-border").css({
            "margin": "0px"
          });
          $('.claimclientdetail').eq(1).css({
            "margin": "0px"
          });
          //alert(heights);
        }, 1000);
        $ionicLoading.hide();
      }, function(err) {
        console.error(err);
      });

    }
    $scope.chnage_status = function() {
      claim_id = $scope.claim_id;
      pass_status = $rootScope.clientList.claims_status;

      $scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");
      $scope.claim_id = localStorage.getItem("UserType");

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id,
        'status': pass_status
      });
      var urlBase = Api.urlBase + "claims/changestatus";
      var req = {
        headers: Api.HEADERS
      };
      if (check_connection()) {
        $http.post(urlBase, pdata, req).then(
          function(response) {
            if (response.data.status == 1) {
              console.log('----------------status responce -----------------');
              console.log(response.data);
              //alert($stateParams.id);
              if (pass_status == 'N') {
                pass_status = 'C';
              } else if (pass_status == 'C') {
                pass_status = 'N';
              }
              DbService.ClaimsStatusUpdate(claim_id, pass_status).then(function() {
                $rootScope.get_claimdetail($stateParams.id);
              });
            } else {
              var alertPopup = $ionicPopup.alert({
                title: 'Claim Detail',
                cssClass: 'notification',
                template: response.data.message
              });
            }
            $ionicLoading.hide();
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          }
        );
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'Delete Structure',
          cssClass: 'notification',
          template: 'No internet connection !'
        });
        $ionicLoading.hide();
      }
    }

    $scope.deleteStructure = function(structure_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete structure',
        template: 'Are you sure you want to delete structure ?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          if (check_connection()) {

            $scope.UserToken = localStorage.getItem("UserToken");
            $scope.UserId = localStorage.getItem("UserId");

            $ionicLoading.show({
              animation: 'fade-in',
              showBackdrop: true,
              maxWidth: 200,
              showDelay: 0
            });
            var pdata = jQuery.param({
              'user_id': localStorage.getItem("UserId"),
              'user_token': localStorage.getItem("UserToken"),
              'id': structure_id
            });
            var urlBase = Api.urlBase + "claimstructures/delete";
            var req = {
              headers: Api.HEADERS
            };

            $http.post(urlBase, pdata, req).then(
              function(response) {

                if (response.data.status == 1) {
                  console.log('----------------delete structure -----------------');
                  console.log(response.data);
                  if (localStorage.getItem("UserType") != 1) {
                    DbService.ClaimStructuresDelete(structure_id)
                      .then(function(delete_res) {
                        console.log(delete_res);
                        $rootScope.get_claimdetail($stateParams.id);
                      });
                  }
                  //alert($stateParams.id);
                  //$state.go("app.detailview",{'id':$stateParams.id});
                  //$rootScope.get_claimdetail($stateParams.id);
                } else {
                  var alertPopup = $ionicPopup.alert({
                    title: 'Delete Structure',
                    cssClass: 'notification',
                    template: response.data.message
                  });
                }
                $ionicLoading.hide();
              },
              function(error) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Structure',
                  cssClass: 'notification',
                  template: 'There is some server issue, please try again after some time'
                });
              }
            );
          } else if (localStorage.getItem("UserType") != 1) {
            DbService.ClaimStructuresDeleteStatus(structure_id, Api.formatDateDatabase(), localStorage.getItem("UserId")).then(function(res_delete_status) {
              $rootScope.get_claimdetail($stateParams.id);
              $ionicLoading.hide();
            });
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Delete Structure',
              cssClass: 'notification',
              template: 'No internet connection !'
            });
            $ionicLoading.hide();
          }
        }
      })
    };
    $rootScope.deleteClaim = function(claim_id) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete claim',
        template: 'Are you sure you want to delete this ?'
      });

      confirmPopup.then(function(res) {
        if (res) {

          $scope.UserToken = localStorage.getItem("UserToken");
          $scope.UserId = localStorage.getItem("UserId");

          $ionicLoading.show({
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
          var pdata = jQuery.param({
            'user_id': localStorage.getItem("UserId"),
            'user_token': localStorage.getItem("UserToken"),
            'claim_id': claim_id
          });
          var urlBase = Api.urlBase + "claims/delete";
          var req = {
            headers: Api.HEADERS
          };

          $http.post(urlBase, pdata, req).then(
            function(response) {
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  $scope.searchClaim(0);
                });
              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Delete Claim',
                  cssClass: 'notification',
                  template: response.data.message
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Delete Claim',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            }
          );
        }
      });
    };
    $rootScope.get_detail_live = function(claim_id, scope_var) {
      $rootScope.from_dashboard = $stateParams.from_dashboard;

      $rootScope.UserToken = localStorage.getItem("UserToken");
      $rootScope.UserId = localStorage.getItem("UserId");
      $rootScope.UserType = localStorage.getItem("UserType");
      //claim_id = $stateParams.id;
      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id
      });
      var urlBase = Api.urlBase + "claims/details";
      var req = {
        headers: Api.HEADERS
      };

      $http.post(urlBase, pdata, req).then(

        function(response) {
          if (response.data.status == 1) {
            $ionicLoading.hide();
            if (!$scope.$$phase) {
              $rootScope.$apply(function() {
                $rootScope.clientList = response.data.response;
                $rootScope.claim_id = scope_var.clientList.id;
                $rootScope.claim_status = scope_var.clientList.status;
              });
            } else {
              $rootScope.clientList = response.data.response;
              $rootScope.claim_id = scope_var.clientList.id;
              $rootScope.claim_status = scope_var.clientList.status;
            }
            setTimeout(function() {
              //console.log($('.claimclientdetail').eq(0).html());
              //console.log($('.claimclientdetail').eq(1).html());
              var heights = angular.element('.claimclientdetail').eq(0).innerHeight();
              $('.claimclientdetail').eq(0).find(".cmob640").css({
                "height": heights
              });
              $('.claimclientdetail').eq(0).find(".box-border").css({
                "margin": "0px"
              });
              $('.claimclientdetail').eq(0).css({
                "margin": "0px"
              });
              //alert(heights);
              var heights = angular.element('.claimclientdetail').eq(1).innerHeight();
              $('.claimclientdetail').eq(1).find(".cmob640").css({
                "height": heights
              });
              $('.claimclientdetail').eq(1).find(".box-border").css({
                "margin": "0px"
              });
              $('.claimclientdetail').eq(1).css({
                "margin": "0px"
              });
              //alert(heights);
            }, 1000);
            //return d.resolve(scope_var);
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: response.data.message
            });
          }

        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });

        }
      )

    }
  }])
  .controller('claimNotesCtrl', ['Api', 'DbService', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, DbService, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    $scope.formatDate = function() {
      var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      var h = addZero(d.getHours());
      var m = addZero(d.getMinutes());
      var s = addZero(d.getSeconds());
      //if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      ];
      return [day, monthNames[month - 1], year].join(' ') + " " + h + ":" + m + ":" + s;
    }
    $scope.addClaimNotes = function(addClaimNotefrm) {
      if (addClaimNotefrm.$valid) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        AddClaimNote = $('#frmAddClaimNote').serializeArray();
        var AddClaimNoteList = {};
        $.each(AddClaimNote, function(k, v) {
          AddClaimNoteList[v['name']] = v['value'];
        });

        if (check_connection()) { //is online
          pdata = [];
          $scope.ServererrMsg = false;
          console.log($('#frmAddClaimNote').serializeArray());

          var pdata = $('#frmAddClaimNote').serialize();
          var urlBase = Api.urlBase + "claims/addnotes";
          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              /*console.log(response);
              return;*/
              $ionicLoading.hide();
              if (response.data.status == 1) {
                id = response.data.response['id'];
                created = response.data.response['created'];
                user_id = response.data.response['user']['id'];
                user_name = response.data.response['user']['firstname'] + ' ' + response.data.response['user']['lastname'];
                if (localStorage.getItem('UserType') != 1) {
                  DbService.addClaimInternalnote(id, $stateParams.id, AddClaimNoteList['internal_notes'], created, user_name, user_id);
                }
                var alertPopup = $ionicPopup.alert({
                  title: 'Claim Notes',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  $rootScope.AddClaimNotePopup.close();
                  if (localStorage.getItem('UserType') != 1) {
                    $rootScope.get_claimdetail($stateParams.id);
                  } else {
                    $rootScope.get_detail_live($stateParams.id, $scope);
                  }
                });
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Add Internal Notes',
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Internal Notes',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else if (localStorage.getItem("UserType") != 1) {

          $rootScope.AddClaimNotePopup.close();

          user_detail = JSON.parse(localStorage.getItem("UserData"));
          created = $scope.formatDate();
          user_name = user_detail['firstname'] + user_detail['lastname'];
          user_id = user_detail['id'];
          DbService.addClaimInternalnoteOffline(null, $stateParams.id, AddClaimNoteList['internal_notes'], created, 0, user_name, user_id).then(function() {
            $rootScope.get_claimdetail($stateParams.id);
            DbService.syncStatusClaimsData($stateParams.id, '0');
            $ionicLoading.hide();
          });
        } else {
          $ionicLoading.hide()
          $rootScope.AddClaimNotePopup.close();
          var alertPopup = $ionicPopup.alert({
            title: 'Add Internal Notes',
            cssClass: 'notification',
            template: 'No internet connection !'
          });
        }
      }
    }
  }])
  .controller('structureCtrl', ['Api', 'DbService', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', '$filter', function(Api, DbService, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams, $filter) {
    $scope.addStructure = function(structure_id, structure_name) {
      claim_id = $stateParams.id;
      if (structure_id > 0) {
        $ionicLoading.show({
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        pdata = [];
        $scope.ServererrMsg = false;
        var StructureData = {
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claim_id': claim_id,
          'structure_id': structure_id,
          'created': $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
          'name': structure_name
        }
        if (check_connection()) {
          var pdata = jQuery.param(StructureData);
          var urlBase = Api.urlBase + "claimstructures/add";
          var req = {
            headers: Api.HEADERS
          };
          $http.post(urlBase, pdata, req).then(
            function(response) {
              structure_response = response['data']['response'];
              if (localStorage.getItem("UserType") != 1) {
                DbService.setClaimStructure(structure_response.id, structure_response.user_id, structure_response.claim_id, structure_response.structure_id, structure_response.name, Api.formatDateDatabase(structure_response.created), Api.formatDateDatabase(structure_response.modified), Api.formatDateDatabase(structure_response.deleted), structure_response.modified_by, structure_response.deleted_by, structure_response.status, 1);
              }
              $ionicLoading.hide();
              if (response.data.status == 1) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Add Structure',
                  cssClass: 'notification',
                  template: response['data']['message']
                });
                alertPopup.then(function(res) {
                  console.log($rootScope);
                  $rootScope.AddStructur.close();
                  if (localStorage.getItem('UserType') != 1) {
                    $rootScope.get_claimdetail($stateParams.id);
                  } else {
                    $rootScope.get_detail_live($stateParams.id);
                  }

                  //$state.go("app.detailview",{'id':$stateParams.id});
                });
              } else {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Add Structure',
                  cssClass: 'notification',
                  template: response.data.message
                });
                alertPopup.then(function(res) {
                  //location.reload();
                });
              }
            },
            function(error) {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Structure',
                cssClass: 'notification',
                template: 'There is some server issue, please try again after some time'
              });
            });
        } else {
          if (localStorage.getItem("UserType") != 1) {
            $ionicLoading.hide();
            var structure_response = {
              'id': '00' + ($filter('date')(new Date(), 'yyyyMMddHHmmss')),
              'user_id': localStorage.getItem("UserId"),
              'claim_id': claim_id,
              'structure_id': structure_id,
              'name': structure_name,
              'created': $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
              'modified': null,
              'deleted': null,
              'modified_by': null,
              'deleted_by': 0,
              'status': 1
            };

            DbService.setClaimStructure(structure_response.id, structure_response.user_id, structure_response.claim_id, structure_response.structure_id, structure_response.name, structure_response.created, structure_response.modified, structure_response.deleted, structure_response.modified_by, structure_response.deleted_by, structure_response.status, 0)
              .then(function() {
                $rootScope.AddStructur.close();
                DbService.syncStatusClaimsData(claim_id, '0');
                $rootScope.get_claimdetail($stateParams.id);
              });
          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Structure',
              cssClass: 'notification',
              template: 'No internet connection !'
            });
          }
        }
      }
    }

    $scope.addStructureList = function() {
      claim_id = $stateParams.id;

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      pdata = [];
      $scope.ServererrMsg = false;
      if (check_connection()) {
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'claim_id': claim_id,
          'parent_id': $rootScope.current_structure_page,
          'name': $("input[name='name']").val()
        });
        var urlBase = Api.urlBase + "claimstructures/addstructure";
        var req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            $ionicLoading.hide();
            console.log(response.data);
            if (response.data.status == 1) {
              DbService.addStructure(response.data.response).then(function() {
                $scope.getStructure($rootScope.current_structure_page);
              });
              $("input[name='name']").val('');
            } else {
              $ionicLoading.hide();
              var alertPopup = $ionicPopup.alert({
                title: 'Add Structure',
                cssClass: 'notification',
                template: response.data.message
              });
              alertPopup.then(function(res) {
                //location.reload();
              });
            }
          },
          function(error) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Add Structure',
              cssClass: 'notification',
              template: 'There is some server issue, please try again after some time'
            });
          });
      } else {

      }
    }
    $scope.getStructure = function(parent_id) {
      $rootScope.current_structure_page = parent_id;
      /*$scope.UserToken = localStorage.getItem("UserToken");
      $scope.UserId = localStorage.getItem("UserId");
      $scope.UserType = localStorage.getItem("UserType");
      claim_id = $stateParams.id;
      $rootScope.current_structure_page = parent_id;

      $ionicLoading.show({
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      var pdata = jQuery.param({
        'user_id': localStorage.getItem("UserId"),
        'user_token': localStorage.getItem("UserToken"),
        'claim_id': claim_id,
        'parent_id': parent_id
      });
      var urlBase = Api.urlBase + "claimstructures/getStructuredata";
      var req = {
        headers: Api.HEADERS
      };

      $http.post(urlBase, pdata, req).then(

        function(response) {
          console.log(response);
          if (response.data.status == 1) {
            $scope.StructuretList = response.data.response;

            if ($scope.StructuretList.length > 0 && parent_id != 0) {
              $scope.backParentId = $scope.StructuretList[0]['previous_parent_id'];
            } else {
              $scope.backParentId = null
            }
            $('.popup-title.ng-binding').html(response.data.resposetitle.title.toUpperCase());
            $scope.button_title = response.data.resposetitle.btntitle;
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Claim Detail',
              cssClass: 'notification',
              template: response.data.message
            });
          }
          $ionicLoading.hide();
        },
        function(error) {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Claim Detail',
            cssClass: 'notification',
            template: 'There is some server issue, please try again after some time'
          });
        }
      );
    }

    $scope.getStructure(0);*/
      DbService.getStructures(parent_id, $stateParams.id)
        .then(function(StructuresResponse) {
          //console.log(StructuresResponse);
          if (StructuresResponse['back_button_val'] == 0) {
            $('.popup-title.ng-binding').html("SELECT INTERIOR OR EXTERIOR FROM LIST BELOW".toUpperCase());
          } else if (StructuresResponse['back_button_val'] == null) {
            $('.popup-title.ng-binding').html("SELECT FROM THE LIST BELOW OR ADD A NEW STRUCTURE".toUpperCase());
          } else {
            $('.popup-title.ng-binding').html("SELECT FROM THE LIST BELOW OR ADD A NEW " + StructuresResponse['back_button_value'].toUpperCase());
          }
          if (!$scope.$$phase) {
            $scope.$apply(function() {
              $scope.StructureList = StructuresResponse;
            });
          } else {
            $scope.StructureList = StructuresResponse;
          }
        });
    }
    $scope.getStructure(0);
  }])
  .controller('pdfCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {

    $scope.claim_id = $stateParams.claim_id;
    $scope.url = $stateParams.api_request;
    $ionicLoading.show({
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    var pdata = jQuery.param({
      'user_id': localStorage.getItem("UserId"),
      'claim_id': $stateParams.claim_id,
      'user_token': localStorage.getItem("UserToken")
    });

    var urlBase = Api.urlBase + "claimreports/pdf";

    var req = {
      headers: Api.HEADERS
    };
    $http.post(urlBase, pdata, req).then(
      function(response) {
        $ionicLoading.hide();
        var alertTitle = "Claim report pdf";
        if (response.data.status == 1) {
          $scope.claim_report_url = response.data.response.url;
          window.open($scope.claim_report_url, '_system', 'location=yes');
          //$('iframe').attr('src',$scope.claim_report_url);
          //$('iframe').css('height',$(window).height() - $('.inner-header').height() - $('.bar-stable.bar.bar-header').height() - 30);
        } else {
          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: alertTitle,
            cssClass: 'notification',
            template: response.data.message
          });
          alertPopup.then(function(res) {
            //location.reload();
          });
        }
      },
      function(error) {
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: alertTitle,
          cssClass: 'notification',
          template: 'There is some server issue, please try again after some time'
        });
      });
  }])
  .controller('CarouselDemoCtrl', ['Api', '$scope', '$state', '$http', '$ionicPopup', '$ionicLoading', '$rootScope', '$cordovaNetwork', '$cordovaFileTransfer', '$stateParams', function(Api, $scope, $state, $http, $ionicPopup, $ionicLoading, $rootScope, $cordovaNetwork, $cordovaFileTransfer, $stateParams) {
    $scope.myInterval = 1000;
    $scope.slides = [{
        image: 'http://lorempixel.com/400/200/'
      },
      {
        image: 'http://lorempixel.com/400/200/food'
      },
      {
        image: 'http://lorempixel.com/400/200/sports'
      },
      {
        image: 'http://lorempixel.com/400/200/people'
      }
    ];
  }])
  .directive('repeatDone', function() {
    return function(scope, element, attrs) {
      if (scope.$last) { // all are rendered
        scope.$eval(attrs.repeatDone);
      }
    }
  });
