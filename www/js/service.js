angular.module('starter.services', [])
  .service("DbService", ['Api', '$ionicPlatform', '$http', '$filter', '$rootScope', '$cordovaSQLite', '$cordovaFile', '$cordovaFileTransfer', '$q', function(Api, $ionicPlatform, $http, $filter, $rootScope, $cordovaSQLite, $cordovaFile, $cordovaFileTransfer, $q) {
    DbService = this;
    $ionicPlatform.ready(function() {
      this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });
    });
    //CLAIM TABLES
    this.CreateClaimInternalnote = "CREATE TABLE IF NOT EXISTS `claim_internalnote` (`id`	INTEGER,	`claim_id`	TEXT,	`internal_notes`	TEXT,	`created`	TEXT,`sync` TEXT,`user_name` TEXT,`user_id` TEXT);";
    this.Createsowtask = "CREATE TABLE IF NOT EXISTS `sowtask` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,	`id`	TEXT,	`user_id`	TEXT,	`claimstructure_id`	TEXT,	`trade_id`	TEXT,`task_id` TEXT,`surveyor_id` TEXT,`start_date` TEXT,`end_date` TEXT,`measure` TEXT,`uom` TEXT,`status` TEXT,`created` TEXT,`modified` TEXT,`deleted` TEXT,`modified_by` TEXT,`deleted_by` TEXT);";
    this.Createstructuremedia = "CREATE TABLE IF NOT EXISTS `structuremedia` (`db_id` INTEGER PRIMARY KEY AUTOINCREMENT, `id` TEXT,  `claimstructure_id` TEXT,  `user_id` TEXT,`media_file` TEXT,  `filetype` TEXT,    `created` TEXT,  `modified` TEXT,  `deleted` TEXT, `modified_by` TEXT,  `deleted_by` TEXT,  `status` TEXT,`sync` TEXT);";
    this.Createrecoveryform = "CREATE TABLE IF NOT EXISTS `recoveryform` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,`id`	INTEGER,`user_id`	TEXT,`claim_id`	TEXT,`allegation_negligence`	TEXT,`third_party`	TEXT,`response_referral`	TEXT,`followup1`	TEXT,`followup2`	TEXT,`aditional_followup`	TEXT,`final_action`	TEXT,`created`	TEXT);"
    this.Createstructurenotes = "CREATE TABLE IF NOT EXISTS `structurenotes` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,	`id`	TEXT,	`claimstructure_id`	TEXT,	`user_id`	TEXT,	`internal_notes`	TEXT,`username` TEXT,`created` TEXT,`modified` TEXT,`deleted` TEXT,`modified_by` TEXT,`deleted_by` TEXT,`status` TEXT);";
    this.Createpfiform = "CREATE TABLE IF NOT EXISTS `pfiform` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,`id`	INTEGER,`user_id`	TEXT,`claim_id`	TEXT,`fraud_type`	TEXT,`referral_reason`	TEXT,`concerns_reason`	TEXT,`customer_challenged`	TEXT,`response_referral`	TEXT,`followup1`	TEXT,`followup2`	TEXT,`aditional_followup`	TEXT,`final_action`	TEXT,`created`	TEXT);";
    this.Createdrying = "CREATE TABLE IF NOT EXISTS `drying` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,`id`	INTEGER,`user_id`	TEXT,`claim_id`	TEXT,`property_address`	TEXT,`customer_name`	TEXT,`firstcall_reference`	TEXT,`drying_date` TEXT,`incident_date`	TEXT,`firstcall_name`	TEXT,`firstcall_position`	TEXT,`firstcall_signature`	TEXT,`created`	TEXT);";
    this.Createclaimreports = "CREATE TABLE IF NOT EXISTS `claimreports` (`db_id` INTEGER PRIMARY KEY AUTOINCREMENT,`id` INTEGER,`user_id` TEXT,`claim_id` TEXT,`property_id` TEXT,`propertyage_id` TEXT,`propertycondition_id` TEXT,`storey_id` TEXT,`specify_storey` TEXT," + "`rooftype_id` TEXT,`specify_rooftype` TEXT,`roofmaterial_id` TEXT,`specify_roofmaterial` TEXT,`walltype_id` TEXT,`specify_walltype` TEXT, `listed_building` TEXT," +
      " `listing_type` TEXT, `property_unoccupied` TEXT,`unoccupied_days` TEXT,`unoccupied_months` TEXT,`unoccupied_years` TEXT,`unoccupied_date` TEXT,`unoccupied_length` TEXT,  `multiple_flats` TEXT,  `specify_multiple_flats` TEXT, " +
      " `actualperil_id` TEXT,  `roomsaffected_id` TEXT,  `specify_roomafftected` TEXT,  `recovery_opportunity` TEXT,  `crime_reference` TEXT,  `eow_reason` TEXT,  `inspection_performed` TEXT,  `ta_started` TEXT,  `ta_completed` TEXT, " +
      " `strip_outworks_completed` TEXT,  `nirs_completed` TEXT,  `drying_end` TEXT,  `aa_type` TEXT,  `aa_start_date` TEXT, `aa_end_date_estimated` TEXT,`aa_end_date_actual` TEXT,`business_trading` TEXT,`reinstatement_type` TEXT,`works_started` TEXT," +
      "`works_completed` TEXT,`estimated_works_completed` TEXT,`strip_outworks` TEXT,`drying_equipment_installed` TEXT,`drying_equipment_removed` TEXT,`reinstatement_started` TEXT,`customer_completion` TEXT,`reserves_content` TEXT,`reserves_building` TEXT," +
      "`reserves_fees` TEXT,`reports` TEXT,`requirements` TEXT,`environments` TEXT,`substances` TEXT,`access_and_egress` TEXT, `plant_uses` TEXT,`assign_cmd` TEXT,`health_safety_comments` TEXT,`customer_considerations` TEXT,`created` TEXT,`eurotempest` TEXT);";
    this.Createhealthsafety = "CREATE TABLE IF NOT EXISTS `healthsafety` (`db_id`	INTEGER PRIMARY KEY AUTOINCREMENT,`id`	INTEGER,`user_id`	TEXT,`claim_id`	TEXT,`environments`	TEXT,`substances`	TEXT,`access_and_egress`	TEXT,`plant_uses` TEXT,`assign_cmd`	TEXT,`health_safety_comments`	TEXT,`customer_considerations`	TEXT,`created`	TEXT);";
    this.claimstructures = "CREATE TABLE IF NOT EXISTS `claimstructures` (`db_id` INTEGER PRIMARY KEY AUTOINCREMENT, `id` TEXT,  `user_id` TEXT,  `claim_id` TEXT,  `structure_id` TEXT,  `name` TEXT,  `created` TEXT,  `modified` TEXT,  `deleted` TEXT, `modified_by` TEXT,  `deleted_by` TEXT,  `status` TEXT, `sync` TEXT);";
    this.claimsTable = "CREATE TABLE IF NOT EXISTS `claims` (  `id` INTEGER PRIMARY KEY AUTOINCREMENT,db_id INTEGER,  `user_id` TEXT,  `client_id` TEXT,  `customer_name` TEXT,  `address` TEXT,  `address2` TEXT,  `city` TEXT,  `state` TEXT,  `zipcode` TEXT,  `latitude` TEXT,  `longitude` TEXT,  `phone` TEXT,  `mobile` TEXT,  `email` TEXT,  `policy_number` TEXT,  `reported_perill` TEXT,  `st_reference` TEXT, `customer_reference` TEXT,  `incident_date` TEXT,  `booked_date` TEXT,  `booked_time` TEXT,  `internal_notes` TEXT,  `fnol` TEXT,`is_dashboard` TEXT,`job_no` TEXT,`status` TEXT,`assign_to` TEXT,claims_reported_perill TEXT,`sync` TEXT,`created` TEXT,`listing` TEXT);";

    this.DatabaseClaimTable = [
      DbService.CreateClaimInternalnote,
      DbService.Createsowtask,
      DbService.Createstructuremedia,
      DbService.Createrecoveryform,
      DbService.Createstructurenotes,
      DbService.Createpfiform,
      DbService.Createdrying,
      DbService.Createclaimreports,
      DbService.Createhealthsafety,
      DbService.claimstructures,
      'CREATE INDEX IF NOT EXISTS claims_db_id ON claims(db_id);',
      'CREATE INDEX IF NOT EXISTS claims_sync ON claims(sync);',
      'CREATE INDEX IF NOT EXISTS internalnote_claim_id ON claim_internalnote(claim_id);',
      'CREATE INDEX IF NOT EXISTS pfiform_claim_id ON pfiform(claim_id);',
      'CREATE INDEX IF NOT EXISTS drying_claim_id ON drying(claim_id);',
      'CREATE INDEX IF NOT EXISTS claimreports_claim_id ON claimreports(claim_id);',
      'CREATE INDEX IF NOT EXISTS recoveryform_claim_id ON recoveryform(claim_id);',
      'CREATE INDEX IF NOT EXISTS claimstructures_claim_id ON claimstructures(claim_id);',
      'CREATE INDEX IF NOT EXISTS sowtask_claimstructure_id ON sowtask(claimstructure_id);',
      'CREATE INDEX IF NOT EXISTS claimstructures_claimstructure_id ON structuremedia(claimstructure_id);',
      'CREATE INDEX IF NOT EXISTS claimstructures_claimstructure_id ON structurenotes(claimstructure_id);'
    ];

    this.addClaim = function(db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, is_dashboard, status, job_no, assign_to, reported_perill_value, created, listing) {
      incident_date = Api.formatDate(incident_date);
      created = Api.formatDateDatabase(created);
      incident_date = incident_date.split(' ')[0] + ' ' + incident_date.split(' ')[1] + ' ' + incident_date.split(' ')[2];
      if (listing == null || listing == '' || typeof listing == 'undefined') {
        listing = 0;
      }
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.claimsTable);
      });
      this.db.transaction(function(tx) {
        var date = new Date();
        var offset = date.getTimezoneOffset();
        var device_time = $filter('date')(date, 'yyyy-MM-dd HH:mm:ss');
        var Geo_LatestLat = localStorage.getItem("Geo_LatestLat");
        var Geo_LatestLon = localStorage.getItem("Geo_LatestLon");
        console.log('-claim detail--');
        console.log([db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, is_dashboard, status, assign_to, reported_perill_value]);
        console.log('-claim end detail--');
        if (listing == 1) {
          var query = "UPDATE claims SET `db_id`=(?),`user_id`=(?),`client_id`=(?),`customer_name`=(?),`address`=(?),`address2`=(?),`city`=(?),`state`=(?),`zipcode`=(?),`latitude`=(?),`longitude`=(?),`phone`=(?),`mobile`=(?),`email`=(?),`policy_number`=(?),`reported_perill`=(?),`st_reference`=(?),`customer_reference`=(?),`incident_date`=(?),`booked_date`=(?),`booked_time`=(?),`internal_notes`=(?),`fnol`=(?),`listing`=(?),`status`=(?),`assign_to` = (?),`claims_reported_perill` = (?),`sync` = (?) ,`created` = (?) where `db_id` = (?)";
          tx.executeSql(query, [db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, listing, status, assign_to, reported_perill_value, '1', created, db_id], function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO claims (`db_id`,`user_id`,`client_id`,`customer_name`,`address`,`address2`,`city`,`state`,`zipcode`,`latitude`,`longitude`,`phone`,`mobile`,`email`,`policy_number`,`reported_perill`,`st_reference`,`customer_reference`,`incident_date`,`booked_date`,`booked_time`,`internal_notes`,`fnol`,`listing`,`job_no`,`status`,`assign_to`,`claims_reported_perill`,`sync`,`created`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, listing, job_no, status, assign_to, reported_perill_value, '1', created],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          });
        } else {
          var query = "UPDATE claims SET `db_id`=(?),`user_id`=(?),`client_id`=(?),`customer_name`=(?),`address`=(?),`address2`=(?),`city`=(?),`state`=(?),`zipcode`=(?),`latitude`=(?),`longitude`=(?),`phone`=(?),`mobile`=(?),`email`=(?),`policy_number`=(?),`reported_perill`=(?),`st_reference`=(?),`customer_reference`=(?),`incident_date`=(?),`booked_date`=(?),`booked_time`=(?),`internal_notes`=(?),`fnol`=(?),`is_dashboard`=(?),`status`=(?),`assign_to` = (?),`claims_reported_perill` = (?),`sync` = (?),created = (?) where `db_id` = (?)";
          tx.executeSql(query, [db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, is_dashboard, status, assign_to, reported_perill_value, '1', created, db_id], function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO claims (`db_id`,`user_id`,`client_id`,`customer_name`,`address`,`address2`,`city`,`state`,`zipcode`,`latitude`,`longitude`,`phone`,`mobile`,`email`,`policy_number`,`reported_perill`,`st_reference`,`customer_reference`,`incident_date`,`booked_date`,`booked_time`,`internal_notes`,`fnol`,`is_dashboard`,`job_no`,`status`,`assign_to`,`claims_reported_perill`,`sync`,created) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [db_id, user_id, client_id, customer_name, address, address2, city, state, zipcode, Geo_LatestLat, Geo_LatestLon, phone, mobile, email, policy_number, reported_perill, st_reference, customer_reference, incident_date, booked_date, booked_time, internal_notes, fnol, is_dashboard, job_no, status, assign_to, reported_perill_value, created, '1'],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          });
        }
      });
    }
    this.addUsers = function(db_id, id_number, firstname, lastname, address1, address2, city, state, country, zip, email, mobile, vehical_reg, homephone, profile_pic, profile_pick_full_url, claim_id, assigned_date) {
      fiels = profile_pick_full_url.split('/');
      file_name = fiels[fiels.length - 1];
      fullPath = "/";
      folder = 'claims/' + claim_id + "/surveyor_image/";
      folders = folder.split('/');
      fullPath = '/';
      i = 0;

      while (i < folders.length) {
        fullPath = fullPath + '/' + folders[i];
        $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false);
        i = i + 1;
      }

      fp = file_name;
      var fileTransfer = new FileTransfer();
      fileTransfer.download(profile_pick_full_url, cordova.file.dataDirectory + folder + fp,
        function(entry) {

          /*this.db = window.sqlitePlugin.openDatabase({
            name: 'survey.db',
            location: 'default'
          });*/
          this.db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS `users` (  `id` INTEGER PRIMARY KEY AUTOINCREMENT, db_id INTEGER,  `id_number` TEXT,  `firstname` TEXT,  `lastname` TEXT,  `address1` TEXT,  `address2` TEXT,  `city` TEXT,  `state` TEXT,`country` TEXT,`zip` TEXT, `email` TEXT, `mobile` TEXT,`vehical_reg` TEXT,`homephone` TEXT,`profile_pic` TEXT,'assigned_date' TEXT);");
          });
          this.db.transaction(function(tx) {
            // File download function with URL and local path

            var query = "UPDATE users SET `db_id`=(?),`id_number`=(?),`firstname`=(?),`lastname`=(?),`address1`=(?),`address2`=(?),`city`=(?),`state`=(?),`country`=(?),`zip`=(?),`email`=(?),`mobile`=(?),`vehical_reg`=(?),`homephone`=(?),`profile_pic`=(?),`assigned_date` = (?) where `db_id` = (?)";
            tx.executeSql(query, [db_id, id_number, firstname, lastname, address1, address2, city, state, country, zip, email, mobile, vehical_reg, homephone, profile_pic, assigned_date, db_id], function(tx, res) {
              if (res.rowsAffected == 0) {
                var query = "INSERT INTO users (`db_id`,`id_number`,`firstname`,`lastname`,`address1`,`address2`,`city`,`state`,`country`,`zip`,`email`,`mobile`,`vehical_reg`,`homephone`,`profile_pic`,`assigned_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                tx.executeSql(query, [db_id, id_number, firstname, lastname, address1, address2, city, state, country, zip, email, mobile, vehical_reg, homephone, profile_pic, assigned_date],
                  function(tx, res) {
                    console.log(res);
                  },
                  function(tx, error) {
                    console.log('INSERT error -----------: ' + error.message);
                  });
              }
            });
          });
        },
        function(error) {
          console.log(error)
        });
    }
    this.addClaimInternalnoteOffline = function(id, claim_id, internal_notes, created, sync, user_name, user_id) {
      created = Api.formatDateDatabase(created);
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.CreateClaimInternalnote);
      });
      var d = $q.defer();

      this.db.transaction(function(tx) {
        /*var update_query = "UPDATE claim_internalnote set `id` = (?),`claim_id`= (?),`internal_notes`= (?),`created`= (?),`sync`= (?),`user_name`= (?),`user_id`= (?) WHERE `id` = (?)";
        tx.executeSql(update_query, [id, claim_id, internal_notes, created, sync, user_name, user_id, id],
          function(tx, res) {
            if (res.rowsAffected == 0) {*/
        //this.db.transaction(function(tx) {
        var query = "INSERT INTO claim_internalnote (`id`,`claim_id`,`internal_notes`,`created`,`sync`,`user_name`,`user_id`) VALUES (?,?,?,?,?,?,?)";
        tx.executeSql(query, [id, claim_id, internal_notes, created, sync, user_name, user_id],
          function(tx, res) {
            console.log(res);
            return d.resolve('done INSERT');
          },
          function(tx, error) {
            console.log('INSERT error -----------: ' + error.message);
            return d.resolve('done INSERT');
          });
        //});
        /*    }
          },
          function(tx, error) {
            console.log('UPDATE error -----------: ' + error.message);
            return d.resolve('error INSERT');
          });*/
      });
      return d.promise;
    }
    this.addStructurnotes = function(structurenotes) {
      if (structurenotes.created == '') {
        structurenotes.created = Api.formatDate('');
      }
      if (typeof structurenotes.user != 'undefined') {
        structurenotes.username = structurenotes.user['firstname'] + ' ' + structurenotes.user['lastname'];
      }
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructurenotes);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {

        var query_update = "UPDATE `structurenotes` SET `id` = (?), `claimstructure_id`= (?), `user_id`= (?), `internal_notes`= (?),`username` = (?), `created`= (?), `deleted`= (?), `modified_by` = (?), `deleted_by` = (?), `status`= (?) where id = (?)";
        tx.executeSql(query_update, [structurenotes.id, structurenotes.claimstructure_id, structurenotes.user_id, structurenotes.internal_notes, structurenotes.username, structurenotes.created, structurenotes.deleted, structurenotes.modified_by, structurenotes.deleted_by, structurenotes.status, structurenotes.id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO structurenotes (`id`,`claimstructure_id`,`user_id`,`internal_notes`,`username`,`created`,`modified`,`deleted`,`modified_by`,`deleted_by`,`status`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [structurenotes.id, structurenotes.claimstructure_id, structurenotes.user_id, structurenotes.internal_notes, structurenotes.username, structurenotes.created, structurenotes.modified, structurenotes.deleted, structurenotes.modified_by, structurenotes.deleted_by, structurenotes.status],
                function(tx, res) {
                  console.log(res);
                  return d.resolve('done INSERT');
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                  return d.resolve('error INSERT');
                });
            } else {
              return d.resolve('done INSERT');
            }
          },
          function(tx, error) {
            console.log('update error -----------: ' + error.message);
            return d.resolve('error INSERT');
          });
      });
      return d.promise;
    }
    this.DeleteStructurnotes = function(live_db_id, local_db_id, user_id) {
      var deleted = Api.formatDate('');
      var deleted_by = user_id;

      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructurenotes);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        if (live_db_id == '') {
          var delete_structurenotes = 'DELETE FROM structurenotes WHERE db_id = ? ';
          tx.executeSql(delete_structurenotes, [local_db_id],
            function(tx, res) {
              console.log(res);
              return d.resolve('done to Delete note');
            },
            function(tx, error) {
              console.log(error);
              return d.resolve('error to Delete note');
            });
        } else {
          var update_structurenotes = 'UPDATE structurenotes SET status = 2,deleted = "' + deleted + '",deleted_by = "' + user_id + '"  WHERE db_id = ? ';
          tx.executeSql(update_structurenotes, [local_db_id],
            function(tx, res) {
              console.log(res);
              return d.resolve('update status with delete');
            },
            function(tx, error) {
              console.log(error);
              return d.resolve('error to update status for note delete status');
            });
        }

      });
      return d.promise;
    }
    this.addSowtask = function(sowtaskModel) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createsowtask);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var query_update = "UPDATE `sowtask` SET `id` = (?), `user_id`= (?), `claimstructure_id`= (?), `trade_id`= (?), `surveyor_id`= (?), `start_date`= (?), `end_date` = (?), `measure` = (?), `uom`= (?), `status` =(?), `created` =(?), `modified` =(?), `deleted` =(?), `modified_by` =(?), `deleted_by` =(?) where id = (?)";
        tx.executeSql(query_update, [sowtaskModel.id, sowtaskModel.user_id, sowtaskModel.claimstructure_id, sowtaskModel.trade_id, sowtaskModel.surveyor_id, sowtaskModel.start_date, sowtaskModel.end_date, sowtaskModel.measure, sowtaskModel.uom, sowtaskModel.status, sowtaskModel.created, sowtaskModel.modified, sowtaskModel.deleted, sowtaskModel.modified_by, sowtaskModel.deleted_by, sowtaskModel.id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO `sowtask` (`id`,`db_id`,`user_id`,`claimstructure_id`,`trade_id`,`task_id`,`surveyor_id`,`start_date`,`end_date`,`measure`,`uom`,`status`,`created`,`modified`,`deleted`,`modified_by`,`deleted_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [sowtaskModel.id, sowtaskModel.id, sowtaskModel.user_id, sowtaskModel.claimstructure_id, sowtaskModel.trade_id, sowtaskModel.task_id, sowtaskModel.surveyor_id, sowtaskModel.start_date, sowtaskModel.end_date, sowtaskModel.measure, sowtaskModel.uom, sowtaskModel.status, sowtaskModel.created, sowtaskModel.modified, sowtaskModel.deleted, sowtaskModel.modified_by, sowtaskModel.deleted_by],
                function(tx, res) {
                  return d.resolve('done INSERT');
                  console.log(res);
                },
                function(tx, error) {
                  return d.resolve('error INSERT');
                  console.log('INSERT error -----------: ' + error.message);
                });
            } else {
              return d.resolve('done INSERT');
            }
          },
          function(tx, error) {
            console.log('update error -----------: ' + error.message);
            return d.resolve('done INSERT');
          });
      });
      return d.promise;
    }
    this.getStructurClaimNo = function(structur_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      var d = $q.defer();
      this.db.transaction(function(tx) {

        tx.executeSql("SELECT claimstructures.name,claims.job_no as job_number FROM claimstructures" +
          " LEFT JOIN claims ON claims.db_id = claimstructures.claim_id WHERE claimstructures.id= ?", [structur_id],
          function(tx, res) {
            if (res['rows']['length']) {
              res_StructurClaimNo = res['rows']['item'](0);
            } else {
              res_StructurClaimNo = {};
            }

            return d.resolve(res_StructurClaimNo);
          },
          function(tx, error) {
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.getClaimPFI = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createpfiform);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM pfiform where claim_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length']) {
            res_pfi = res['rows']['item'](0);
          } else {
            res_pfi = {};
          }
          var claim_pfi = {};
          //delete res_pfi.id;
          claim_pfi[claim_id] = res_pfi;
          return d.resolve(claim_pfi);
        },
        function(error) {
          var claim_pfi = {};
          claim_pfi[claim_id] = {};
          return d.resolve(claim_pfi);
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.getClaimFnol = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createpfiform);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM claims where db_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length'] > 0) {
            var fnol_data = res['rows']['item'](0);

            if (res['rows']['item'](0)['fnol'] == null) {
              res_claims_fnol = '';
            } else {
              res_claims_fnol = res['rows']['item'](0)['fnol'];
            }
          } else {
            res_claims_fnol = '';
          }
          var claims_fnol = {};
          //delete res_pfi.id;
          claims_fnol[claim_id] = res_claims_fnol;
          return d.resolve(claims_fnol);
        },
        function(error) {
          console.log('error in get fnol data');
        })
      return d.promise;
    }
    this.getSOWList = function(structure_id, claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createsowtask);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {

        tx.executeSql("SELECT tasks.task_name as task_name,trades.name as trade_name,sowtask.* FROM sowtask" +
          " LEFT JOIN tasks on tasks.id = sowtask.task_id" +
          " LEFT JOIN trades on trades.id = sowtask.trade_id" +
          " WHERE sowtask.claimstructure_id = ? and sowtask.status = 1 ", [structure_id],
          function(tx, res) {
            var sowtask_list = [];
            if (res['rows']['length']) {
              for (i = 0; i < res['rows']['length']; i++) {
                sowtask_list.push(res['rows']['item'](i));
              }
            } else {
              sowtask_list = [];
            }
            var res_sowtask = {};
            //delete res_pfi.id;
            res_sowtask[claim_id] = {
              structure_id
            };
            res_sowtask[claim_id][structure_id] = sowtask_list;
            return d.resolve(res_sowtask);
          },
          function(tx, error) {
            return d.resolve({
              'error': error
            });
            console.log('error in get pfi data');
          })
      })
      return d.promise;
    }
    this.getSOWForSync = function(structure_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createsowtask);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM sowtask WHERE claimstructure_id = ? ", [structure_id],
        function(res) {
          var sowtask_list = [];
          if (res['rows']['length'] > 0) {
            for (i = 0; i < res['rows']['length']; i++) {
              sowtask_detl = res['rows']['item'](i);
              sowtask_detl['db_id'] = sowtask_detl['id'];
              if (sowtask_detl['id'].substring(0, 2) == 0) {
                sowtask_detl['id'] = '';
              }
              sowtask_list.push(sowtask_detl);
            }
          } else {
            sowtask_list = [];
          }
          var res_sowtask = {};

          res_sowtask[structure_id] = sowtask_list;
          return d.resolve(res_sowtask);
        },
        function(error) {
          return d.resolve({
            'error': error
          });
          console.log('error in get pfi data');
        })
      return d.promise;
    }
    this.getStructurenotesSync = function(structure_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createsowtask);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM structurenotes WHERE claimstructure_id = ? ", [structure_id],
        function(res) {
          var structurenotes_list = [];
          if (res['rows']['length'] > 0) {
            for (i = 0; i < res['rows']['length']; i++) {
              structurenotes_detl = res['rows']['item'](i);
              structurenotes_detl['db_id'] = structurenotes_detl['id'];
              structurenotes_detl['created'] = Api.formatDateDatabase(structurenotes_detl['created']);
              if (structurenotes_detl['id'].toString().substring(0, 2) == 0) {
                structurenotes_detl['id'] = '';
              }
              structurenotes_list.push(structurenotes_detl);
            }
          } else {
            structurenotes_list = [];
          }
          var res_structurenotes = {};

          res_structurenotes[structure_id] = structurenotes_list;
          return d.resolve(res_structurenotes);
        },
        function(error) {
          return d.resolve({
            'error': error
          });
          console.log('error in structurenotes sync');
        });
      return d.promise;
    }
    this.getStructurenotes = function(structure_id, claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructurenotes);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {

        tx.executeSql("SELECT * FROM structurenotes" +
          " WHERE  structurenotes.claimstructure_id = ? and structurenotes.status = 1 ", [structure_id],
          function(tx, res) {
            var structurenotes_list = [];
            if (res['rows']['length']) {
              for (i = 0; i < res['rows']['length']; i++) {
                var structurenotes = res['rows']['item'](i);
                structurenotes.created = Api.formatDate(structurenotes.created);
                structurenotes_list.push(res['rows']['item'](i));
              }
            } else {
              structurenotes_list = [];
            }
            var res_structurenotes = {};
            //delete res_pfi.id;
            res_structurenotes[claim_id] = {
              structure_id
            };
            res_structurenotes[claim_id][structure_id] = structurenotes_list;
            return d.resolve(res_structurenotes);
          },
          function(tx, error) {
            return d.resolve({
              'error': error
            });
            console.log('error in get structure notes data');
          })
      })
      return d.promise;
    }

    this.getClaimMediaSync = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        tx.executeSql("SELECT structuremedia.id as media_db_id,claimstructures.db_id as claimstructures_folder_id,structuremedia.* FROM structuremedia" +
          " left join claimstructures on claimstructures.id = structuremedia.claimstructure_id WHERE claimstructures.claim_id = ? and structuremedia.sync = '0' ", [claim_id],
          function(tx, res) {
            var claimstructures = [];
            if (res['rows']['length']) {
              for (i = 0; i < res['rows']['length']; i++) {
                claimstructures.push(res['rows']['item'](i));
              }
            } else {
              claimstructures = [];
            }
            return d.resolve(claimstructures);
          },
          function(tx, error) {
            return d.resolve({
              'error': error
            });
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.getClaimMedia = function(structure_id, claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {

        tx.executeSql("SELECT * FROM structuremedia" +
          " WHERE structuremedia.claimstructure_id = ? and status = 1", [structure_id],
          function(tx, res) {
            var claimstructures = [];
            if (res['rows']['length']) {
              for (i = 0; i < res['rows']['length']; i++) {
                claimstructures.push(res['rows']['item'](i));
              }
            } else {
              claimstructures = [];
            }
            var claimstructureslist = {};
            //delete res_pfi.id;
            claimstructureslist[claim_id] = {
              structure_id
            };
            claimstructureslist[claim_id][structure_id] = claimstructures;
            return d.resolve(claimstructureslist);
          },
          function(tx, error) {
            return d.resolve({
              'error': error
            });
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.getDeleteClaimMedia = function(structure_id, claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createstructuremedia);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM structuremedia" +
        " WHERE structuremedia.claimstructure_id = ? and sync = 0", [structure_id],
        function(res) {
          var claimstructures = [];
          if (res['rows']['length'] > 0) {
            for (i = 0; i < res['rows']['length']; i++) {
              var structuremedia = res['rows']['item'](i);
              console.log('DB_IDS' + structuremedia.id.substr(0, 2));
              structuremedia.db_id = structuremedia.id;
              if (structuremedia.id.substr(0, 2) == '00') {
                structuremedia.id = 0;
              }
              claimstructures.push(structuremedia);
            }
          } else {
            claimstructures = [];
          }
          var claimstructureslist = {};
          //delete res_pfi.id;
          claimstructureslist[claim_id] = {
            structure_id
          };
          claimstructureslist[structure_id] = claimstructures;
          return d.resolve(claimstructureslist);
        },
        function(error) {
          return d.resolve({
            'error': error
          });
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.getClaimDetail = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {

        tx.executeSql("SELECT claims.* ,users.firstname,users.lastname,users.mobile as user_mobile FROM claims LEFT JOIN users on users.db_id = claims.assign_to WHERE claims.db_id = ? ORDER BY claims.created DESC ", [claim_id],
          function(tx, res) {
            if (res['rows']['length']) {
              res_claim = res['rows']['item'](0);
            } else {
              res_claim = {};
            }
            return d.resolve(res_claim);
          },
          function(tx, error) {
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.getClaimRecovery = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createrecoveryform);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM recoveryform where claim_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length']) {
            res_recovery = res['rows']['item'](0);
          } else {
            res_recovery = {};
          }
          var claim_recovery = {};
          //delete res_pfi.id;
          claim_recovery[claim_id] = res_recovery;
          return d.resolve(claim_recovery);
        },
        function(error) {
          return d.resolve({});
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.getClaimDrying = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createdrying);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM drying where claim_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length']) {
            res_drying = res['rows']['item'](0);
          } else {
            res_drying = {};
          }
          var claim_drying = {};
          //delete res_pfi.id;
          claim_drying[claim_id] = res_drying;
          return d.resolve(claim_drying);
        },
        function(error) {
          var claim_drying = {};
          //delete res_pfi.id;
          claim_drying[claim_id] = {};
          return d.resolve(claim_drying);
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.getClaimClaimReport = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createclaimreports);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM claimreports where claim_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length']) {
            res_claimreports = res['rows']['item'](0);
          } else {
            res_claimreports = {};
          }
          var claim_claimreports = {};
          //delete res_pfi.id;
          claim_claimreports[claim_id] = res_claimreports;
          return d.resolve(claim_claimreports);
        },
        function(error) {
          var claim_claimreports = {};
          claim_claimreports[claim_id] = res_claimreports;
          return d.resolve(claim_claimreports);
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.getClaimHealthSafety = function(claim_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.executeSql(DbService.Createhealthsafety);

      var d = $q.defer();
      this.db.executeSql("SELECT * FROM healthsafety where claim_id = ?", [claim_id],
        function(res) {
          if (res['rows']['length']) {
            res_healthsafety = res['rows']['item'](0);
          } else {
            res_healthsafety = {};
          }
          var claim_healthsafety = {};
          //delete res_pfi.id;
          claim_healthsafety[claim_id] = res_healthsafety;
          return d.resolve(claim_healthsafety);
        },
        function(error) {
          var claim_healthsafety = {};
          //delete res_pfi.id;
          claim_healthsafety[claim_id] = {}
          return d.resolve(claim_healthsafety);
          console.log('error in get pfi data');
        });
      return d.promise;
    }
    this.addClaimHealthSafety = function(healthSafetyModel) {
      created = Api.formatDate();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (healthSafetyModel.live_id == null) {
        healthSafetyModel.live_id = 0;
      }

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createhealthsafety);
      });
      this.db.transaction(function(tx) {

        var query = "UPDATE `healthsafety` SET `id` = (?), `user_id`= (?), `claim_id`= (?), `environments`= (?), `substances`= (?), `access_and_egress`= (?), `plant_uses` = (?), `assign_cmd` = (?), `health_safety_comments`= (?), `customer_considerations`= (?), `created`= (?) where `claim_id` = (?)";
        tx.executeSql(query, [healthSafetyModel.live_id, healthSafetyModel.user_id, healthSafetyModel.claim_id, healthSafetyModel.environments, healthSafetyModel.substances, healthSafetyModel.access_and_egress, healthSafetyModel.plant_uses, healthSafetyModel.assign_cmd, healthSafetyModel.health_safety_comments, healthSafetyModel.customer_considerations, healthSafetyModel.created, healthSafetyModel.claim_id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO `healthsafety` (`id`, `user_id`, `claim_id`, `environments`, `substances`, `access_and_egress`, `plant_uses` , `assign_cmd` , `health_safety_comments`, `customer_considerations`, `created`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [healthSafetyModel.live_id, healthSafetyModel.user_id, healthSafetyModel.claim_id, healthSafetyModel.environments, healthSafetyModel.substances, healthSafetyModel.access_and_egress, healthSafetyModel.plant_uses, healthSafetyModel.assign_cmd, healthSafetyModel.health_safety_comments, healthSafetyModel.customer_considerations, healthSafetyModel.created],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          },
          function(tx, error) {
            console.log('pfi error');
            console.log(error);
          });
      });
    }
    this.addClaimreports = function(claimreportsModel) {

      created = Api.formatDate();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (claimreportsModel.live_id == null) {
        claimreportsModel.live_id = 0;
      }

      claimreportsModel.unoccupied_date = Api.formatDateClaimReport(claimreportsModel.unoccupied_date);
      claimreportsModel.inspection_performed = Api.formatDateClaimReport(claimreportsModel.inspection_performed);
      claimreportsModel.ta_started = Api.formatDateClaimReport(claimreportsModel.ta_started);
      claimreportsModel.ta_completed = Api.formatDateClaimReport(claimreportsModel.ta_completed);
      claimreportsModel.strip_outworks_completed = Api.formatDateClaimReport(claimreportsModel.strip_outworks_completed);
      claimreportsModel.nirs_completed = Api.formatDateClaimReport(claimreportsModel.nirs_completed);
      claimreportsModel.drying_end = Api.formatDateClaimReport(claimreportsModel.drying_end);
      claimreportsModel.aa_start_date = Api.formatDateClaimReport(claimreportsModel.aa_start_date);
      claimreportsModel.aa_end_date_estimated = Api.formatDateClaimReport(claimreportsModel.aa_end_date_estimated);
      claimreportsModel.aa_end_date_actual = Api.formatDateClaimReport(claimreportsModel.aa_end_date_actual);
      claimreportsModel.works_started = Api.formatDateClaimReport(claimreportsModel.works_started);
      claimreportsModel.works_completed = Api.formatDateClaimReport(claimreportsModel.works_completed);
      claimreportsModel.estimated_works_completed = Api.formatDateClaimReport(claimreportsModel.estimated_works_completed);
      claimreportsModel.strip_outworks = Api.formatDateClaimReport(claimreportsModel.strip_outworks);
      claimreportsModel.drying_equipment_installed = Api.formatDateClaimReport(claimreportsModel.drying_equipment_installed);
      claimreportsModel.drying_equipment_removed = Api.formatDateClaimReport(claimreportsModel.drying_equipment_removed);
      claimreportsModel.reinstatement_started = Api.formatDateClaimReport(claimreportsModel.reinstatement_started);
      claimreportsModel.customer_completion = Api.formatDateClaimReport(claimreportsModel.customer_completion);

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createclaimreports,
          function(tx, res) {
            console.log(res);
          },
          function(tx, error) {
            console.log(error);
          });
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {

        var query = "UPDATE `claimreports` SET `id` =(?)," +
          "`user_id` =(?),`claim_id` =(?),`property_id` =(?),`propertyage_id` =(?),`propertycondition_id` =(?),`storey_id` =(?),`specify_storey` =(?)," +
          "`rooftype_id` =(?),`specify_rooftype` =(?),`roofmaterial_id` =(?)," +
          "`specify_roofmaterial` =(?),`walltype_id` =(?),`specify_walltype` =(?),`listed_building` =(?),`listing_type` =(?),`property_unoccupied` =(?),`unoccupied_days` =(?)," +
          "`unoccupied_months` =(?),`unoccupied_years` =(?),`unoccupied_date` =(?),`unoccupied_length` =(?),`multiple_flats` =(?),`specify_multiple_flats` =(?)," +
          "`actualperil_id` =(?),`roomsaffected_id` =(?),`specify_roomafftected` =(?),`recovery_opportunity` =(?),`crime_reference` =(?),`eow_reason` =(?),`inspection_performed` =(?)," +
          "`ta_started` =(?),`ta_completed` =(?),`strip_outworks_completed` =(?),`nirs_completed` =(?),`drying_end` =(?),`aa_type` =(?),`aa_start_date` =(?),`aa_end_date_estimated` =(?),`aa_end_date_actual` =(?)," +
          "`business_trading` =(?),`reinstatement_type` =(?),`works_started` =(?),`works_completed` =(?),`estimated_works_completed` =(?),`strip_outworks` =(?),`drying_equipment_installed` =(?)," +
          "`drying_equipment_removed` =(?),`reinstatement_started` =(?),`customer_completion` =(?),`reserves_content` =(?),`reserves_building` =(?),`reserves_fees` =(?),`reports` =(?),`requirements` =(?),`environments` =(?)," +
          "`substances` =(?),`access_and_egress` =(?),`plant_uses` =(?),`assign_cmd` =(?),`health_safety_comments` =(?),`customer_considerations` =(?),`created` =(?),`eurotempest` = (?) WHERE claim_id = (?)";
        tx.executeSql(query, [claimreportsModel.live_id, claimreportsModel.user_id, claimreportsModel.claim_id, claimreportsModel.property_id, claimreportsModel.propertyage_id, claimreportsModel.propertycondition_id, claimreportsModel.storey_id, claimreportsModel.specify_storey, claimreportsModel.rooftype_id, claimreportsModel.specify_rooftype, claimreportsModel.roofmaterial_id, claimreportsModel.specify_roofmaterial, claimreportsModel.walltype_id, claimreportsModel.specify_walltype, claimreportsModel.listed_building, claimreportsModel.listing_type, claimreportsModel.property_unoccupied, claimreportsModel.unoccupied_days, claimreportsModel.unoccupied_months, claimreportsModel.unoccupied_years, claimreportsModel.unoccupied_date, claimreportsModel.unoccupied_length, claimreportsModel.multiple_flats, claimreportsModel.specify_multiple_flats, claimreportsModel.actualperil_id, claimreportsModel.roomsaffected_id, claimreportsModel.specify_roomafftected, claimreportsModel.recovery_opportunity, claimreportsModel.crime_reference, claimreportsModel.eow_reason, claimreportsModel.inspection_performed, claimreportsModel.ta_started, claimreportsModel.ta_completed, claimreportsModel.strip_outworks_completed, claimreportsModel.nirs_completed, claimreportsModel.drying_end, claimreportsModel.aa_type, claimreportsModel.aa_start_date, claimreportsModel.aa_end_date_estimated, claimreportsModel.aa_end_date_actual, claimreportsModel.business_trading, claimreportsModel.reinstatement_type, claimreportsModel.works_started, claimreportsModel.works_completed, claimreportsModel.estimated_works_completed, claimreportsModel.strip_outworks, claimreportsModel.drying_equipment_installed, claimreportsModel.drying_equipment_removed, claimreportsModel.reinstatement_started, claimreportsModel.customer_completion, claimreportsModel.reserves_content, claimreportsModel.reserves_building, claimreportsModel.reserves_fees, claimreportsModel.reports, claimreportsModel.requirements, claimreportsModel.environments, claimreportsModel.substances, claimreportsModel.access_and_egress, claimreportsModel.plant_uses, claimreportsModel.assign_cmd, claimreportsModel.health_safety_comments, claimreportsModel.customer_considerations, claimreportsModel.created, claimreportsModel.eurotempest, claimreportsModel.claim_id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO `claimreports`" +
                "(`id`,`user_id`,`claim_id`,`property_id`,`propertyage_id`,`propertycondition_id`,`storey_id`,`specify_storey`,`rooftype_id`,`specify_rooftype`,`roofmaterial_id`,`specify_roofmaterial`,`walltype_id`,`specify_walltype`,`listed_building`,`listing_type`,`property_unoccupied`,`unoccupied_days`,`unoccupied_months`,`unoccupied_years`,`unoccupied_date`,`unoccupied_length`,`multiple_flats`," +
                "`specify_multiple_flats`,`actualperil_id`,`roomsaffected_id`,`specify_roomafftected`,`recovery_opportunity`,`crime_reference`,`eow_reason`,`inspection_performed`,`ta_started`,`ta_completed`,`strip_outworks_completed`,`nirs_completed`,`drying_end`,`aa_type`,`aa_start_date`,`aa_end_date_estimated`,`aa_end_date_actual`,`business_trading`,`reinstatement_type`,`works_started`,`works_completed`,`estimated_works_completed`,`strip_outworks`," +
                "`drying_equipment_installed`,`drying_equipment_removed`,`reinstatement_started`,`customer_completion`,`reserves_content`,`reserves_building`,`reserves_fees`,`reports`,`requirements`,`environments`,`substances`,`access_and_egress`,`plant_uses`,`assign_cmd`,`health_safety_comments`,`customer_considerations`,`created`,`eurotempest`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [claimreportsModel.live_id, claimreportsModel.user_id, claimreportsModel.claim_id, claimreportsModel.property_id, claimreportsModel.propertyage_id, claimreportsModel.propertycondition_id, claimreportsModel.storey_id, claimreportsModel.specify_storey, claimreportsModel.rooftype_id, claimreportsModel.specify_rooftype, claimreportsModel.roofmaterial_id, claimreportsModel.specify_roofmaterial, claimreportsModel.walltype_id, claimreportsModel.specify_walltype, claimreportsModel.listed_building, claimreportsModel.listing_type, claimreportsModel.property_unoccupied, claimreportsModel.unoccupied_days, claimreportsModel.unoccupied_months, claimreportsModel.unoccupied_years, claimreportsModel.unoccupied_date, claimreportsModel.unoccupied_length, claimreportsModel.multiple_flats, claimreportsModel.specify_multiple_flats, claimreportsModel.actualperil_id, claimreportsModel.roomsaffected_id, claimreportsModel.specify_roomafftected, claimreportsModel.recovery_opportunity, claimreportsModel.crime_reference, claimreportsModel.eow_reason, claimreportsModel.inspection_performed, claimreportsModel.ta_started, claimreportsModel.ta_completed, claimreportsModel.strip_outworks_completed, claimreportsModel.nirs_completed, claimreportsModel.drying_end, claimreportsModel.aa_type, claimreportsModel.aa_start_date, claimreportsModel.aa_end_date_estimated, claimreportsModel.aa_end_date_actual, claimreportsModel.business_trading, claimreportsModel.reinstatement_type, claimreportsModel.works_started, claimreportsModel.works_completed, claimreportsModel.estimated_works_completed, claimreportsModel.strip_outworks, claimreportsModel.drying_equipment_installed, claimreportsModel.drying_equipment_removed, claimreportsModel.reinstatement_started, claimreportsModel.customer_completion, claimreportsModel.reserves_content, claimreportsModel.reserves_building, claimreportsModel.reserves_fees, claimreportsModel.reports, claimreportsModel.requirements, claimreportsModel.environments, claimreportsModel.substances, claimreportsModel.access_and_egress, claimreportsModel.plant_uses, claimreportsModel.assign_cmd, claimreportsModel.health_safety_comments, claimreportsModel.customer_considerations, claimreportsModel.created, claimreportsModel.eurotempest],
                function(tx, res) {
                  console.log(res);
                  return d.resolve('done');
                },
                function(tx, error) {
                  console.log('INSERT claimreports error -----------: ');
                  console.log(error.message);
                  return d.resolve('error done');
                });
            } else {
              return d.resolve('done');
            }
          },
          function(tx, error) {
            console.log('claimreports error');
            console.log(error);
            return d.resolve('error done');
          });
      });
      return d.promise;
    }
    this.addClaimPFI = function(pfiModel) {
      created = Api.formatDate();
      if (pfiModel.live_id == null) {
        pfiModel.live_id = 0;
      }
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createpfiform);
      });
      this.db.transaction(function(tx) {

        var query = "UPDATE pfiform SET `id`=(?),`user_id`=(?)	,`claim_id`=(?)	,`fraud_type`=(?)	,`referral_reason`=(?)	,`concerns_reason`=(?)	,`customer_challenged`=(?)	,`response_referral`=(?)	,`followup1`=(?)	,`followup2`=(?)	,`aditional_followup`=(?),`final_action`=(?),`created`=(?) where `claim_id` = (?)";
        tx.executeSql(query, [pfiModel.live_id, pfiModel.user_id, pfiModel.claim_id, pfiModel.fraud_type, pfiModel.referral_reason, pfiModel.concerns_reason, pfiModel.customer_challenged, pfiModel.response_referral, pfiModel.followup1, pfiModel.followup2, pfiModel.aditional_followup, pfiModel.final_action, pfiModel.created, pfiModel.claim_id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO pfiform (`id` ,`user_id` ,`claim_id`,`fraud_type`,`referral_reason`,`concerns_reason`,`customer_challenged`,`response_referral`,`followup1`,`followup2`,`aditional_followup`,`final_action`,`created`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [pfiModel.live_id, pfiModel.user_id, pfiModel.claim_id, pfiModel.fraud_type, pfiModel.referral_reason, pfiModel.concerns_reason, pfiModel.customer_challenged, pfiModel.response_referral, pfiModel.followup1, pfiModel.followup2, pfiModel.aditional_followup, pfiModel.final_action, pfiModel.created],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          },
          function(tx, error) {
            console.log('pfi error');
            console.log(error);
          });
      });
    }
    this.addClaimDrying = function(DryingModel) {
      created = Api.formatDate();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (DryingModel.live_id == null) {
        DryingModel.live_id = 0;
      }
      DryingModel.drying_date = Api.formatDate(DryingModel.drying_date);
      DryingModel.drying_date = DryingModel.drying_date.split(" ")[0] + ' ' + DryingModel.drying_date.split(" ")[1] + ' ' + DryingModel.drying_date.split(" ")[2];
      DryingModel.incident_date = Api.formatDate(DryingModel.incident_date);
      DryingModel.incident_date = DryingModel.incident_date.split(" ")[0] + ' ' + DryingModel.incident_date.split(" ")[1] + ' ' + DryingModel.incident_date.split(" ")[2];
      this.db.transaction(function(tx) {
        tx.executeSql();
      });
      this.db.transaction(function(tx) {

        var query = "UPDATE drying SET `id`=(?),`user_id`=(?)	,`claim_id`=(?)	,`property_address`=(?)	,`customer_name`=(?)	,`firstcall_reference`=(?)	,`drying_date`=(?),`incident_date`=(?)	,`firstcall_name`=(?)	,`firstcall_position`=(?)	,`firstcall_signature`=(?),`created`=(?) where `claim_id` = (?)";
        tx.executeSql(query, [DryingModel.live_id, DryingModel.user_id, DryingModel.claim_id, DryingModel.property_address, DryingModel.customer_name, DryingModel.firstcall_reference, DryingModel.drying_date, DryingModel.incident_date, DryingModel.firstcall_name, DryingModel.firstcall_position, DryingModel.firstcall_signature, DryingModel.created, DryingModel.claim_id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO drying (`id`,`user_id`,`claim_id`,`property_address`,`customer_name`,`firstcall_reference`,`drying_date`,`incident_date`,`firstcall_name`,`firstcall_position`,`firstcall_signature`,`created`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [DryingModel.live_id, DryingModel.user_id, DryingModel.claim_id, DryingModel.property_address, DryingModel.customer_name, DryingModel.firstcall_reference, DryingModel.drying_date, DryingModel.incident_date, DryingModel.firstcall_name, DryingModel.firstcall_position, DryingModel.firstcall_signature, DryingModel.created],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          },
          function(tx, error) {
            console.log('pfi error');
            console.log(error);
          });
      });
    }
    this.addClaimRecovery = function(recoveryModel) {
      created = Api.formatDate();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (recoveryModel.live_id == null) {
        recoveryModel.live_id = 0;
      }
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createrecoveryform);
      });
      this.db.transaction(function(tx) {

        if (recoveryModel.live_id == null) {
          recoveryModel.live_id = 0;
        }
        var query = "UPDATE recoveryform SET `id`=(?),`user_id`=(?)	,`claim_id`=(?)	,`allegation_negligence`=(?)	,`third_party` = (?),`response_referral`=(?)	,`followup1`=(?)	,`followup2`=(?)	,`aditional_followup`=(?),`final_action`=(?),`created`=(?) where `claim_id` = (?)";
        tx.executeSql(query, [recoveryModel.live_id, recoveryModel.user_id, recoveryModel.claim_id, recoveryModel.allegation_negligence, recoveryModel.third_party, recoveryModel.response_referral, recoveryModel.followup1, recoveryModel.followup2, recoveryModel.aditional_followup, recoveryModel.final_action, recoveryModel.created, recoveryModel.claim_id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO recoveryform (`id`,`user_id`	,`claim_id`	,`allegation_negligence`	,`third_party` ,`response_referral`,`followup1`	,`followup2`,`aditional_followup`,`final_action`,`created`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [recoveryModel.live_id, recoveryModel.user_id, recoveryModel.claim_id, recoveryModel.allegation_negligence, recoveryModel.third_party, recoveryModel.response_referral, recoveryModel.followup1, recoveryModel.followup2, recoveryModel.aditional_followup, recoveryModel.final_action, recoveryModel.created],
                function(tx, res) {
                  console.log(res);
                },
                function(tx, error) {
                  console.log('INSERT error -----------: ' + error.message);
                });
            }
          },
          function(tx, error) {
            console.log('pfi error');
            console.log(error);
          });
      });
    }
    this.addClaimInternalnote = function(id, claim_id, internal_notes, created, user_name, user_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      created = Api.formatDateDatabase(created);
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.CreateClaimInternalnote);
      });
      this.db.transaction(function(tx) {
        // File download function with URL and local path
        console.log([id, claim_id, internal_notes, created]);
        var query = "UPDATE claim_internalnote SET `id`=(?),`claim_id`=(?),`internal_notes`=(?),`created`=(?),`sync` = '1',`user_name`=(?),`user_id` = (?) where `id` = (?)";
        tx.executeSql(query, [id, claim_id, internal_notes, created, user_name, user_id, id], function(tx, res) {
          if (res.rowsAffected == 0) {
            var query = "INSERT INTO claim_internalnote (`id`,`claim_id`,`internal_notes`,`created`,`sync`,`user_name`,`user_id`) VALUES (?,?,?,?,?,?,?)";
            tx.executeSql(query, [id, claim_id, internal_notes, created, '1', user_name, user_id],
              function(tx, res) {
                console.log(res);
              },
              function(tx, error) {
                console.log('INSERT error -----------: ' + error.message);
              });
          }
        });
      });
    }
    this.deleteClaim = function(dashboard_ids, is_dashboard) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (is_dashboard == 1) {
        condition = 'claims.is_dashboard = 1 ';
      } else {
        condition = 'claims.is_dashboard is null ';
      }
      var d = $q.defer();
      this.db.transaction(function(tx) {
        //if (dashboard_ids != '') {
        //ids = '';
        var DELETE_CLAIMS = [
          //"UPDATE claims SET listing = '0' WHERE is_dashboard = '1'",
          "DELETE FROM claims WHERE db_id NOT IN (" + dashboard_ids + ") AND " + condition + "",
          "DELETE FROM structuremedia WHERE structuremedia.claimstructure_id IN (SELECT claimstructures.id FROM claimstructures left join claims on claims.db_id = claimstructures.claim_id WHERE claimstructures.claim_id not in (" + dashboard_ids + ") AND " + condition + ");",
          "DELETE FROM sowtask WHERE sowtask.claimstructure_id in (SELECT claimstructures.id FROM claimstructures left join claims on claims.db_id = claimstructures.claim_id WHERE claimstructures.claim_id not in (" + dashboard_ids + ") AND " + condition + ");",
          "DELETE FROM structurenotes WHERE structurenotes.claimstructure_id in (SELECT claimstructures.id FROM claimstructures left join claims on claims.db_id = claimstructures.claim_id WHERE claimstructures.claim_id not in (" + dashboard_ids + ") AND " + condition + ");",
          "DELETE FROM claimstructures WHERE id in (SELECT claimstructures.id FROM claimstructures left join claims on claims.db_id = claimstructures.claim_id WHERE claimstructures.claim_id not in (" + dashboard_ids + ") AND " + condition + ");",
        ];
        /*console.log(DELETE_CLAIMS);

        var DELETE_CLAIMS = [
          "DELETE FROM claims WHERE db_id NOT IN (" + dashboard_ids + ") AND " + condition + ""
        ]*/
        DbService.db.sqlBatch(DELETE_CLAIMS, function(res) {
          console.log(res);
          return d.resolve('done');
        }, function(error) {
          console.log(error);
          return d.resolve('error');
        });
      });
      return promise = d.promise;
    }
    this.removeDashbordClaimsAsClaimList = function() {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();

      this.db.executeSql("UPDATE claims SET listing = '0' WHERE is_dashboard = ? ", [1], function(res) {
        return d.resolve('done');
      }, function(error) {
        return d.resolve('error');
      });
      return promise = d.promise;
    }
    this.deleteSowTask = function(SowTask_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        if (SowTask_id != '') {
          tx.executeSql("DELETE FROM sowtask WHERE sowtask.id = ? ", [SowTask_id],
            function(res) {
              console.log('delete sowtask');
              console.log(res);
              return d.resolve('delete done');
            },
            function(error) {
              console.log('error delete sowtask');
              console.log(error);
              return d.resolve('error done');
            });
        }
      });
      return promise = d.promise;
    }
    this.deleteStarusSowTask = function(SowTask_id, deleted, delete_by) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      deleted = Api.formatDateDatabase(deleted);
      var d = $q.defer();
      this.db.transaction(function(tx) {
        if (SowTask_id != '') {
          tx.executeSql("UPDATE sowtask SET deleted = (?) ,deleted_by = (?),status = (?) WHERE sowtask.id = ? ", [deleted, delete_by, 2, SowTask_id],
            function(tx, res) {
              console.log('delete sowtask');
              console.log(res);
              return d.resolve('delete done');
            },
            function(tx, error) {
              console.log('error delete sowtask');
              console.log(error);
              return d.resolve('error done');
            });
        }
      });
      return d.promise;
    }
    this.addClient = function(db_id, client_name, contact_person_name, client_address, client_city, client_state, client_country, client_zipcode, client_phone_no, client_fax_no, client_email, client_warranty, client_show_info, client_image, client_full_path_image, claim_id) {
      fiels = client_full_path_image.split('/');
      file_name = fiels[fiels.length - 1];
      fullPath = "/";
      folder = 'claims/' + claim_id + "/client_image/";
      folders = folder.split('/');
      fullPath = '/';
      i = 0;

      while (i < folders.length) {
        fullPath = fullPath + '/' + folders[i];
        $cordovaFile.createDir(cordova.file.dataDirectory, fullPath, false);
        i = i + 1;
      }

      fp = file_name;
      console.log(cordova.file.dataDirectory);
      var fileTransfer = new FileTransfer();
      fileTransfer.download(client_full_path_image, cordova.file.dataDirectory + folder + fp,
        function(entry) {
          /*this.db = window.sqlitePlugin.openDatabase({
            name: 'survey.db',
            location: 'default'
          });*/
          this.db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS `clients` (  `id` INTEGER PRIMARY KEY AUTOINCREMENT,db_id INTEGER,`client_name` TEXT, `contact_person_name` TEXT,  `client_address` TEXT,  `client_city` TEXT,  `client_state` TEXT,  `client_country` TEXT,  `client_zipcode` TEXT,  `client_phone_no` TEXT,  `client_fax_no` TEXT,  `client_email` TEXT,  `client_warranty` TEXT,  `client_show_info` TEXT,  `client_image` TEXT);");
          });
          this.db.transaction(function(tx) {
            var date = new Date();
            var offset = date.getTimezoneOffset();
            var device_time = $filter('date')(date, 'yyyy-MM-dd HH:mm:ss');

            var query = "UPDATE clients set `db_id` = (?),`client_name`= (?),`contact_person_name`= (?),`client_address`= (?),`client_city`= (?),`client_state`= (?),`client_country`= (?),`client_zipcode`= (?),`client_phone_no`= (?),`client_fax_no`= (?),`client_email`= (?),`client_warranty`= (?),`client_show_info`= (?),`client_image`= (?) where db_id = ? ";
            tx.executeSql(query, [db_id, client_name, contact_person_name, client_address, client_city, client_state, client_country, client_zipcode, client_phone_no, client_fax_no, client_email, client_warranty, client_show_info, client_image, db_id],
              function(tx, res) {
                if (res.rowsAffected == 0) {
                  var query = "INSERT INTO clients (`db_id`,`client_name`,`contact_person_name`,`client_address`,`client_city`,`client_state`,`client_country`,`client_zipcode`,`client_phone_no`,`client_fax_no`,`client_email`,`client_warranty`,`client_show_info`,`client_image`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                  tx.executeSql(query, [db_id, client_name, contact_person_name, client_address, client_city, client_state, client_country, client_zipcode, client_phone_no, client_fax_no, client_email, client_warranty, client_show_info, client_image], function(tx, res) {

                    },
                    function(tx, error) {
                      console.log('INSERT clients error -----------: ' + error.message);
                    });
                }
              });
          });
        },
        function(error) {
          console.log(error)
        });
    }
    this.addClientsMaster = function(customerModel) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS `clients_master` (id INTEGER,`name` TEXT);");
      });
      this.db.transaction(function(tx) {
        var query = "UPDATE clients_master set `name` = (?) where id = ? ";
        tx.executeSql(query, [customerModel.name, customerModel.id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO clients_master (`id`,`name`) VALUES (?,?)";
              tx.executeSql(query, [customerModel.id, customerModel.name], function(tx, res) {},
                function(tx, error) {
                  console.log('INSERT clients master error -----------: ' + error.message);
                });
            }
          });
      });
    }
    this.addStructure = function(structuresModel) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE `structures`(`id`	INTEGER UNIQUE,claim_id TEXT,parent_id TEXT,name TEXT,position TEXT,status TEXT,created_by TEXT,modified_by TEXT,created TEXT,modified TEXT,deleted TEXT,deleted_by TEXT);");
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var query = "UPDATE structures set `id` = (?),claim_id = (?),parent_id = (?),name = (?),position = (?) where id = ? ";
        tx.executeSql(query, [structuresModel.id, structuresModel.claim_id, structuresModel.parent_id, structuresModel.name, structuresModel.position, structuresModel.id],
          function(tx, res) {
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO structures (`id`,claim_id,parent_id,name,position) VALUES (?,?,?,?,?)";
              tx.executeSql(query, [structuresModel.id, structuresModel.claim_id, structuresModel.parent_id, structuresModel.name, structuresModel.position],
                function(tx, res) {
                  return d.resolve('done');
                },
                function(tx, error) {
                  return d.resolve('error');
                  console.log('INSERT clients master error -----------: ' + error.message);
                });
            }
          });
      });
      return d.promise;
    }
    this.getClientList = function() {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (typeof this.db == 'undefined') {
        this.db = window.sqlitePlugin.openDatabase({
          name: 'survey.db',
          location: 'default'
        });
      }
      this.db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS `clients_master` (id INTEGER,`name` TEXT);");
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM clients_master WHERE 1 = ?", [1],
          function(tx, res) {
            var claimstructures = [];
            if (res['rows']['length']) {
              for (i = 0; i < res['rows']['length']; i++) {
                claimstructures.push(res['rows']['item'](i));
              }
            }
            return d.resolve(claimstructures);
          },
          function(tx, error) {
            return d.resolve({});
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.deleteClient = function(dashboard_ids) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("delete from claims where is_dashboard = 1 and db_id not in (?)", dashboard_ids);
      });
    }
    this.syncStatusClaimsData = function(claim_id, sync) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("UPDATE claims SET sync = (?) where db_id = ?", [sync, claim_id],
          function(tx, res) {
            console.log(res);
            console.log('claim status updated');
          },
          function(tx, error) {
            console.log(error);
            console.log('error in claim status update');
          });
      });
    }
    this.ClaimsStatusUpdate = function(claim_id, status) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        tx.executeSql("UPDATE claims SET status = (?) where db_id = ?", [status, claim_id],
          function(tx, res) {
            return d.resolve('sync is done');
            console.log('claim status updated');
          },
          function(tx, error) {
            return d.resolve('sync is done');
            console.log('error in claim status update');
          });
      });
      return promise = d.promise;
    }
    this.syncClaimsData = function() {
      var claimIdPromise = DbService.syncClaimId();
      var d = $q.defer();
      claimIdPromise.then(function(claim_ids) {
        var claimList = {};
        for (var i = 0; i < claim_ids.length; i++) {
          claim_note_found = false;
          var promiseInternalNotes = DbService.syncGetInternalNotes(claim_ids[i]);
          var promiseClaimPFI = DbService.getClaimPFI(claim_ids[i]);
          var promiseClaimRecovery = DbService.getClaimRecovery(claim_ids[i]);
          var promiseClaimDrying = DbService.getClaimDrying(claim_ids[i]);
          var promiseClaimHealthSafety = DbService.getClaimHealthSafety(claim_ids[i]);
          var promiseClaimReport = DbService.getClaimClaimReport(claim_ids[i]);
          var promiseClaimStructures = DbService.getClaimStructuresForSync(claim_ids[i]);
          var promiseClaimFnol = DbService.getClaimFnol(claim_ids[i]);
          claimList[claim_ids[i]] = {
            internalNote: null,
            pfiform: null,
            recoveryform: null,
            drying: null,
            healthsafety: null,
            claimreport: null,
            claimstructures: null,
            fnol: null
          };
          //claimList[k] = );
          promiseInternalNotes.then(function(result) {
            $.each(result, function(Notesk, Notesv) {
              claimList[Notesk]['internalNote'] = Notesv;
              DbService.syncToLive(claimList[Notesk], Notesk);
            });
          })
          promiseClaimPFI.then(function(result) {
            $.each(result, function(PFIk, PFIv) {
              claimList[PFIk]['pfiform'] = PFIv;
              DbService.syncToLive(claimList[PFIk], PFIk);
            });
          })
          promiseClaimRecovery.then(function(result) {
            $.each(result, function(recoveryk, recoveryv) {
              claimList[recoveryk]['recoveryform'] = recoveryv;
              DbService.syncToLive(claimList[recoveryk], recoveryk);
            });
          })
          promiseClaimDrying.then(function(result) {
            $.each(result, function(dryingk, dryingv) {
              claimList[dryingk]['drying'] = dryingv;
              DbService.syncToLive(claimList[dryingk], dryingk);
            });
          })
          promiseClaimHealthSafety.then(function(result) {
            $.each(result, function(healthsafetyk, healthsafetyv) {
              claimList[healthsafetyk]['healthsafety'] = healthsafetyv;
              DbService.syncToLive(claimList[healthsafetyk], healthsafetyk);
            });
          })
          promiseClaimReport.then(function(result) {
            $.each(result, function(claimreportk, claimreportv) {
              claimList[claimreportk]['claimreport'] = claimreportv;
              DbService.syncToLive(claimList[claimreportk], claimreportk);
            });
          })
          promiseClaimStructures.then(function(result) {
            $.each(result, function(ClaimStructuresk, ClaimStructuresv) {
              claimList[ClaimStructuresk]['claimstructures'] = ClaimStructuresv;
              DbService.syncToLive(claimList[ClaimStructuresk], ClaimStructuresk);
            });
          })
          promiseClaimFnol.then(function(result) {
            $.each(result, function(fnolk, fnolv) {
              claimList[fnolk]['fnol'] = fnolv;
              DbService.syncToLive(claimList[fnolk], fnolk);
            });
          })
        }
        return d.resolve('sync is done');
      });
      return promise = d.promise;
    }

    this.check_data_is_syncable = function(claimList) {
      var return_val = true;
      $.each(claimList, function(k, v) {
        if (v == null) {
          return_val = false;
          return return_val;
        }
      });
      return return_val;
    }

    this.syncToLive = function(claimList, claim_id) {
      if (DbService.check_data_is_syncable(claimList) == true) {
        //console.log(claimList);
        //$.each(claimList, function(k, v) {
        syncClaimList = {
          'claim_id': claim_id,
          'claimnotes': claimList['internalNote'],
          'pfiform': claimList['pfiform'],
          'recoveryform': claimList['recoveryform'],
          'drying': claimList['drying'],
          'healthsafety': claimList['healthsafety'],
          'claimreport': claimList['claimreport'],
          'claimstructures': claimList['claimstructures'],
          'fnol': claimList['fnol']
        };
        //});
        var pdata = jQuery.param({
          'user_id': localStorage.getItem("UserId"),
          'user_token': localStorage.getItem("UserToken"),
          'user_type': localStorage.getItem("UserType"),
          'synchronization_data': JSON.stringify(syncClaimList),
          'device_token': ''
        });
        var urlBase = Api.urlBase + "sync";
        req = {
          headers: Api.HEADERS
        };
        $http.post(urlBase, pdata, req).then(
          function(response) {
            console.log(response);
            //response['response']['notes_request_ids'];
            claim_id = response['data']['response']['id'];
            DbService.updateWithSyncStatus(response['data']['response']);
            $.each(response['data']['response']['claimstructures'], function(k, v) {
              var structure_id = v['db_id'];
              var new_structure_id = v['id'];
              DbService.updateSyncStructuresIds(new_structure_id, structure_id);
              //update SOW id's
              $.each(v['structuremedia'], function(structuremedia_k, structuremedia_v) {
                var structuremedia_db_id = structuremedia_v['db_id'];
                var structuremedia_id = structuremedia_v['id'];
                DbService.updateSyncStructureMediaIds(structuremedia_db_id, structuremedia_id);
              });
              $.each(v['sowtask'], function(sowtask_k, sowtask_v) {
                var sow_id = sowtask_v['id'];
                var sow_db_id = sowtask_v['db_id'];
                DbService.updateSyncSowIds(sow_db_id, sow_id);
              });
              $.each(v['structurenotes'], function(structurenotes_k, structurenotes_v) {
                var Structurenotes_db_id = structurenotes_v['db_id'];
                var Structurenotes_id = structurenotes_v['id'];
                DbService.updateSyncStructurenotesIds(Structurenotes_db_id, Structurenotes_id);
              });
            });
            /*$.each(response['data']['response']['claimnotes'], function(notesk, notesv) {
              user_id = notesv['user']['id'];
              user_name = notesv['user']['firstname'] + notesv['user']['lastname'];

              DbService.addClaimInternalnote(notesv['id'], notesv['claim_id'], notesv['internal_notes'], notesv['created'], user_name, user_id);
            });*/
            setTimeout(function() {


              if (typeof localStorage.getItem("UserId") != 'undefined') {
                DbService.getClaimMediaSync(claim_id).then(function(ClaimMediaList) {
                  if (ClaimMediaList.length > 0) {
                    $.each(ClaimMediaList, function(key, value_media) {
                      var filename = value_media.media_file;
                      //var urlBase = Api.urlBase + "claimstructures/uploadmedia";
                      var urlBase = Api.urlBase + "sync/uploadmedia";
                      var options = {
                        fileKey: "mediafiles[]",
                        fileName: value_media.media_file,
                        chunkedMode: false,
                        mimeType: "multipart/form-data",
                        params: {
                          'id': value_media.media_db_id,
                          'user_id': localStorage.getItem("UserId"),
                          'user_token': localStorage.getItem("UserToken"),
                          'filetype': value_media.filetype,
                          'chunkedMode': false,
                          'claimstructure_id': value_media.claimstructure_id,
                          'db_id': value_media.media_db_id
                        }
                      };

                      $cordovaFileTransfer.upload(urlBase, cordova.file.dataDirectory + 'claims/' + claim_id + '/' + value_media.claimstructures_folder_id + '/' + filename, options).then(
                        function(result) {
                          console.log(JSON.parse(result['response']));
                          response = JSON.parse(result['response'])
                          var new_media_id = response['response'][0]['id'];
                          var media_id = response['response'][0]['db_id'];
                          //DbService.updateSyncStructureMediaIds(media_id, new_media_id);
                          DbService.updateSyncStructureMediaStatus(new_media_id);
                        },
                        function(error) {
                          console.log(error);
                        })

                      if (key == ClaimMediaList.length - 1) {
                        DbService.syncStatusClaimsData(claim_id, '1');
                      }
                    });
                  } else {
                    DbService.syncStatusClaimsData(claim_id, '1');
                  }
                });
              }
              DbService.DeleteStructurnotesANDSOWByStatusDelete(claim_id);
            }, 3000);
          },
          function(error) {
            console.log('syncing issue');
          }
        )
      }
    }

    this.updateSyncStructuresIds = function(new_structure_id, structure_id) { //update live structure_id
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        console.log('DB_IDS' + structure_id.substr(0, 2));
        if (structure_id.substr(0, 2) == '00') {
          var claimstructuresDelete = [
            "UPDATE claimstructures SET id = '" + new_structure_id + "' WHERE id = '" + structure_id + "'",
            "UPDATE structuremedia SET claimstructure_id = '" + new_structure_id + "' WHERE claimstructure_id = '" + structure_id + "'",
            "UPDATE sowtask SET claimstructure_id = '" + new_structure_id + "' WHERE claimstructure_id = '" + structure_id + "'",
            "UPDATE structurenotes SET claimstructure_id = '" + new_structure_id + "' WHERE claimstructure_id = '" + structure_id + "'"
          ];
          console.log(claimstructuresDelete);
          DbService.db.sqlBatch(claimstructuresDelete,
            function(res) {
              console.log(res);
              return d.resolve('done');
            },
            function(error) {
              console.log(error);
              return d.resolve('error');
            });
        } else {
          return d.resolve('done');
        }
      });
      return d.promise;
    }
    this.updateSyncStructureMediaStatus = function(structuremedia_id) { //update live structure_id
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var claimstructuresmediaSyncStatus = [
          "UPDATE structuremedia SET sync = '1' WHERE id = '" + structuremedia_id + "'",
        ];
        console.log(claimstructuresmediaSyncStatus);
        DbService.db.sqlBatch(claimstructuresmediaSyncStatus,
          function(res) {
            console.log(res);
            return d.resolve('done');
          },
          function(error) {
            console.log(error);
            return d.resolve('error');
          });
      });
      return d.promise;
    }

    this.updateSyncSowIds = function(sow_id, new_sow_id) { //update live structure_id
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        console.log('DB_IDS' + sow_id.substr(0, 2));

        var sowtaskUpdate = [
          "UPDATE sowtask SET id = '" + new_sow_id + "' WHERE id = '" + sow_id + "'"
        ];
        console.log(sowtaskUpdate);
        DbService.db.sqlBatch(sowtaskUpdate,
          function(res) {
            console.log(res);
            return d.resolve('done');
          },
          function(error) {
            console.log(error);
            return d.resolve('error');
          });
      });
      return d.promise;
    }
    this.updateSyncStructurenotesIds = function(structurenotes_id, new_structurenotes_id) { //update live structure_id
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var structurenotesUpdate = [
          "UPDATE structurenotes SET id = '" + new_structurenotes_id + "' WHERE id = '" + structurenotes_id + "'"
        ];
        console.log(structurenotesUpdate);
        DbService.db.sqlBatch(structurenotesUpdate,
          function(res) {
            console.log(res);
            return d.resolve('done');
          },
          function(error) {
            console.log(error);
            return d.resolve('error');
          });
      });
      return d.promise;
    }
    this.updateSyncStructureMediaIds = function(structurenotes_id, new_structurenotes_id) { //update live structure_id
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var structurenotesUpdate = [
          "UPDATE structuremedia SET id = '" + new_structurenotes_id + "' WHERE id = '" + structurenotes_id + "'"
        ];
        console.log(structurenotesUpdate);
        DbService.db.sqlBatch(structurenotesUpdate,
          function(res) {
            console.log(res);
            return d.resolve('done');
          },
          function(error) {
            console.log(error);
            return d.resolve('error');
          });
      });
      return d.promise;
    }
    this.DeleteStructurnotesANDSOWByStatusDelete = function(claim_id) {

      this.db.transaction(function(tx) {
        query = "DELETE FROM structurenotes " +
          "WHERE  structurenotes.status = 2 AND structurenotes.claimstructure_id IN (SELECT id FROM claimstructures WHERE claim_id = ? );";
        console.log(query);
        tx.executeSql(query, [claim_id],
          function(tx, response) {
            console.log(response)
          },
          function(tx, error) {
            console.log(error);
          });
      });
      this.db.transaction(function(tx) {
        query = "DELETE FROM sowtask " +
          "WHERE  sowtask.status = 2 AND sowtask.claimstructure_id IN (SELECT id FROM claimstructures WHERE claim_id = ? );";
        console.log(query);
        tx.executeSql(query, [claim_id],
          function(tx, response) {
            console.log(response)
          },
          function(tx, error) {
            console.log(error);
          });
      });
    }
    this.updateWithSyncStatus = function(response) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default',
        androidDatabaseImplementation: 2,
        androidLockWorkaround: 1
      });*/
      var note_calim_id = response['id'];
      //notes_request_ids = response['notes_request_ids'];
      var claimnotes = response['claimnotes'];
      this.db.transaction(function(tx) {
        query = 'DELETE FROM claim_internalnote where claim_id = (?)';
        tx.executeSql(query, [note_calim_id],
          function(tx, response) {
            $.each(claimnotes, function(k, v) {
              var id = v['id'],
                claim_id = v['claim_id'],
                internal_notes = v['internal_notes'],
                created = v['created'],
                sync = 1,
                user_name = v['user']['firstname'] + ' ' + v['user']['lastname'],
                user_id = v['user']['id'];
              DbService.addClaimInternalnoteOffline(id, claim_id, internal_notes, created, sync, user_name, user_id);
            });
          },
          function(tx, error) {
            console.log(error);
          });
      });
    }
    this.syncClaimId = function() {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      //this.db = window.openDatabase('survey.db', '', 'Bar Tab Orders', 2500000);

      var sync_val = '0';
      var counter_diff = Api.formatDateDatabase();
      var d = $q.defer();
      this.db.executeSql("SELECT db_id FROM claims where sync = ? AND '" + counter_diff + "'='" + counter_diff + "'", [sync_val],
        function(res) {
          console.log(res);
          var syncClaimList = {};
          var i = 0;
          var call_counter = 0;
          var temp = [];
          var claim_ids = [];
          if (res['rows']['length'] > 0) {
            while (i < res['rows']['length']) {
              claim_ids[i] = res['rows']['item'](i).db_id;
              i = i + 1;
            }
          }
          return d.resolve(claim_ids);
        },
        function(error) {
          return d.resolve({});
        });
      return d.promise;
    }

    this.getStructures = function(parent_id, claim_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("SELECT structures.*,count(structures_child.id) as has_child " +
          "FROM structures " +
          "left join structures as structures_child on structures.id = structures_child.parent_id " +
          "WHERE structures.parent_id = ? and structures.claim_id = ? or (structures.parent_id = ? and structures.claim_id = '') GROUP BY structures.id", [parent_id, claim_id, parent_id],
          function(tx, structures_res) {
            console.log('claim_id :' + parent_id);
            j = 0;
            var structures = [];
            var structuresdata = {};

            while (j < structures_res['rows']['length']) {
              structures.push(structures_res['rows']['item'](j));
              j = j + 1;
            }
            structuresdata['structures'] = structures;
            if (parent_id == 0) {
              structuresdata['back_button_val'] = null;
              structuresdata['back_button_value'] = 'Structure';
              return d.resolve(structuresdata);
            } else {
              DbService.db.transaction(function(tx) {
                tx.executeSql("SELECT parent_id,name FROM structures WHERE id = ?", [parent_id],
                  function(tx, structures_para_res) {
                    if (0 < structures_para_res['rows']['length']) {
                      structuresdata['back_button_val'] = structures_para_res['rows']['item'](0)['parent_id'];
                      structuresdata['back_button_value'] = structures_para_res['rows']['item'](0)['name'];
                    }
                    return d.resolve(structuresdata);
                  },
                  function(tx, error) {
                    structuresdata['back_button_val'] = '';
                    return d.resolve(structuresdata);
                  });
              }, function(tx, error) {
                return d.resolve([]);
              });
            }
          });
      });
      return d.promise;
    }

    this.getSOWMaster = function(parent_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var or_parent_id = '';
      if (parent_id != 0) {
        or_parent_id = parent_id;
      }

      this.db.transaction(function(tx) {
        tx.executeSql("SELECT trades.* ,'false' as is_task FROM trades WHERE parent_id = ? or parent_id = ?", [parent_id, or_parent_id],
          function(tx, sowtrades_res) {
            j = 0;
            var sowtrades = [];
            var sowtradesdata = {};
            if (sowtrades_res['rows']['length'] > 0) {
              while (j < sowtrades_res['rows']['length']) {
                sowtrades.push(sowtrades_res['rows']['item'](j));
                j = j + 1;
              }
              sowtradesdata['sowtrades'] = sowtrades;
              if (parent_id == 0) {
                sowtradesdata['back_button_val'] = null;
                return d.resolve(sowtradesdata);
              } else {
                DbService.db.transaction(function(tx) {
                  tx.executeSql("SELECT parent_id,name FROM trades WHERE id = ?", [parent_id],
                    function(tx, sowtrades_para_res) {
                      if (0 < sowtrades_para_res['rows']['length']) {
                        sowtradesdata['back_button_val'] = sowtrades_para_res['rows']['item'](0)['parent_id'];
                      }
                      return d.resolve(sowtradesdata);
                    },
                    function(tx, error) {
                      sowtradesdata['back_button_val'] = '';
                      return d.resolve(sowtradesdata);
                    });
                }, function(tx, error) {
                  return d.resolve([]);
                });
              }
            } else {
              DbService.db.transaction(function(tx) {
                tx.executeSql("SELECT tasks.*,task_name as name,'true' as is_task FROM tasks WHERE trade_id = ?", [parent_id],
                  function(tx, sowtask) {
                    j = 0;
                    while (j < sowtask['rows']['length']) {
                      sowtrades.push(sowtask['rows']['item'](j));
                      j = j + 1;
                    }
                    sowtradesdata['sowtrades'] = sowtrades;
                    DbService.db.transaction(function(tx) {
                      tx.executeSql("SELECT parent_id,name FROM trades WHERE id = ?", [parent_id],
                        function(tx, sowtrades_para_res) {
                          if (0 < sowtrades_para_res['rows']['length']) {
                            sowtradesdata['back_button_val'] = sowtrades_para_res['rows']['item'](0)['parent_id'];
                          }
                          return d.resolve(sowtradesdata);
                        },
                        function(tx, error) {
                          sowtradesdata['back_button_val'] = '';
                          return d.resolve(sowtradesdata);
                        });
                    }, function(tx, error) {
                      return d.resolve([]);
                    });
                  },
                  function(tx, error) {

                  });
              });
            }
          });
      });
      return d.promise;
    }

    this.syncGetInternalNotes = function(claim_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.executeSql("SELECT * FROM claim_internalnote WHERE claim_id = ? and (id = '' or id is null)", [claim_id],
        function(internalnote) {
          var response = [];
          console.log('claim_id :' + claim_id);
          var j = 0;
          var claimnotes = [];
          var notes = '';
          while (j < internalnote['rows']['length']) {
            claimnotes.push(internalnote['rows']['item'](j));
            j = j + 1;
          }
          var noteResult = {};
          noteResult[claim_id] = claimnotes;
          return d.resolve(noteResult);
        },
        function(error) {
          var noteResult = {};
          noteResult[claim_id] = claimnotes;
          return d.resolve(noteResult);
        });
      return promise = d.promise;
    }

    this.getReportStatus = function(claim_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.sqlBatch([
        DbService.Createpfiform,
        DbService.Createdrying,
        DbService.Createclaimreports,
        DbService.Createhealthsafety,
        DbService.claimstructures,
        DbService.Createrecoveryform
      ], function(res) {
        console.log(res);
      }, function(error) {
        console.log(error);
      });
      this.db.transaction(function(tx) {
        var claimreportsSelect = "SELECT claimreports.claim_id as claimreports_id,recoveryform.claim_id as recoveryform_id,pfiform.claim_id as pfiform_id,drying.claim_id as drying_id,healthsafety.claim_id as healthsafety_id,claimstructures.id as structures_id" +
          " FROM claims" +
          " left join claimreports on claimreports.claim_id = claims.db_id " +
          " left join recoveryform on recoveryform.claim_id = claims.db_id " +
          " left join pfiform on pfiform.claim_id = claims.db_id  " +
          " left join drying on drying.claim_id = claims.db_id  " +
          " left join claimstructures on claimstructures.claim_id = claims.db_id  " +
          " left join healthsafety on healthsafety.claim_id = claims.db_id WHERE claims.db_id = ? group by claims.db_id";

        tx.executeSql(claimreportsSelect, [claim_id],
          function(tx, res_reportsStatus) {
            reportsStatus = res_reportsStatus['rows']['item'](0);
            return d.resolve(reportsStatus);
          },
          function(tx, error) {
            console.log(error);
          });
      });
      return promise = d.promise;
    }

    this.ClaimStructuresDeleteStatus = function(structure_id, deleted, deleted_by) { //set delete status
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        var claimreportsSelect = "UPDATE claimstructures SET status = 2 , deleted = '" + deleted + "',deleted_by ='" + deleted_by + "' WHERE id = ?";

        tx.executeSql(claimreportsSelect, [structure_id],
          function(tx, res_reportsStatus) {
            return d.resolve('done');
          },
          function(tx, error) {
            return d.resolve('error');
            console.log('error');
          });
      });
      return d.promise;
    }
    this.UpdateFnol = function(fnol_detail, claim_id) { //set delete status
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        var claimFnolSelect = "UPDATE claims SET fnol = ? WHERE db_id = ?";

        tx.executeSql(claimFnolSelect, [fnol_detail, claim_id],
          function(tx, res_reportsStatus) {
            return d.resolve('fnol done');
          },
          function(tx, error) {
            console.log('fnol error');
          });
      });
      return d.promise;
    }
    this.ClaimStructuresUpdate = function(structure_id, modified, modified_by, name) { //set delete status
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/

      this.db.transaction(function(tx) {
        var claimreportsSelect = "UPDATE claimstructures SET name = (?) , modified = (?),modified_by =(?) WHERE id = ?";

        tx.executeSql(claimreportsSelect, [name, modified, modified_by, structure_id],
          function(tx, res_reportsStatus) {
            return d.resolve('done');
          },
          function(tx, error) {
            console.log('error');
          });
      });
      return d.promise;
    }

    this.DeleteMediaStatus = function(structuremedia_id) { //set delete status
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      this.db.transaction(function(tx) {
        var structuremediaUpdateStatus = "UPDATE structuremedia SET status = 2 , sync = 0 WHERE id = ?";

        tx.executeSql(structuremediaUpdateStatus, [structuremedia_id],
          function(tx, res_reportsStatus) {
            return d.resolve('done');
          },
          function(tx, error) {
            console.log('error');
          });
      });
      return d.promise;
    }
    this.DeleteMedia = function(structuremedia_id) { //set delete status
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var structuremediaDelete = "DELETE FROM structuremedia WHERE id = ?";

        tx.executeSql(structuremediaDelete, [structuremedia_id],
          function(tx, res_reportsStatus) {
            return d.resolve('done');
          },
          function(tx, error) {
            console.log('error');
          });
      });
      return d.promise;
    }
    this.ClaimStructuresDelete = function(structure_id) { //set delete status
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var claimstructuresDelete = [
          "DELETE FROM claimstructures WHERE id = '" + structure_id + "'",
          "DELETE FROM structuremedia WHERE structuremedia.claimstructure_id = '" + structure_id + "'",
          "DELETE FROM sowtask WHERE sowtask.claimstructure_id = '" + structure_id + "'",
          "DELETE FROM structurenotes WHERE structurenotes.claimstructure_id = '" + structure_id + "'"
        ];
        DbService.db.sqlBatch(claimstructuresDelete,
          function(res) {
            console.log(res);
            return d.resolve('done');
          },
          function(error) {
            console.log(error);
            return d.resolve('error');
          });
      });
      return d.promise;
    }

    this.getClaimStructures = function(claim_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructurenotes);
      });
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createsowtask);
      });
      this.db.transaction(function(tx) {
        var claimstructuresSelect = "SELECT claimstructures.id,claimstructures.db_id,claimstructures.claim_id,claimstructures.name,count(media_3d.id) as media_3ds,count(media_v.id) as media_vs,count(media_a.id) as media_as,count(media_p.id) as media_ps,count(structurenotes.id) as structurenotes,count(sowtask.id) as sowtasks" +
          " FROM claimstructures" +
          " LEFT JOIN structuremedia as media_3d on media_3d.claimstructure_id = claimstructures.id AND media_3d.filetype = '3D' AND media_3d.status = '1'" +
          " LEFT JOIN structuremedia as media_v on media_v.claimstructure_id = claimstructures.id AND media_v.filetype = 'V' AND media_v.status = '1'" +
          " LEFT JOIN structuremedia as media_a on media_a.claimstructure_id = claimstructures.id AND media_a.filetype = 'A' AND media_a.status = '1'" +
          " LEFT JOIN structuremedia as media_p on media_p.claimstructure_id = claimstructures.id AND media_p.filetype = 'P' AND media_p.status = '1'" +
          " LEFT JOIN structurenotes on structurenotes.claimstructure_id = claimstructures.id AND claimstructures.status = '1'" +
          " LEFT JOIN sowtask on sowtask.claimstructure_id = claimstructures.id AND claimstructures.status = '1'" +
          " WHERE claimstructures.claim_id = ? AND claimstructures.status = '1' GROUP BY claimstructures.id order by claimstructures.created ASC";

        tx.executeSql(claimstructuresSelect, [claim_id],
          function(tx, res_claimstructures) {
            var claimstructures = {};
            for (i = 0; i < res_claimstructures['rows']['length']; i++) {
              claimstructures[i] = res_claimstructures['rows']['item'](i);
            }
            return d.resolve(claimstructures);
          },
          function(tx, error) {
            console.log(error);
          });
      });
      return promise = d.promise;
    }

    this.getClaimStructuresForSync = function(claim_id) {
      var d = $q.defer();
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var claimstructuresSelect = "SELECT * FROM claimstructures where claim_id = ?";
      this.db.executeSql(claimstructuresSelect, [claim_id],
        function(res_claimstructures) {
          var claimstructures = {};
          if (res_claimstructures['rows']['length'] > 0) {
            for (i = 0; i < res_claimstructures['rows']['length']; i++) {
              claimstructures[i] = res_claimstructures['rows']['item'](i);

              claimstructures[i]['structurenotes'] = null;
              claimstructures[i]['sowtask'] = null;
              claimstructures[i]['structuremedia'] = null;
            }
            $.each(claimstructures, function(key, value) {
              DbService.getSOWForSync(value['id'])
                .then(function(responce_sowtask) {
                  console.log(responce_sowtask);
                  $.each(responce_sowtask, function(k, v) {
                    var found = false;
                    $.each(claimstructures, function(sk, sv) {
                      if (sv['id'] == k) {
                        claimstructures[sk]['sowtask'] = v;
                      }
                    });
                    claimstructures = DbService.checkStructuresdata(claimstructures, claimstructures);
                    if (claimstructures['data_is_ready']) {
                      delete claimstructures['data_is_ready'];
                      var claim_structures = {};
                      claim_structures[claim_id] = claimstructures;
                      return d.resolve(claim_structures);
                    }
                  });
                });
              DbService.getStructurenotesSync(value['id'])
                .then(function(responce_structurenotes) {
                  console.log(responce_structurenotes);
                  $.each(responce_structurenotes, function(k, v) {
                    var found = false;
                    $.each(claimstructures, function(sk, sv) {
                      if (sv['id'] == k) {
                        claimstructures[sk]['structurenotes'] = v;
                      }
                    });

                    claimstructures = DbService.checkStructuresdata(claimstructures, claimstructures);
                    if (claimstructures['data_is_ready']) {
                      delete claimstructures['data_is_ready'];
                      var claim_structures = {};
                      claim_structures[claim_id] = claimstructures;
                      return d.resolve(claim_structures);
                    }
                  });
                });
              DbService.getDeleteClaimMedia(value['id'], claim_id)
                .then(function(responce_claimMedia) {
                  console.log(responce_claimMedia);
                  $.each(responce_claimMedia, function(k, v) {
                    $.each(claimstructures, function(sk, sv) {
                      if (sv['id'] == k) {
                        claimstructures[sk]['structuremedia'] = v;
                      }
                    });

                    claimstructures = DbService.checkStructuresdata(claimstructures, claimstructures);
                    if (claimstructures['data_is_ready']) {
                      delete claimstructures['data_is_ready'];
                      var claim_structures = {};
                      claim_structures[claim_id] = claimstructures;
                      return d.resolve(claim_structures);
                    }
                  });
                });
            });
          } else {
            var claim_structures = {};
            claim_structures[claim_id] = {};
            return d.resolve(claim_structures);
          }
        },
        function(error) {
          var claim_structures = {};
          claim_structures[claim_id] = {};
          return d.resolve(claim_structures);
          console.log(error);
        });
      return d.promise;
    }

    this.checkStructuresdata = function(claim_structures, claimstructures) { //check sow and Structures note is get from local database
      var data_is_ready = true;
      //console.log(claimstructures);
      $.each(claim_structures, function(k, v) {
        if (k != 'data_is_ready') {
          if (v['sowtask'] == null) {
            data_is_ready = false;
          } else if (v['structurenotes'] == null) {
            data_is_ready = false;
          } else if (v['structuremedia'] == null) {
            data_is_ready = false;
          } else {
            console.log(claimstructures[k]);
            if (claimstructures[k]['id'] != 0) {
              claimstructures[k]['db_id'] = claimstructures[k]['id'];
            }
            if (claimstructures[k]['id'] != 0 && claimstructures[k]['id'].substring(0, 2) == 0) {
              claimstructures[k]['id'] = 0;
            }
          }
        }
      });
      /*if (data_is_ready == true) {
        claimstructures['db_id'] = claimstructures['id'];
        if (claimstructures['id'].substring(0, 2) == 0) {
          claimstructures['id'] = 0;
        }
      }*/
      claimstructures['data_is_ready'] = data_is_ready;

      return claimstructures;
    }

    this.getDashbordClaim = function(is_dashboard, callBackFunctionValue) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (typeof this.db == 'undefined' || this.db == null) {
        this.db = window.sqlitePlugin.openDatabase({
          name: 'survey.db',
          location: 'default'
        });
      }
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.claimsTable,
          function(tx, error) {
            console.log(error);
          },
          function(tx, res) {
            console.log('table create');
          });
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var where_condition = '';
        if (is_dashboard == 1) {
          var where_condition = ' where claims.is_dashboard = 1';
        }
        var query = "SELECT claims.*,clients.contact_person_name,clients.client_name,users.firstname as assign_f,users.lastname as assign_l " +
          "FROM claims " +
          "left join clients on clients.db_id = claims.client_id " +
          "left join users on users.db_id = claims.assign_to " + where_condition + " ORDER BY claims.created DESC ";
        tx.executeSql(query, function(tx, error) {
          console.log('select claim error: ' + error.message);
        }, function(tx, res) {
          var claims = [];
          var i = 0;
          while (i < res['rows']['length']) {
            claims[i] = res['rows']['item'](i);
            //console.log(claims[i]);
            i = i + 1;
          }
          //callBackFunctionValue(claims);
          return d.resolve(claims);
        });
      }, function(error) {
        console.log('transaction error: ' + error.message);
        return d.resolve({});
      });
      return d.promise;
    }
    this.getListingClaim = function(is_listing) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.claimsTable,
          function(tx, error) {
            console.log(error);
          },
          function(tx, res) {
            console.log('table create');
          });
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var where_condition = '';
        //if (is_listing == 1) {
        var where_condition = ' where claims.listing = 1';
        //}
        var query = "SELECT claims.*,clients.client_name,clients.contact_person_name,users.firstname as assign_f,users.lastname as assign_l " +
          "FROM claims " +
          "LEFT JOIN clients ON clients.db_id = claims.client_id " +
          "LEFT JOIN users ON users.db_id = claims.assign_to " + where_condition + " ORDER BY claims.created DESC";
        tx.executeSql(query, function(tx, error) {
          console.log('select claim error: ' + error.message);
        }, function(tx, res) {
          var claims = [];

          for (var i = 0; i < res['rows']['length']; i++) {
            claims[i] = res['rows']['item'](i);
          }
          return d.resolve(claims);
        });
      }, function(error) {
        return d.resolve({});
        console.log('transaction error: ' + error.message);
      });
      return d.promise;
    }

    this.getStructureId = function(id) {
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var query = "select db_id,claim_id from claimstructures where id = ?";
        tx.executeSql(query, [id], function(tx, res) {
          var db_id = res['rows']['item'](0)['db_id'];
          var claim_id = res['rows']['item'](0)['claim_id'];
          return d.resolve({
            'claimstructurest_insertId': db_id,
            'claim_id': claim_id
          });
        }, function(tx, err) {
          return d.resolve(0);
        });
      });
      return d.promise;
    }
    this.setClaimStructure = function(id, user_id, claim_id, structure_id, name, created, modified, deleted, modified_by, deleted_by, status, sync) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.claimstructures);
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        var query = "UPDATE claimstructures set `user_id`= (?), `claim_id`= (?)  ,`structure_id`= (?),`name`= (?),`created`= (?),`modified`= (?),`deleted`= (?),`modified_by`= (?),`deleted_by`= (?),`status`= (?),`sync`= (?) where id = ? ";
        tx.executeSql(query, [user_id, claim_id, structure_id, name, created, modified, deleted, modified_by, deleted_by, status, sync, id],
          function(tx, res) {
            console.log(res);
            if (res.rowsAffected == 0) {
              var query = "INSERT INTO claimstructures (id,user_id ,  claim_id ,  structure_id ,  name ,  created ,  modified ,  deleted , modified_by ,  deleted_by ,  status, sync) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
              tx.executeSql(query, [id, user_id, claim_id, structure_id, name, created, modified, deleted, modified_by, deleted_by, status, sync], function(tx, res) {
                  //console.log(res);
                  return d.resolve(res);
                },
                function(tx, error) {
                  // console.log('INSERT claimstructures error -----------: ' + error.message);
                  return d.resolve({
                    insertId: 0
                  });
                });
            }
            return d.resolve(res);
          },
          function(tx, error) {
            //console.log('UPDATE claimstructures error -----------: ' + error.message);
            return d.resolve({
              insertId: 0
            });
          });
      });
      return d.promise;
    }

    this.setMediaLocal = function(id, claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status) {
      if (typeof this.db == 'undefined') {
        this.db = window.sqlitePlugin.openDatabase({
          name: 'survey.db',
          location: 'default'
        });
      }
      var sync = 1;
      if (id.toString().substr(0, 2) == 0) {
        sync = 0;
      }

      if (created == '') {
        created = Api.formatDateDatabase(created);
      }
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      if (filetype == "3D") {
        this.db.transaction(function(tx) {
          var query = "DELETE FROM structuremedia WHERE claimstructure_id = ? AND filetype = ? AND user_id = ?";
          tx.executeSql(query, [claimstructure_id, filetype, user_id], function(txD, res) {
              console.log(res);
              console.log('INSERT structuremedia sucsess -----------');
            },
            function(txD, error) {
              console.log('INSERT structuremedia error -----------: ' + error.message);
            });
        });
      }

      var d = $q.defer();

      var query = "INSERT INTO structuremedia (id,claimstructure_id,user_id,media_file,filetype, created ,  modified ,  deleted , modified_by ,  deleted_by ,  status,sync) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      this.db.executeSql(query, [id, claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status, sync],
        function(res) {
          console.log(res);
          return d.resolve(res);
          console.log('INSERT structuremedia sucsess -----------');
        },
        function(error) {
          return d.resolve({
            insertId: 0
          });
          console.log('INSERT structuremedia error -----------: ' + error.message);
        });
      return d.promise;
    }

    this.getLocalMedia = function() {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      var result = [];
      this.db.transaction(function(txsel) {
        ////console.log("You are in transaction");
        var query = "SELECT * FROM structuremedia";
        console.log(query);
        txsel.executeSql(query, [], function(txsel, resultSet) {
            for (var x = 0; x < resultSet.rows.length; x++) {
              result[x] = resultSet.rows.item(x);
            }
            console.log(result);
          },
          function(txsel, error) {
            console.log('SELECT error: ' + error.message);
          });
      }, function(error) {
        console.log(error);
        //console.log('transaction error: ' + error.message);
      }, function() {
        console.log('transaction ok');
      });
      return result;
    }

    this.setStructureMedia = function(id, claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status, claim_id, constant_media, full_media_path, upload_claimstructure_id, folder, file_name) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql(DbService.Createstructuremedia);
      });
      //fiels = profile_pick_full_url.split('/');

      //}
      var fileTransfer = new FileTransfer();

      var d = $q.defer();
      //$cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(
      /*$cordovaFile.checkFile(cordova.file.dataDirectory + folder, file_name).then(
        function(res) {
          console.log('result of file exiest function');
          console.log(res);
          this.db.transaction(function(tx) {
            var query = "UPDATE structuremedia set `claimstructure_id`= (?), `user_id`= (?) ,`media_file` = (?) ,`filetype`= (?),`created`= (?),`modified`= (?),`deleted`= (?),`modified_by`= (?),`deleted_by`= (?),`status`= (?) where id = ? ";
            tx.executeSql(query, [claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status, id],
              function(tx, res) {
                if (res.rowsAffected == 0) {
                  var query = "INSERT INTO structuremedia (id,claimstructure_id,user_id,media_file,filetype, created ,  modified ,  deleted , modified_by ,  deleted_by ,  status) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                  tx.executeSql(query, [id, claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status],
                    function(tx, res) {
                      console.log('MEDIA download done ' + id + ' folder ' + upload_claimstructure_id + ' name ' + media_file);
                      return d.resolve(res);
                    },
                    function(tx, error) {
                      return d.resolve({
                        insertId: 0
                      });
                    });
                }
                console.log('MEDIA download done ' + id + ' folder ' + upload_claimstructure_id + ' name ' + media_file);
                return d.resolve(res);
              },
              function(tx, error) {
                console.log('UPDATE structuremedia error -----------: ' + error.message);
                return d.resolve({
                  insertId: 0
                });
              });
          })
        },
        function(error) {
      console.log('error of file exiest function');
      console.log(error);*/

      fileTransfer.download(full_media_path, cordova.file.dataDirectory + folder + file_name,
        function(entry) {
          this.db.transaction(function(tx) {
            var query = "UPDATE structuremedia set `claimstructure_id`= (?), `user_id`= (?) ,`media_file` = (?) ,`filetype`= (?),`created`= (?),`modified`= (?),`deleted`= (?),`modified_by`= (?),`deleted_by`= (?),`status`= (?) where id = ? ";
            tx.executeSql(query, [claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status, id],
              function(tx, res) {
                if (res.rowsAffected == 0) {
                  var query = "INSERT INTO structuremedia (id,claimstructure_id,user_id,media_file,filetype, created ,  modified ,  deleted , modified_by ,  deleted_by ,  status) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                  tx.executeSql(query, [id, claimstructure_id, user_id, media_file, filetype, created, modified, deleted, modified_by, deleted_by, status],
                    function(tx, res) {
                      console.log('MEDIA download done ' + id + ' folder ' + upload_claimstructure_id + ' name ' + media_file);
                      return d.resolve(res);
                    },
                    function(tx, error) {
                      return d.resolve({
                        insertId: 0
                      });
                    });
                }
                console.log('MEDIA download done ' + id + ' folder ' + upload_claimstructure_id + ' name ' + media_file);
                return d.resolve(res);
              },
              function(tx, error) {
                console.log('UPDATE structuremedia error -----------: ' + error.message);
                return d.resolve({
                  insertId: 0
                });
              });
          })
        },
        function(error) {
          //console.log('start error in download ' + upload_claimstructure_id);
          console.log('meida download fail ' + id + ' name ' + media_file);
          //console.log(error);
          //console.log('end error in download ' + upload_claimstructure_id);

          /*return d.resolve({
            insertId: 0
          });*/
        }
      );
      //});
      return d.promise;
    }

    this.CreateDatabaseClaimTable = function() {
      this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });
      //this.db.transaction(function(tx) {
      this.db.sqlBatch(this.DatabaseClaimTable, function(res) {
        console.log('table are addred')
      }, function(error) {
        console.log('error table are addred')
      });
      //});
    }
  }])
  .service("masterTable", ['$http', '$q', '$cordovaSQLite', '$filter', 'Api', function($http, $q, $cordovaSQLite, $filter, Api) {
    var masterTableService = this;
    this.masterTable = function() {
      if (typeof this.db == 'undefined' || this.db == '') {
        this.db = window.sqlitePlugin.openDatabase({
          name: 'survey.db',
          location: 'default'
        });
      }
      var d = $q.defer();
      //this.db.transaction(function(tx) {
      this.db.sqlBatch([
        "DROP TABLE IF EXISTS `actualperil`;",
        "DROP TABLE IF EXISTS `properties`;",
        "DROP TABLE IF EXISTS `propertyage`;",
        "DROP TABLE IF EXISTS `propertycondition`;",
        "DROP TABLE IF EXISTS `roofmaterial`;",
        "DROP TABLE IF EXISTS `rooftype`;",
        "DROP TABLE IF EXISTS `roomsaffected`;",
        "DROP TABLE IF EXISTS `storeys`;",
        "DROP TABLE IF EXISTS `walltype`;",
        "DROP TABLE IF EXISTS `structures`;",
        "DROP TABLE IF EXISTS `tasks`;",
        "DROP TABLE IF EXISTS `parameters`;",
        "DROP TABLE IF EXISTS `trades`;"
      ], function(res) {
        masterTableService.db.sqlBatch([
          "CREATE TABLE `actualperil` (`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `properties` (`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `propertyage`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `propertycondition`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `roofmaterial`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `roomsaffected`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `rooftype`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `storeys`(`id`	INTEGER UNIQUE,`name`	TEXT,	`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `walltype`(`id`	INTEGER UNIQUE,`name`	TEXT,`status`	TEXT,`created_by`	TEXT,`modified_by`	TEXT,`created`	TEXT,`modified`	TEXT,`deleted`	TEXT,`deleted_by`	TEXT);",
          "CREATE TABLE `structures`(`id`	INTEGER UNIQUE,claim_id TEXT,parent_id TEXT,name TEXT,position TEXT,status TEXT,created_by TEXT,modified_by TEXT,created TEXT,modified TEXT,deleted TEXT,deleted_by TEXT);",
          "CREATE TABLE `tasks`(id INTEGER UNIQUE,user_id TEXT, trade_id TEXT, task_name TEXT, wi_key TEXT, code TEXT, unit_type TEXT, labour TEXT, material TEXT, hours_per_unit TEXT, material_cost_per_unit TEXT, plant_cost_per_unit TEXT, prompt_for_material TEXT, wastage TEXT,materialunits_per_unit TEXT,no_overheads TEXT,created TEXT,modified TEXT,deleted TEXT,deleted_by TEXT,status TEXT);",
          "CREATE TABLE `parameters`( para_id INTEGER UNIQUE,para_parent_id TEXT,para_value TEXT,slug TEXT,para_desc TEXT,para_sort_order TEXT,para_tech_desc TEXT,created TEXT,created_by TEXT,updated TEXT,updated_by TEXT,status TEXT);",
          "CREATE TABLE `trades`( id INTEGER UNIQUE,claim_id TEXT,parent_id TEXT,name TEXT,position TEXT,created_by TEXT,modified_by TEXT,created TEXT,modified TEXT,deleted TEXT,deleted_by TEXT,status TEXT);"
        ], function(res) {
          console.log(res);
          return d.resolve('done');
        }, function(error) {
          console.log(error);
          return d.resolve('error');
        });
        console.log(res);
      }, function(error) {
        console.log(error);
        return d.resolve('error');
      });
      return d.promise;
    }

    this.masterTableImport = function() {
      urlBase = Api.urlBase + 'master/masterdataDump';
      req = {
        headers: Api.HEADERS
      };
      pdata = [];
      $http.post(urlBase, pdata, req).then(
        function(response) {
          if (response['data']['status'] == 1) {

            masterTableService.masterTable().then(function() {
              $.each(response['data']['response'], function(k, v) {
                masterTableService.db.transaction(function(tx) {
                    masterTableService.db.sqlBatch(v, function(res) {
                      console.log(res);
                    }, function(error) {
                      console.log(error);
                    })
                  },
                  function(error) {
                    console.log('syncing issue');
                  }
                )
              });
            });
            console.log('master data import');
          }
        })
    }
    this.getParameters = function(param_id) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      this.db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE `parameters`( para_id INTEGER ,para_parent_id TEXT,para_value TEXT,slug TEXT,para_desc TEXT,para_sort_order TEXT,para_tech_desc TEXT,created TEXT,created_by TEXT,updated TEXT,updated_by TEXT,status TEXT);");
      });
      var d = $q.defer();
      this.db.transaction(function(tx) {
        tx.executeSql("SELECT para_id as id,para_value as name FROM `parameters` where para_parent_id = ?", [param_id],
          function(tx, res) {
            res_parameters = [];
            if (res['rows']['length'] > 0) {
              for (var i = 0; i < res['rows']['length']; i++) {
                res_parameters.push(res['rows']['item'](i));
              }
            }
            return d.resolve(res_parameters);
          },
          function(tx, error) {
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
    this.getMasterTable = function(TableFieldname, TableName, TableCondition, TableConditionValues) {
      /*this.db = window.sqlitePlugin.openDatabase({
        name: 'survey.db',
        location: 'default'
      });*/
      if (typeof this.db == 'undefined') {
        this.db = window.sqlitePlugin.openDatabase({
          name: 'survey.db',
          location: 'default'
        });
      }
      var d = $q.defer();
      this.db.transaction(function(tx) {
        if (TableCondition != '') {
          TableCondition = 'WHERE ' + TableCondition;
        }
        tx.executeSql("SELECT " + TableFieldname + " FROM `" + TableName + "` " + TableCondition, TableConditionValues,
          function(tx, res) {
            res_parameters = [];
            if (res['rows']['length'] > 0) {
              for (var i = 0; i < res['rows']['length']; i++) {
                res_parameters.push(res['rows']['item'](i));
              }
            }
            return d.resolve(res_parameters);
          },
          function(tx, error) {
            console.log('error in get pfi data');
          })
      });
      return d.promise;
    }
  }])
  .service("Api", ['$http', '$filter', function($http, $filter) {
    ApiService = this;
    this.urlBase = "http://devsurveytek.azurewebsites.net/api/";
    this.api_version = "0.070201";
    this.starthour = 6;
    this.endhour = 21;
    this.HEADERS = {
      'X-public': localStorage.getItem("X-public"),
      'X-hash': '9XTMgu0h0R14lm8vO2cQYG8SFMe4A50j',
      'device-id': localStorage.getItem("device-id"),
      //'uuid':device.uuid,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.addZero = function(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    this.formatDate = function(date) {
      if (typeof date == 'undefined' || date == '') {
        var date_formate = new Date();
        var month = '' + (date_formate.getMonth() + 1);
        var day = '' + date_formate.getDate();
        var year = date_formate.getFullYear();
      } else {
        var date_formate = new Date(date);
        var month = '' + (date_formate.getMonth() + 1);
        var day = '' + date_formate.getDate();
        var year = date_formate.getFullYear();
      }
      var h = ApiService.addZero(date_formate.getHours());
      var m = ApiService.addZero(date_formate.getMinutes());
      var s = ApiService.addZero(date_formate.getSeconds());
      //if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      ];
      return [day, monthNames[month - 1], year].join(' ') + " " + h + ":" + m + ":" + s;
    }
    this.formatDateClaimReport = function(date) {
      if (typeof date != 'undefined' && date != '' && typeof date != 'null' && date != null) {
        var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (day.length < 2) day = '0' + day;
        monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        return [day, monthNames[month - 1], year].join('-');
        //return [year, ApiService.addZero(month - 1), day].join('-') + " " + h + ":" + m + ":" + s;
      }
      return '';
    }

    this.formatDateDatabase = function(date) {
      if (typeof date != 'undefined' && date != '') {
        var vdate = new Date(date),
          month = '' + (vdate.getMonth() + 1),
          day = '' + vdate.getDate(),
          year = vdate.getFullYear();

        if (day.length < 2) day = '0' + day;

        var h = ApiService.addZero(vdate.getHours());
        var m = ApiService.addZero(vdate.getMinutes());
        var s = ApiService.addZero(vdate.getSeconds());

        return [year, ApiService.addZero(month), day].join('-') + " " + h + ":" + m + ":" + s;
      } else {
        return $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
      }
    }
  }]);
