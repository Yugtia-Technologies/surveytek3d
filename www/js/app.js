// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.jobcontrollers', 'starter.logincontrollers', 'starter.dashboardController', 'starter.forgotpassworcontrollers', 'starter.profilecontrollers', 'starter.forgotpassworcontrollers', 'starter.controllers', 'starter.surveyorControllers', 'starter.services', 'starter.sowcontrollers', 'starter.directives', 'starter.claimcontrollers', 'starter.claimreportcontrollers', 'ngMessages'])
  .run(function($ionicPlatform, $ionicPopup, $rootScope, $cordovaSQLite) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }


      function fail(e) {
        console.log("FileSystem Error");
        console.dir(e);
      }
      // check connection
      ionic.Platform.fullScreen();
      if (window.StatusBar) {
        return StatusBar.hide();
      }

    });
  })
  .config(function($ionicConfigProvider) {
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $ionicConfigProvider.views.maxCache(0);
  })
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        cache: false,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.search', {
        url: '/search',
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html'
          }
        }
      })
      .state('app.add-claim', {
        url: '/add-claim/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/add-claim.html',
            params: {
              claim_id: 0
            }
          }
        }
      })
      .state('login', {
        url: '/login-home',
        templateUrl: 'templates/login-home.html',
        controller: 'loginCtrl'
      })
      .state('forgot_password', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html',
        controller: 'ForgotPasswordCtrl'
      })
      .state('app.dashboard', {
        url: '/dashboard',
        views: {
          'menuContent': {
            templateUrl: 'templates/dashboard.html',
            cache: false,
            controller: 'dashboardCtrl'
          }
        }
      })
      .state('app.jobpreview', {
        url: '/jobpreview/:client_id/:daterange/:type/:count',
        views: {
          'menuContent': {
            templateUrl: 'templates/jobpreview.html',
            controller: 'claimviewCtrl',
            cache: false,
            params: {
              'client_id': 0,
              'daterange': '',
              'type': '1',
              'count': ''
            }
          },
        }
      })
      .state('app.claimview', {
        url: '/claimview/:id/:from_dashboard',
        views: {
          'menuContent': {
            templateUrl: 'templates/claimview.html',
            controller: 'claimdetailCtrl',
            cache: false,
            params: {
              'id': 0,
              'from_dashboard': 0
            }
          },
        }
      })
      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
            controller: 'profileCtrl'
          }
        }
      })
      .state('app.claim-report', {
        url: '/claim-report/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/claim-report.html',
            controller: 'claimreportCtrl',
            cache: false,
            params: {
              'claim_id': 0
            }
          }
        }
      })
      .state('app.room-detail', {
        url: '/room-detail',
        views: {
          'menuContent': {
            templateUrl: 'templates/room-detail.html',
            //controller : 'roomDetailCtrl'
          }
        }
      })
      .state('app.surveyorlist', {
        url: '/surveyorlist',
        views: {
          'menuContent': {
            templateUrl: 'templates/surveyorlist.html',
            controller: 'surveyorCtrl'
          }
        }
      })
      .state('app.editsurveyor', {
        url: '/editsurveyor/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/edit-surveryor.html',
            controller: 'surveyorCtrl',
            cache: false,
            params: {
              'id': 0
            }
          }
        }
      })
      .state('app.detailview', {
        url: '/detailview/:id/:from_dashboard',
        views: {
          'menuContent': {
            templateUrl: 'templates/detailview.html',
            controller: 'jobdetailCtrl',
            cache: false,
            params: {
              'id': 0,
              'from_dashboard': 0
            }
          }
        },
        cache: false
      })
      .state('app.editProfile', {
        url: '/editProfile',
        views: {
          'menuContent': {
            templateUrl: 'templates/edit-profile.html',
            controller: 'editProfileCtrl'
          }
        }
      }).state('app.recovery_form', {
        url: '/recovery_form/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/recovery_form.html',
            controller: 'claimreportCtrl',
            params: {
              'claim_id': 0
            }
          }
        }
      }).state('app.pfi_form', {
        url: '/pfi_form/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/pfi_form.html',
            controller: 'claimreportCtrl',
            params: {
              'claim_id': 0
            }
          }
        }
      }).state('app.drying_form', {
        url: '/drying_form/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/drying_form.html',
            controller: 'claimreportCtrl',
            params: {
              'claim_id': 0
            }
          }
        }
      }).state('app.healthsafe_form', {
        url: '/healthsafe_form/:claim_id',
        views: {
          'menuContent': {
            templateUrl: 'templates/healthsafe_form.html',
            controller: 'claimreportCtrl',
            params: {
              'claim_id': 0
            }
          }
        }
      }).state('app.sow', {
        url: '/sow/:room_id/:claim_id/:open_tab/:local_db_id/:to_link',
        views: {
          'menuContent': {
            templateUrl: 'templates/sow.html',
            controller: 'sowCtrl',
            cache: false,
            params: {
              room_id: 0,
              claim_id: 0,
              open_tab: '',
              local_db_id: 0,
              to_link: ''
            }
          }
        }
      })
      .state('app.browse', {
        url: '/browse',
        views: {
          'menuContent': {
            templateUrl: 'templates/browse.html'
          }
        }
      }).state('app.pdf', {
        url: '/pdf/:claim_id/:api_request',
        views: {
          'menuContent': {
            templateUrl: 'templates/pdf.html',
            controller: 'pdfCtrl'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback

    if (check_connection() || JSON.parse(localStorage.getItem("UserData")) == null) {
      $urlRouterProvider.otherwise('/login-home');
    } else {
      $urlRouterProvider.otherwise('/app/dashboard');
    }
  }).config(function($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(5);

    // note that you can also chain configs
    $ionicConfigProvider.backButton.previousTitleText(false).text('< Go Back');
    //$ionicConfigProvider.backButton.text('Go Back');
  });
