$(document).ready(function() {
  $(document).on('click', '.tab-container li', function(e) {
    position = $(this).index();

    $(".form-container form").removeClass("form-show").addClass("form-hide");
    $(".form-container form").eq(position).addClass('form-show').removeClass('form-hide');

    $(".tab-container li").removeClass("selected");
    $(".tab-container li").eq(position).addClass('selected');

    $(".scroll-content .scroll").attr('style', "transform: translate3d(0px, 0px, 0px) scale(1);");
  });
});

function equalHeight(group) {
  tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if (thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}
