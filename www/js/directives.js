angular.module('starter.directives', [])

.directive('map', function($rootScope) {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {
		console.log($scope.$parent.TaskData);
	function initialize() {
		var address = $rootScope.mapAddress ;		
		var geocoder = new google.maps.Geocoder();
		console.log("initialize Map");
		  var LatestLat = -34.397;//localStorage.getItem("LatestLat");
		  var LatestLon = 150.644;//localStorage.getItem("LatestLon");
			if( typeof LatestLat=='null' && typeof $scope.$parent.TaskData !='undefined')
			{
				LatestLat=$scope.$parent.TaskData.late;
				LatestLon=$scope.$parent.TaskData.long;
			}
			
			var mapOptions = {
				mapTypeId: google.maps.MapTypeId.SATELLITE,
				zoom: 19,
				center: new google.maps.LatLng(LatestLat, LatestLon),
				mapTypeControl: true,
				mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
				navigationControl: true
			};
			var map = new google.maps.Map($element[0], mapOptions);
			if (geocoder) {
				 geocoder.geocode( { 'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
					  if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
						map.setCenter(results[0].geometry.location);

						var infowindow = new google.maps.InfoWindow(
							{ content: '<b>'+address+'</b>',
							  size: new google.maps.Size(150, 50)
							});
			
						var marker = new google.maps.Marker({
							position: results[0].geometry.location,
							map: map, 
							title:address
						}); 
						console.log(marker);
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.open(map,marker);
						});

					  } 
					} 
				  });
			}
			$scope.onCreate({map: map});
			google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
				e.preventDefault();
				return false;
			});
	  }
    // Stop the side bar from dragging when mousedown/tapdown on the map
      if (document.readyState === "complete") {
        setTimeout(function(){ initialize(); }, 2000);
        
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
})
.directive('validateEmail', function() {
  var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

  return {
    require: 'ngModel',
    restrict: '',
    link: function(scope, elm, attrs, ctrl) {
      // only apply the validator if ngModel is present and Angular has added the email validator
      if (ctrl && ctrl.$validators.email) {

        // this will overwrite the default Angular email validator
        ctrl.$validators.email = function(modelValue) {
          return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
        };
      }
    }
  };
});
